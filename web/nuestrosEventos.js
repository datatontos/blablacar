var nuestrosEventos = [ 
	 { 
	 	 "id": "1",
	 	 "nombre": "A SUMMER STORY",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Arganda del Rey",
	 	 "fecha_ini_2018": "22-06-2018",
	 	 "fecha_fin_2018": "23-06-2018",
	 	 "fecha_ini_2019": "21-06-2019",
	 	 "fecha_fin_2019": "22-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 2.6666666666666665,
	 	 "D002_evento_media_viajes_2018": 0.46,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.55,
	 	 "D004_evento_viajes_2019": 3.33,
	 	 "D005_evento_media_viajes_2019": 0.83,
	 	 "D006_evento_media_viajes_dia_semana_2019": 1.38,

	 	 "D007_proporcion_asientos_evento_2018": 45.83,
	 	 "D008_proporcion_asientos_media_evento_2018": 1.71,
	 	 "D009_proporcion_asientos_evento_2019": 21.31,
	 	 "D010_proporcion_asientos_media_evento_2019": 2.74,

	 	 "D011_ocupacion_asientos_evento_2018": 0.07,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.41,
	 	 "D013_ocupacion_asientos_evento_2019": 0.08,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.32,

	 	 "D015_precio_evento_2018": 0.0526,
	 	 "D016_precio_medio_evento_2018": 0.051,
	 	 "D017_precio_evento_2019": 0.0504,
	 	 "D018_precio_medio_evento_2019": 0.0529,

	 	 "D019_novatos_evento_2018": 0.67,
	 	 "D020_novatos_media_evento_2018": 2.84,
	 	 "D021_novatos_evento_2019": 0.33,
	 	 "D022_novatos_media_evento_2019": 2.52,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-a summer story-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valladolid', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.25, 'Proporción': 784.62}, 2: {'City': 'Badajoz', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Castellón de la Plana ', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1266.67}, 4: {'City': 'Irun', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Jaén', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2300.0}, 6: {'City': 'Pamplona', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-a summer story-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Murcia', 'Viajes en evento': 2, 'Viajes normal': 0.24, 'Proporción': 850.0}, 2: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.47, 'Proporción': 429.63}, 3: {'City': 'Almería', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Badajoz', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}, 5: {'City': 'Granada', 'Viajes en evento': 1, 'Viajes normal': 0.09, 'Proporción': 1166.67}, 6: {'City': 'Gandia', 'Viajes en evento': 1, 'Viajes normal': 0.22, 'Proporción': 450.0}, 7: {'City': 'Quart de Poblet', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-a summer story-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1570.59}, 1: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 3625.0}, 2: {'City': 'Benidorm', 'Viajes en evento': 2, 'Viajes normal': 0.05, 'Proporción': 4200.0}, 3: {'City': 'Castellón de la Plana ', 'Viajes en evento': 1, 'Viajes normal': 0.1, 'Proporción': 1000.0}, 4: {'City': 'Granada', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1250.0}, 5: {'City': 'Málaga', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}, 6: {'City': 'el Campello', 'Viajes en evento': 1, 'Viajes normal': 0.1, 'Proporción': 1000.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-a summer story-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 3, 'Viajes normal': 0.47, 'Proporción': 636.99}, 1: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1600.0}, 2: {'City': 'Cartagena', 'Viajes en evento': 1, 'Viajes normal': 0.23, 'Proporción': 433.33}, 3: {'City': 'Jaén', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_a summer story_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_a summer story_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ARMIN VAN BUUREN', 'DIMITRI VEGAS', 'AXWELL A INGROSSO', 'BRENNANT HEARTH', 'REEKO', 'VINI VICI', 'FEDDE LE GRAND', 'MICHAEL CALFAN', 'CID', 'JORIS VOORN', 'DIMITRI VANGELIS', 'KÖLSCH', 'TOM STAAR', 'SUNNERY JAMES & RYAN MARCIANO', 'MACEO PLEX', 'VITALIC', 'THE MARTINEZ BROTHERS', 'AMELIE LENS', 'KRYDER', 'CYA', 'RECONDITE', 'GOURYELLA', 'BRIAN CROSS', 'DJ NANO', 'LOCO DICE', 'DIM3NSION', 'COREY JAMES', 'ANDRES CAMPO', 'PACO OSUNA', 'BLACKGATE', 'OSCAR MULERO', 'REBEKAH', 'LEWIS FAUTZI', 'MANU GONZALEZ', 'GONÇALO', 'RAUL PACHECO', 'FERNANDO BALLESTEROS', 'SURGEON & LADY STARLIGHT'],
	 	 "artistas_evento_2019":['ARDIYA', 'ARGIA', 'DIMITRI VEGAS', 'OLIVER HELDENS', 'DON DIABLO', 'BERET', 'LOST FREQUENCIES', 'MIKE WILLIAMS', 'NICKY ROMERO', 'ABOVE BEYOND', 'DJ NEIL', 'LIKE MIKE', 'TADEO', 'TENSAC', 'AYAX Y PROK', 'COONE', 'GARETH EMERY', 'HEADHUNTERZ', 'CID', 'YVESV', 'BORGORE', 'DIMITRI VANGELIS', 'THIRD PARTY', 'CARLCOX', 'ADAM BEYER', 'ZETAZEN', 'AMELIE LENS', 'DEBORAH DE LUCA', 'DELAPORTE', 'SAM PAGANINI', 'SUBSHOCK & EVANGELOS', 'ANDREA OLIVA', 'NIC FANCIULLI', 'BRIAN CROSS', 'DJ NANO', 'DIM3NSION', 'RICHIE HAWTIN', 'BLAWAN', 'ANDRESCAMPO ', 'GARABATTO', 'PACO OSUNA', 'FATIMA HAJJI', 'PAUL RITCH', 'OSCAR MULERO', 'MILO SPYKERS', 'LES CASTIZOS', 'BEN SIMS', 'BRIAN VAN ANDEL', 'SANSIXTO', 'AVISION', 'MANU GONZALEZ', 'SALDIVAR', 'ARTURO GRAO', 'FER BR', 'GONÇALO', 'JOSE M DURO', 'FRAN DC', 'KARRETERO', 'RAMSES LOPEZ', 'SHAKE COCONUT', 'LUIS ROCA', 'FERNANDO COSTA', 'ANGEL SANCHEZ', 'FERCHO ENERGY', 'JUANJO VERGARA', 'NIKONE WALLS', 'BIGTOPO & OMAR DIAZ', 'JOSE DE MARA & CRUSY', 'JULIEN LEIK', 'MASANORI MORITA', 'QUIKE AV'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_a summer story_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_a summer story_2019.jpg",
	 }, 
	 { 
	 	 "id": "2",
	 	 "nombre": "ALTERNA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "El Bonillo",
	 	 "fecha_ini_2018": "05-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "12-07-2019",
	 	 "fecha_fin_2019": "13-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 7.0,
	 	 "D002_evento_media_viajes_2018": 0.37,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.82,
	 	 "D004_evento_viajes_2019": 6.5,
	 	 "D005_evento_media_viajes_2019": 0.4,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.17,

	 	 "D007_proporcion_asientos_evento_2018": 49.38,
	 	 "D008_proporcion_asientos_media_evento_2018": 5.8,
	 	 "D009_proporcion_asientos_evento_2019": 30.67,
	 	 "D010_proporcion_asientos_media_evento_2019": 4.11,

	 	 "D011_ocupacion_asientos_evento_2018": 1.74,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.83,
	 	 "D013_ocupacion_asientos_evento_2019": 1.0,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.69,

	 	 "D015_precio_evento_2018": 0.057,
	 	 "D016_precio_medio_evento_2018": 0.0535,
	 	 "D017_precio_evento_2019": 0.0556,
	 	 "D018_precio_medio_evento_2019": 0.0525,

	 	 "D019_novatos_evento_2018": 1.0,
	 	 "D020_novatos_media_evento_2018": 0.79,
	 	 "D021_novatos_evento_2019": 3.0,
	 	 "D022_novatos_media_evento_2019": 0.85,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-alterna-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 8, 'Viajes normal': 0.25, 'Proporción': 3200.0}, 1: {'City': 'Valencia', 'Viajes en evento': 5, 'Viajes normal': 0.07, 'Proporción': 7500.0}, 2: {'City': 'Albacete', 'Viajes en evento': 3, 'Viajes normal': 0.07, 'Proporción': 4350.0}, 3: {'City': 'San Vicente del Raspeig', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}, 4: {'City': 'Alcobendas', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 6: {'City': "Sant Joan d'Alacant", 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}, 7: {'City': 'Torrejón de Ardoz', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-alterna-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Albacete', 'Viajes en evento': 4, 'Viajes normal': 0.19, 'Proporción': 2133.33}, 1: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.4, 'Proporción': 750.0}, 2: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Murcia', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Almansa', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}, 6: {'City': 'Sevilla', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-alterna-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-alterna-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_alterna_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_alterna_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['LA RAIZ', 'ANIMAL', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'THE QEMISTS', 'NON SERVIUM', 'DUBIOZA KOLEKTIV', 'SONS OF AGUIRRE', 'LENDAKARIS MUERTOS', 'SOZIEDAD ALKOHOLIKA', 'SINKOPE', 'ASIAN DUB FOUNDATION', 'SKAKEITAN', 'RIOT PROPAGANDA', 'HORA ZULU', 'NARCO', 'FUNKIWIS', 'TRASHTUCADA', 'RAT-ZINGER', 'ROSENDO', 'FUCKOP FAMILY', "O FUNK'ILLO", 'BARRIZAL', 'CAMBO BALADA'],
	 	 "artistas_evento_2019":['KOMA', 'SFDK', 'LA PEGATINA', 'ZOO', 'DESAKATO', 'LOS DE MARRAS', 'GATILLAZO', 'KAOTIKO', 'MACHETE EN BOCA', 'NARCO', 'MAFALDA', 'EUKZ', 'THE NIFTYS', 'IKER & PLAN B'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_alterna_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_alterna_2019.jpg",
	 }, 
	 { 
	 	 "id": "3",
	 	 "nombre": "ARENAL SOUND",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Vila-real",
	 	 "fecha_ini_2018": "31-07-2018",
	 	 "fecha_fin_2018": "05-08-2018",
	 	 "fecha_ini_2019": "30-07-2019",
	 	 "fecha_fin_2019": "04-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 1.5,
	 	 "D002_evento_media_viajes_2018": 0.67,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.4,
	 	 "D004_evento_viajes_2019": 1.75,
	 	 "D005_evento_media_viajes_2019": 0.73,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.35,

	 	 "D007_proporcion_asientos_evento_2018": 53.33,
	 	 "D008_proporcion_asientos_media_evento_2018": 3.09,
	 	 "D009_proporcion_asientos_evento_2019": 56.25,
	 	 "D010_proporcion_asientos_media_evento_2019": 3.34,

	 	 "D011_ocupacion_asientos_evento_2018": 0.03,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.4,
	 	 "D013_ocupacion_asientos_evento_2019": 0.04,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.38,

	 	 "D015_precio_evento_2018": 0.0528,
	 	 "D016_precio_medio_evento_2018": 0.0531,
	 	 "D017_precio_evento_2019": 0.0544,
	 	 "D018_precio_medio_evento_2019": 0.0548,

	 	 "D019_novatos_evento_2018": 0.25,
	 	 "D020_novatos_media_evento_2018": 2.12,
	 	 "D021_novatos_evento_2019": 0.25,
	 	 "D022_novatos_media_evento_2019": 1.9,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-arenal sound-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.43, 'Proporción': 701.54}, 1: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2620.0}, 2: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.29, 'Proporción': 341.67}, 3: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.14, 'Proporción': 700.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-arenal sound-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.47, 'Proporción': 639.13}, 1: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.11, 'Proporción': 940.0}, 2: {'City': 'Petrer', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.24, 'Proporción': 421.43}, 4: {'City': "la Font d'en Carròs", 'Viajes en evento': 1, 'Viajes normal': 0.25, 'Proporción': 400.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-arenal sound-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.46, 'Proporción': 219.05}, 1: {'City': 'Gandia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 2: {'City': 'San Sebastián de los Reyes', 'Viajes en evento': 1, 'Viajes normal': 0.38, 'Proporción': 266.67}, 3: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.36, 'Proporción': 278.95}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-arenal sound-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.49, 'Proporción': 616.9}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_arenal sound_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_arenal sound_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BAD BUNNY', 'STEVE AOKI', 'DIMITRI VEGAS', 'JESS GLYNNE', 'JAX JONES', 'BERET', 'JAMES BLUNT', 'THE VAMPS', 'LOST FREQUENCIES', 'MARTIN JENSEN', 'ZOE', 'KIDD KEO', 'BAD GYAL', 'ROZALEN', 'TABURETE', 'CARLOS SADNESS', 'MALDITA NEREA', 'RECYCLED J', 'JUANCHO MARQUES', 'DAVID OTERO', 'AZEALIA BANKS', 'NATION', 'LA M.O.D.A', 'EL KANKA', 'LA PEGATINA', 'LA CASA AZUL', 'CRYSTAL FIGHTERS', 'SIDECARS', 'BELY BASARTE', 'KUVE', 'LA RAIZ', 'MUERDO', 'NIKONE', 'JUANITO MAKANDE', 'LAGRIMAS DE SANGRE', 'GREEN VALLEY', 'ALFRED GARCIA', 'FYAHBWOY', 'ARKANO', 'ZAZO & GXURMET', 'DORIAN', 'DELAPORTE', 'FRIDA', 'FULL', 'EL COLUMPIO ASESINO', 'SUBSHOCK & EVANGELOS', 'ELYELLA', 'MUEVELOREINA', 'THE ZOMBIE KIDS', 'DJ PLAN B', 'BLACK MAMBA', 'SEXY ZEBRAS', 'THE SOUND OF ARROWS', 'INNMIR', 'LES CASTIZOS', 'VENDETTA', 'BRIAN VAN ANDEL', 'LEY DJ', 'GOMAD!&MONSTER', 'EL TIO LA CARETA', 'LAURA PUT', 'THE TRIPLETZ', 'SPACE ELEPHANTS', 'WE ARE NOT DJ', 'KING KONG BOY', 'SIX TO FIX', 'KING JEDET', 'LITTLE BIGRELS B', 'SANDRO AVILA', 'MAADRAASSOO'],
	 	 "artistas_evento_2019":['FARRUKO', 'KAROL G', 'MARTIN GARRIX', 'ANITTA', 'MORAT', 'OLIVER HELDENS', 'BERET', 'C TANGANA', 'DON PATRICIO', 'THIRTY SECONDS TO MARS', 'BAD GYAL', 'CAT DEALERS', 'LOLA INDIGO', 'NATOS Y WAOR', 'DELLAFUENTE', 'VETUSTA MORLA', 'UMMET OZCAN', 'AYAX Y PROK', 'MAIKEL DELACALLE', 'SFDK', 'DANI FERNANDEZ', 'SOFIA ELLAR', 'MS NINA', 'SECOND', 'LA PEGATINA', 'ZAHARA', 'ARNAU GRISO', 'IVAN FERREIRO', 'FANGORIA', 'MUERDO', 'ZOO', 'TOTEKING', 'DORIAN', 'PIGNOISE', 'YAHAIRA', 'CAROLINA DURANTE', 'POL 3.14', 'DAVID RES', 'MANEMA', 'LADILLA RUSA', 'FLACA', 'LINQAE', 'RAYDEN', 'MELO MORENO', 'HEKTOR MASS', 'NANCYS RUBIAS', 'THE PARROTS', 'ADALA', 'BRISA FENOY', 'ZEAZIN', 'TODO EL RATO', 'LES CASTIZOS', 'FUSA NOCTA', 'LUC LOREN', 'BLACKMAMBA', 'BRIAN VAN ANDEL', 'GOMAD!&MONSTER', 'EL TIO LA CARETA', 'AGU LUKKE', 'JAKE RELLO', 'WE ARE NOT DJ', 'SPACE ELEPHANTS', 'PERANOIA', 'ICANTEACHYOY', 'BOCACHICCO', 'CORI MATIUS', 'KING JEDET', 'BORJA SANT', 'PIITY BERNAD', 'SAY YES DJ', 'HIDDN & DJ JUNIOR', 'SANDRO AVILA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_arenal sound_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_arenal sound_2019.jpg",
	 }, 
	 { 
	 	 "id": "4",
	 	 "nombre": "AZKENA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Vitoria-Gasteiz",
	 	 "fecha_ini_2018": "22-06-2018",
	 	 "fecha_fin_2018": "23-06-2018",
	 	 "fecha_ini_2019": "21-06-2019",
	 	 "fecha_fin_2019": "22-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 30.0,
	 	 "D002_evento_media_viajes_2018": 22.22,
	 	 "D003_evento_media_viajes_dia_semana_2018": 33.35,
	 	 "D004_evento_viajes_2019": 29.0,
	 	 "D005_evento_media_viajes_2019": 24.0,
	 	 "D006_evento_media_viajes_dia_semana_2019": 36.24,

	 	 "D007_proporcion_asientos_evento_2018": 21.48,
	 	 "D008_proporcion_asientos_media_evento_2018": 6.94,
	 	 "D009_proporcion_asientos_evento_2019": 18.4,
	 	 "D010_proporcion_asientos_media_evento_2019": 7.26,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.47,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.43,

	 	 "D015_precio_evento_2018": 0.0569,
	 	 "D016_precio_medio_evento_2018": 0.0564,
	 	 "D017_precio_evento_2019": 0.0543,
	 	 "D018_precio_medio_evento_2019": 0.0559,

	 	 "D019_novatos_evento_2018": 14.67,
	 	 "D020_novatos_media_evento_2018": 21.7,
	 	 "D021_novatos_evento_2019": 11.0,
	 	 "D022_novatos_media_evento_2019": 18.8,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-azkena-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 39, 'Viajes normal': 8.34, 'Proporción': 467.49}, 1: {'City': 'Burgos', 'Viajes en evento': 3, 'Viajes normal': 0.93, 'Proporción': 323.96}, 2: {'City': 'Pamplona', 'Viajes en evento': 11, 'Viajes normal': 1.74, 'Proporción': 633.28}, 3: {'City': 'Logroño', 'Viajes en evento': 4, 'Viajes normal': 0.49, 'Proporción': 808.89}, 4: {'City': 'Bilbao', 'Viajes en evento': 3, 'Viajes normal': 0.82, 'Proporción': 367.45}, 5: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.36, 'Proporción': 276.74}, 6: {'City': 'Donostia', 'Viajes en evento': 1, 'Viajes normal': 0.82, 'Proporción': 122.48}, 7: {'City': 'Barcelona', 'Viajes en evento': 3, 'Viajes normal': 0.8, 'Proporción': 375.31}, 8: {'City': 'Santander', 'Viajes en evento': 3, 'Viajes normal': 0.74, 'Proporción': 405.47}, 9: {'City': 'Valencia', 'Viajes en evento': 3, 'Viajes normal': 0.59, 'Proporción': 509.09}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-azkena-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 33, 'Viajes normal': 8.83, 'Proporción': 373.91}, 1: {'City': 'Burgos', 'Viajes en evento': 6, 'Viajes normal': 1.2, 'Proporción': 501.1}, 2: {'City': 'Pamplona', 'Viajes en evento': 9, 'Viajes normal': 2.58, 'Proporción': 349.43}, 3: {'City': 'Donostia', 'Viajes en evento': 5, 'Viajes normal': 0.99, 'Proporción': 504.98}, 4: {'City': 'Bilbao', 'Viajes en evento': 6, 'Viajes normal': 1.04, 'Proporción': 579.05}, 5: {'City': 'Valladolid', 'Viajes en evento': 2, 'Viajes normal': 0.53, 'Proporción': 376.4}, 6: {'City': 'Salamanca', 'Viajes en evento': 1, 'Viajes normal': 0.68, 'Proporción': 146.19}, 7: {'City': 'Logroño', 'Viajes en evento': 3, 'Viajes normal': 0.47, 'Proporción': 644.68}, 8: {'City': 'Bordeaux', 'Viajes en evento': 2, 'Viajes normal': 0.26, 'Proporción': 780.0}, 9: {'City': 'Barcelona', 'Viajes en evento': 2, 'Viajes normal': 0.69, 'Proporción': 290.06}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-azkena-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Burgos', 'Viajes en evento': 5, 'Viajes normal': 0.97, 'Proporción': 515.54}, 1: {'City': 'Madrid', 'Viajes en evento': 28, 'Viajes normal': 8.16, 'Proporción': 343.07}, 2: {'City': 'Donostia', 'Viajes en evento': 2, 'Viajes normal': 0.68, 'Proporción': 293.17}, 3: {'City': 'Valladolid', 'Viajes en evento': 4, 'Viajes normal': 0.42, 'Proporción': 944.0}, 4: {'City': 'Pamplona', 'Viajes en evento': 2, 'Viajes normal': 2.19, 'Proporción': 91.48}, 5: {'City': 'Bilbao', 'Viajes en evento': 3, 'Viajes normal': 0.85, 'Proporción': 353.23}, 6: {'City': 'León', 'Viajes en evento': 4, 'Viajes normal': 0.47, 'Proporción': 851.85}, 7: {'City': 'Logroño', 'Viajes en evento': 1, 'Viajes normal': 0.52, 'Proporción': 193.12}, 8: {'City': 'Santander', 'Viajes en evento': 4, 'Viajes normal': 0.73, 'Proporción': 545.17}, 9: {'City': 'Salamanca', 'Viajes en evento': 1, 'Viajes normal': 0.7, 'Proporción': 142.04}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-azkena-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Burgos', 'Viajes en evento': 3, 'Viajes normal': 1.32, 'Proporción': 228.0}, 1: {'City': 'Madrid', 'Viajes en evento': 37, 'Viajes normal': 8.41, 'Proporción': 439.72}, 2: {'City': 'Bilbao', 'Viajes en evento': 5, 'Viajes normal': 1.05, 'Proporción': 475.0}, 3: {'City': 'Pamplona', 'Viajes en evento': 10, 'Viajes normal': 2.77, 'Proporción': 361.47}, 4: {'City': 'Valladolid', 'Viajes en evento': 4, 'Viajes normal': 0.59, 'Proporción': 680.23}, 5: {'City': 'Salamanca', 'Viajes en evento': 7, 'Viajes normal': 0.71, 'Proporción': 988.24}, 6: {'City': 'Donostia', 'Viajes en evento': 6, 'Viajes normal': 0.9, 'Proporción': 665.69}, 7: {'City': 'Zaragoza', 'Viajes en evento': 3, 'Viajes normal': 0.36, 'Proporción': 822.86}, 8: {'City': 'León', 'Viajes en evento': 3, 'Viajes normal': 0.44, 'Proporción': 674.42}, 9: {'City': 'Santander', 'Viajes en evento': 1, 'Viajes normal': 0.67, 'Proporción': 149.47}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_azkena_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_azkena_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MORGAN', 'MT. JOY', 'WILCO', 'THE B-52S', 'GARBAYO', 'THE CULT', 'BLACKBERRY SMOKE', 'GIANTE', 'TESLA', 'NEKO CASE', 'STRAY CATS', 'THE LIVING END', 'GANG OF FOUR', 'CORROSION OF CONFORMITY', 'MEAT PUPPETS', 'LUCERO', 'THE HILLBILLY MOON EXPLOSION', 'GLASSJAW', 'STARCRAWLER', 'DEADBEATZ', 'TAMI NEILSON', 'INGLORIOUS', 'SURFBORT', 'DJ ARBIS', 'DEADLAND RITUAL', 'PHILIP H. ANSELMO & THE ILLEGALS', 'WOLF-A DJ', 'DANNY & THE CHAMPIONS OF THE WORLD', 'DOLLAR BILL & HIS ONE MAN BAND', 'THE COURETTES', 'LOS TORONTOS', 'PUSSYCAT & THE DIRTY JOHNSONS', 'LOS DUQUES DE MONTERREY', 'OUTGRAVITY', 'BLIND RAGE & VIOLENCE', 'TROPICAL F**K STORM', 'MICKY & THE BUZZ', 'THE CHEATING HEARTS', 'DJ BOB HOP', 'LAURA PREMINGER', 'DJ SEÑOR LOBO', 'CRISTINA SANDALIA', 'DEBORAH DEVOBOT', 'HOLY CUERVO DJ', 'DJ GAUTXOS', ''],
	 	 "artistas_evento_2019":['VAN MORRISON', 'TOM PETTY', 'JOAN JETT & THE BLACKHEARTS', 'RIVAL SONS', 'MOTT THE HOOPLE', 'BERRI TXARRAK', 'THE SHEEPDOGS', 'THE SHEEPDOGS', 'TURBONEGRO', 'MC50', 'CHRIS ROBINSON BROTHERHOOD', 'GIRLSCHOOL', 'GLUECIFER', 'THE DREAM SYNDICATE', 'TONI LOVE', 'MAN OR ASTRO-MAN?', 'NEBULA', 'BLOODSHOT BILL', 'DEAD CROSS', 'THE SOUL JACKETS', 'NUEVO CATECISMO CATÓLICO', 'THEE HYPNOTICS', 'SOL LAGARTO', 'LOROS OF ALTAMONT', 'WOLFWOLF', 'TUTAN COME ON', 'HOMBRE LOBO INTERNACIONAL', "MAMAGIGI'S", 'CARLOS VUDÚ', 'THE ALLNIGHTERS', "THE 5,6,7,8'S", 'THE SENSATIONAL SECOND COUSINS', 'DIEGO RJ', 'TOMAS COWABUNGA', 'JF LEON', 'THE BEASTS OF BOURBON', 'REVEREND BEAT MAN & SISTER NICOLE IZOBEL GARCIA', 'DEAD ELVIS & HIS ONE MAN GRAVE', 'HUGO RACE & MICHELANGELO RUSSO', 'THE JAMES TAYLOR QUARTET'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_azkena_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_azkena_2019.jpg",
	 }, 
	 { 
	 	 "id": "5",
	 	 "nombre": "BILBAO BBK LIVE",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Bilbao",
	 	 "fecha_ini_2018": "12-07-2018",
	 	 "fecha_fin_2018": "14-07-2018",
	 	 "fecha_ini_2019": "11-07-2019",
	 	 "fecha_fin_2019": "13-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 159.5,
	 	 "D002_evento_media_viajes_2018": 77.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 63.37,
	 	 "D004_evento_viajes_2019": 128.25,
	 	 "D005_evento_media_viajes_2019": 81.14,
	 	 "D006_evento_media_viajes_dia_semana_2019": 69.19,

	 	 "D007_proporcion_asientos_evento_2018": 50.08,
	 	 "D008_proporcion_asientos_media_evento_2018": 20.48,
	 	 "D009_proporcion_asientos_evento_2019": 47.48,
	 	 "D010_proporcion_asientos_media_evento_2019": 21.54,

	 	 "D011_ocupacion_asientos_evento_2018": 0.04,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.75,
	 	 "D013_ocupacion_asientos_evento_2019": 0.04,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.74,

	 	 "D015_precio_evento_2018": 0.0571,
	 	 "D016_precio_medio_evento_2018": 0.0558,
	 	 "D017_precio_evento_2019": 0.0586,
	 	 "D018_precio_medio_evento_2019": 0.0567,

	 	 "D019_novatos_evento_2018": 43.75,
	 	 "D020_novatos_media_evento_2018": 30.36,
	 	 "D021_novatos_evento_2019": 26.5,
	 	 "D022_novatos_media_evento_2019": 27.01,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-bilbao bbk live-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 184, 'Viajes normal': 21.23, 'Proporción': 866.8}, 1: {'City': 'Zaragoza', 'Viajes en evento': 31, 'Viajes normal': 2.46, 'Proporción': 1260.13}, 2: {'City': 'Gijón', 'Viajes en evento': 50, 'Viajes normal': 4.08, 'Proporción': 1226.42}, 3: {'City': 'Donostia', 'Viajes en evento': 42, 'Viajes normal': 4.12, 'Proporción': 1019.96}, 4: {'City': 'Barcelona', 'Viajes en evento': 59, 'Viajes normal': 2.52, 'Proporción': 2343.46}, 5: {'City': 'Pamplona', 'Viajes en evento': 31, 'Viajes normal': 6.53, 'Proporción': 474.42}, 6: {'City': 'Vitoria-Gasteiz', 'Viajes en evento': 6, 'Viajes normal': 0.85, 'Proporción': 706.45}, 7: {'City': 'Santander', 'Viajes en evento': 25, 'Viajes normal': 5.13, 'Proporción': 487.71}, 8: {'City': 'Logroño', 'Viajes en evento': 11, 'Viajes normal': 1.48, 'Proporción': 745.62}, 9: {'City': 'Burgos', 'Viajes en evento': 6, 'Viajes normal': 1.65, 'Proporción': 363.18}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-bilbao bbk live-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 176, 'Viajes normal': 21.58, 'Proporción': 815.61}, 1: {'City': 'Donostia', 'Viajes en evento': 28, 'Viajes normal': 5.06, 'Proporción': 553.81}, 2: {'City': 'Santander', 'Viajes en evento': 24, 'Viajes normal': 4.93, 'Proporción': 486.4}, 3: {'City': 'Gijón', 'Viajes en evento': 26, 'Viajes normal': 4.6, 'Proporción': 564.97}, 4: {'City': 'Pamplona', 'Viajes en evento': 32, 'Viajes normal': 6.86, 'Proporción': 466.57}, 5: {'City': 'Zaragoza', 'Viajes en evento': 16, 'Viajes normal': 2.69, 'Proporción': 595.58}, 6: {'City': 'Barcelona', 'Viajes en evento': 29, 'Viajes normal': 2.34, 'Proporción': 1238.35}, 7: {'City': 'Logroño', 'Viajes en evento': 11, 'Viajes normal': 1.41, 'Proporción': 778.74}, 8: {'City': 'Burgos', 'Viajes en evento': 5, 'Viajes normal': 2.15, 'Proporción': 232.06}, 9: {'City': 'Valencia', 'Viajes en evento': 19, 'Viajes normal': 1.85, 'Proporción': 1025.84}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-bilbao bbk live-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 83, 'Viajes normal': 20.98, 'Proporción': 395.6}, 1: {'City': 'Gijón', 'Viajes en evento': 38, 'Viajes normal': 3.93, 'Proporción': 967.22}, 2: {'City': 'Santander', 'Viajes en evento': 32, 'Viajes normal': 5.21, 'Proporción': 614.41}, 3: {'City': 'Donostia', 'Viajes en evento': 25, 'Viajes normal': 4.24, 'Proporción': 589.47}, 4: {'City': 'Pamplona', 'Viajes en evento': 33, 'Viajes normal': 6.5, 'Proporción': 508.01}, 5: {'City': 'Zaragoza', 'Viajes en evento': 10, 'Viajes normal': 2.55, 'Proporción': 392.43}, 6: {'City': 'Barcelona', 'Viajes en evento': 18, 'Viajes normal': 2.52, 'Proporción': 715.01}, 7: {'City': 'Logroño', 'Viajes en evento': 5, 'Viajes normal': 1.45, 'Proporción': 346.01}, 8: {'City': 'Torrelavega', 'Viajes en evento': 3, 'Viajes normal': 0.78, 'Proporción': 383.75}, 9: {'City': 'Valencia', 'Viajes en evento': 9, 'Viajes normal': 1.79, 'Proporción': 503.7}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-bilbao bbk live-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Gijón', 'Viajes en evento': 50, 'Viajes normal': 4.48, 'Proporción': 1116.83}, 1: {'City': 'Santander', 'Viajes en evento': 28, 'Viajes normal': 5.2, 'Proporción': 538.39}, 2: {'City': 'Madrid', 'Viajes en evento': 57, 'Viajes normal': 21.47, 'Proporción': 265.44}, 3: {'City': 'Donostia', 'Viajes en evento': 29, 'Viajes normal': 4.75, 'Proporción': 610.1}, 4: {'City': 'Pamplona', 'Viajes en evento': 41, 'Viajes normal': 6.78, 'Proporción': 604.46}, 5: {'City': 'Vitoria-Gasteiz', 'Viajes en evento': 3, 'Viajes normal': 1.04, 'Proporción': 289.52}, 6: {'City': 'Oviedo', 'Viajes en evento': 12, 'Viajes normal': 2.01, 'Proporción': 595.97}, 7: {'City': 'Zaragoza', 'Viajes en evento': 7, 'Viajes normal': 2.72, 'Proporción': 257.32}, 8: {'City': 'Logroño', 'Viajes en evento': 4, 'Viajes normal': 1.53, 'Proporción': 261.47}, 9: {'City': 'Burgos', 'Viajes en evento': 6, 'Viajes normal': 1.87, 'Proporción': 321.2}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_bilbao bbk live_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_bilbao bbk live_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BEJO', 'LUKIEK', 'JAMES', 'CHILDISH GAMBINO', 'MORGAN', 'GORILLAZ', 'FLORENCE + THE MACHINE', 'CIGARETTES AFTER SEX', 'ALT-J', 'DANILESS', 'THE XX', 'BAD GYAL', 'THE CHEMICAL BROTHERS', 'KING GIZZARD & THE LIZARD WIZARD', 'JUNGLE', 'HOT CHIP', 'NOEL GALLAGHER', 'SOPHIE', 'COOPER', 'MOUNT KIMBIE', 'PARQUET COURTS', 'DAVID BYRNE', 'MEUTE', 'PORCHES', 'NILUFER YANYA', 'YONAKA', 'FRIENDLY FIRES', 'TEMPLES', 'DJ FRA', 'ISEO & DODOSOUND', 'CAROLINA DURANTE', 'DJ GOO', 'MODESELEKTOR', 'GENGAHR', 'BENJAMIN CLEMENTINE', 'YOUNG FATHERS', 'GREENCLASS', 'MY BLOODY VALENTINE', 'FISCHERSPOONER', 'NEUMAN', 'DARK DJ', 'FAT WHITE FAMILY', 'BAD SOUNDS', 'TRIANGULO DE AMOR BIZARRO', 'DJ MATO', 'LETS EAT GRANDMA', 'GAZ COOMBES', 'MARIA ARNAL I MARCEL BAGES', 'JOHN TALABOT', 'NUNATAK', 'HUNEE', 'AVALON EMERSON', 'ANTHONY NAPLES', 'YOUNG MARCO', 'ED IS DEAD', 'SKATEBARD', 'ANTEROS', 'HAL9OOO', 'RURAL ZOMBIES', 'BEN UFO', 'INNMIR', 'ANEGURA', 'MELENAS', 'MEXRRISSEY', 'LAS ODIO', 'QUENTIN GAS & LOS ZINGAROS', 'THE ZEPHYR BONES', 'PROSUMER', 'ANA CURRA', 'CORA NOVOA', 'SAMO DJ', 'PET FENNEC', 'OPTIMO (ESPACIO)', 'BOMBA STEREO', 'EDU ANMU', 'BORROKAN', 'CECILIA PAYNE', 'ROCK NIGHTS', 'DEKMANTEL SOUNDSYSTE', 'JOE ILA RENA', 'DANIEL BAUGHMAN'],
	 	 "artistas_evento_2019":['DJAKE', 'SUA', 'THE STROKES', 'BROCKHAMPTON', 'ROSALIA', 'OLIVIA', 'NUEL', 'WEEZER', 'VINCE STAPLES', 'KHRUANGBIN', 'BELATZ', 'NILS FRAHM', 'VETUSTA MORLA', 'MIDLAND', 'THE BLAZE', 'GEORGIA', 'LIAM GALLAGHER', 'ANARI', 'PRINCESS NOKIA', 'YAEJI', 'MS NINA', 'HOT CHIP', 'NINJA', 'SECOND', 'NATHY PELUSO', 'NICOLA CRUZ', 'KERO KERO BONITO', 'IDLES', 'SLAVES', 'CUPIDO', 'THE VOIDZ', 'BIIG PIIG', 'HVOB', 'CECILIO G', 'CUT COPY', 'TODD TERJE', 'IDER', 'MR.K', 'MODESELEKTOR', 'DELAPORTE', 'DJOHNSTON', 'VIAGRA BOYS', 'SLEAFORD MODS', 'DJ SEINFELD', 'HONEY DIJON', 'LAURENT GARNIER', 'JOHN GRANT', 'LAS LIGAS MENORES', 'NADIA ROSE', 'SHAME', 'JONATHAN BREE', 'THE GOOD, THE BAD & THE QUEEN', 'PERRO', 'BAIUCA', 'OCTO OCTA', 'OMAR SOULEYMAN', 'FORT ROMEAU', 'MOXIE', 'BOY AZOOGA', 'CALA VENTO', 'JOHN TALABOT', 'JULIAN FALK', 'DERBY MOTORETA BURRITO KACHIMBA', 'MOURN', 'MISLAV', 'OSO LEONE', 'MUEVELOREINA', 'ZAZA', 'PONY BRAVO', 'ANTIFAN', 'OLATZ SALVADOR', 'SUEDE', 'THE PSYCHOTIC MONKS', 'PHUONG DAN', 'NOGEN', 'TAMA SUMO', 'COURTESY', 'ALVARO CABANA', 'SERRULLA', 'UNIFORMS', 'ERABATERA', 'KETIOV', 'ELENA COLOMBI', 'LESTER Y ELIZA', 'MIRAVALLES', "THOM YORKE TOMORROW'S MODERN BOXES", 'CASPER TIELROOIJ', 'ORPHEU THE WIZARD', 'BICEP DJ SET', 'MUEVELOKUMBIA', '2MANYDJS', 'ALICIA CARRERA', 'FENNA FICTION', 'SACHA MAMBO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_bilbao bbk live_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_bilbao bbk live_2019.jpg",
	 }, 
	 { 
	 	 "id": "6",
	 	 "nombre": "BULL MUSIC FESTIVAL",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Granada",
	 	 "fecha_ini_2018": "18-05-2018",
	 	 "fecha_fin_2018": "19-05-2018",
	 	 "fecha_ini_2019": "31-05-2019",
	 	 "fecha_fin_2019": "01-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 252.33333333333334,
	 	 "D002_evento_media_viajes_2018": 236.04,
	 	 "D003_evento_media_viajes_dia_semana_2018": 339.85,
	 	 "D004_evento_viajes_2019": 299.67,
	 	 "D005_evento_media_viajes_2019": 263.64,
	 	 "D006_evento_media_viajes_dia_semana_2019": 368.55,

	 	 "D007_proporcion_asientos_evento_2018": 25.27,
	 	 "D008_proporcion_asientos_media_evento_2018": 20.15,
	 	 "D009_proporcion_asientos_evento_2019": 26.14,
	 	 "D010_proporcion_asientos_media_evento_2019": 21.08,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.7,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.68,

	 	 "D015_precio_evento_2018": 0.0523,
	 	 "D016_precio_medio_evento_2018": 0.0532,
	 	 "D017_precio_evento_2019": 0.0542,
	 	 "D018_precio_medio_evento_2019": 0.0542,

	 	 "D019_novatos_evento_2018": 59.0,
	 	 "D020_novatos_media_evento_2018": 73.58,
	 	 "D021_novatos_evento_2019": 51.0,
	 	 "D022_novatos_media_evento_2019": 62.79,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-bull music festival-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Málaga', 'Viajes en evento': 127, 'Viajes normal': 38.5, 'Proporción': 329.86}, 1: {'City': 'Madrid', 'Viajes en evento': 85, 'Viajes normal': 21.29, 'Proporción': 399.19}, 2: {'City': 'Sevilla', 'Viajes en evento': 91, 'Viajes normal': 25.19, 'Proporción': 361.19}, 3: {'City': 'Jaén', 'Viajes en evento': 27, 'Viajes normal': 10.24, 'Proporción': 263.64}, 4: {'City': 'Almería', 'Viajes en evento': 54, 'Viajes normal': 12.27, 'Proporción': 439.96}, 5: {'City': 'Murcia', 'Viajes en evento': 35, 'Viajes normal': 10.97, 'Proporción': 319.06}, 6: {'City': 'Baza', 'Viajes en evento': 15, 'Viajes normal': 5.03, 'Proporción': 298.37}, 7: {'City': 'Córdoba', 'Viajes en evento': 31, 'Viajes normal': 8.04, 'Proporción': 385.78}, 8: {'City': 'Algeciras', 'Viajes en evento': 28, 'Viajes normal': 7.02, 'Proporción': 399.06}, 9: {'City': 'Marbella', 'Viajes en evento': 12, 'Viajes normal': 5.37, 'Proporción': 223.36}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-bull music festival-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Málaga', 'Viajes en evento': 122, 'Viajes normal': 40.19, 'Proporción': 303.58}, 1: {'City': 'Jaén', 'Viajes en evento': 49, 'Viajes normal': 13.47, 'Proporción': 363.85}, 2: {'City': 'Madrid', 'Viajes en evento': 98, 'Viajes normal': 20.91, 'Proporción': 468.65}, 3: {'City': 'Sevilla', 'Viajes en evento': 93, 'Viajes normal': 25.42, 'Proporción': 365.89}, 4: {'City': 'Almería', 'Viajes en evento': 44, 'Viajes normal': 13.04, 'Proporción': 337.52}, 5: {'City': 'Murcia', 'Viajes en evento': 39, 'Viajes normal': 10.63, 'Proporción': 366.83}, 6: {'City': 'Baza', 'Viajes en evento': 16, 'Viajes normal': 6.61, 'Proporción': 242.11}, 7: {'City': 'Marbella', 'Viajes en evento': 22, 'Viajes normal': 6.3, 'Proporción': 349.06}, 8: {'City': 'El Ejido', 'Viajes en evento': 21, 'Viajes normal': 5.39, 'Proporción': 389.74}, 9: {'City': 'Jerez de la Frontera', 'Viajes en evento': 15, 'Viajes normal': 6.53, 'Proporción': 229.61}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-bull music festival-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Málaga', 'Viajes en evento': 131, 'Viajes normal': 35.28, 'Proporción': 371.32}, 1: {'City': 'Sevilla', 'Viajes en evento': 81, 'Viajes normal': 24.59, 'Proporción': 329.34}, 2: {'City': 'Madrid', 'Viajes en evento': 39, 'Viajes normal': 22.2, 'Proporción': 175.65}, 3: {'City': 'Jaén', 'Viajes en evento': 30, 'Viajes normal': 10.44, 'Proporción': 287.25}, 4: {'City': 'Murcia', 'Viajes en evento': 34, 'Viajes normal': 11.16, 'Proporción': 304.61}, 5: {'City': 'Almería', 'Viajes en evento': 42, 'Viajes normal': 11.97, 'Proporción': 350.8}, 6: {'City': 'Córdoba', 'Viajes en evento': 43, 'Viajes normal': 8.01, 'Proporción': 536.58}, 7: {'City': 'Alicante', 'Viajes en evento': 18, 'Viajes normal': 5.25, 'Proporción': 342.72}, 8: {'City': 'Marbella', 'Viajes en evento': 24, 'Viajes normal': 5.19, 'Proporción': 462.03}, 9: {'City': 'Valencia', 'Viajes en evento': 18, 'Viajes normal': 7.99, 'Proporción': 225.31}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-bull music festival-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Málaga', 'Viajes en evento': 144, 'Viajes normal': 38.32, 'Proporción': 375.76}, 1: {'City': 'Jaén', 'Viajes en evento': 46, 'Viajes normal': 13.83, 'Proporción': 332.56}, 2: {'City': 'Sevilla', 'Viajes en evento': 81, 'Viajes normal': 25.71, 'Proporción': 315.05}, 3: {'City': 'Madrid', 'Viajes en evento': 53, 'Viajes normal': 21.68, 'Proporción': 244.42}, 4: {'City': 'Almería', 'Viajes en evento': 46, 'Viajes normal': 12.83, 'Proporción': 358.47}, 5: {'City': 'Murcia', 'Viajes en evento': 40, 'Viajes normal': 10.81, 'Proporción': 370.05}, 6: {'City': 'El Ejido', 'Viajes en evento': 24, 'Viajes normal': 4.8, 'Proporción': 500.41}, 7: {'City': 'Baza', 'Viajes en evento': 24, 'Viajes normal': 5.78, 'Proporción': 415.02}, 8: {'City': 'Jerez de la Frontera', 'Viajes en evento': 28, 'Viajes normal': 6.05, 'Proporción': 462.86}, 9: {'City': 'Córdoba', 'Viajes en evento': 33, 'Viajes normal': 8.37, 'Proporción': 394.34}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_bull music festival_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_bull music festival_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ARCO', 'BONI', 'JUNIOR', 'GRISES', 'NATOS Y WAOR', 'IZAL', 'AYAX Y PROK', 'SFDK', 'MACACO', 'JORIS VOORN', 'LA RAIZ', 'JUANITO MAKANDE', 'FYAHBWOY', 'LA HABITACION ROJA', 'BOIKOT', 'NOVEDADES CARMINHA', 'VARRY BRAVA', 'EL CANIJO DE JEREZ', 'TEQUILA', 'ANTILOPEZ', 'EL COLUMPIO ASESINO', 'ANDREA OLIVA', 'THE NEW RAEMON', "O'FUNK'ILLO", 'POPOF', 'MATTHIAS TANZMANN', 'MONIKA KRUSE', 'POLOCK', 'EL JOSE', 'TRASHTUCADA', 'CARLOTA', 'TAAO KROSS', 'FAUSTO TARANTO', 'ROSENDO', 'MANU GONZALEZ', 'DJ MURPHY', 'GONÇALO', 'PAULA CAZENAVE', 'LOS VECINOS DEL CALLEJON', 'PANGLOSS', 'DJ SILVER', 'GIOSER', 'KASE 0', 'INES Y LOS INESPERADOS', 'ALVARO SANCHEZ', 'ALTERMINDS', 'DJ VILLE ROWLAND'],
	 	 "artistas_evento_2019":['BERET', 'JUNIOR', 'NACH', 'C TANGANA', 'MAGO DE OZ', 'ROZALEN', 'AYAX Y PROK', 'INT', 'SFDK', 'LA PEGATINA', 'ARCE', 'GREEN VALLEY', 'JUANITO MAKANDE', 'ZOO', 'RAPSUSKLEI', 'MR. KILOMBO', 'CHARLOTTE DE WITTE', 'FUEL FANDANGO', 'DORIAN', 'DENNIS FERRER', 'MEDINA AZAHARA', 'NIÑOS MUTANTES', 'ANTILOPEZ', 'KOEL', 'LOS ZIGARROS', 'ESKORZO', 'CARMEN GEA', 'CHRISTIAN SMITH', 'FATIMA HAJJI', 'HOSSE', 'INNMIR', 'CJ JEFF', 'DAVIDE SQUILLACE', 'CARLOTA', 'SEX MUSEUM', 'CHELINA MANUHUTU', 'THE FIXED', 'REBEKA ARK', 'MARIEN NOVI', 'MARTINA KARSCH', 'JASON CENADOR', 'MALDITOS MEGIAS Y LOS DECADENTES', 'LORENA CABA', 'INES Y LOS INESPERADOS', 'ALVARO SANCHEZ'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_bull music festival_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_bull music festival_2019.jpg",
	 }, 
	 { 
	 	 "id": "7",
	 	 "nombre": "CABO DE PLATA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Cádiz",
	 	 "fecha_ini_2018": "05-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "24-07-2019",
	 	 "fecha_fin_2019": "27-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 48.75,
	 	 "D002_evento_media_viajes_2018": 39.08,
	 	 "D003_evento_media_viajes_dia_semana_2018": 36.31,
	 	 "D004_evento_viajes_2019": 48.8,
	 	 "D005_evento_media_viajes_2019": 43.72,
	 	 "D006_evento_media_viajes_dia_semana_2019": 40.27,

	 	 "D007_proporcion_asientos_evento_2018": 38.8,
	 	 "D008_proporcion_asientos_media_evento_2018": 19.21,
	 	 "D009_proporcion_asientos_evento_2019": 35.69,
	 	 "D010_proporcion_asientos_media_evento_2019": 21.0,

	 	 "D011_ocupacion_asientos_evento_2018": 0.03,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.81,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.83,

	 	 "D015_precio_evento_2018": 0.056,
	 	 "D016_precio_medio_evento_2018": 0.0555,
	 	 "D017_precio_evento_2019": 0.0582,
	 	 "D018_precio_medio_evento_2019": 0.0569,

	 	 "D019_novatos_evento_2018": 10.25,
	 	 "D020_novatos_media_evento_2018": 13.86,
	 	 "D021_novatos_evento_2019": 8.2,
	 	 "D022_novatos_media_evento_2019": 11.98,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-cabo de plata-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Sevilla', 'Viajes en evento': 79, 'Viajes normal': 16.52, 'Proporción': 478.35}, 1: {'City': 'Madrid', 'Viajes en evento': 21, 'Viajes normal': 2.46, 'Proporción': 855.1}, 2: {'City': 'Málaga', 'Viajes en evento': 22, 'Viajes normal': 4.85, 'Proporción': 453.42}, 3: {'City': 'Granada', 'Viajes en evento': 17, 'Viajes normal': 3.9, 'Proporción': 436.24}, 4: {'City': 'Algeciras', 'Viajes en evento': 11, 'Viajes normal': 2.61, 'Proporción': 422.19}, 5: {'City': 'Mérida', 'Viajes en evento': 4, 'Viajes normal': 0.42, 'Proporción': 944.12}, 6: {'City': 'Cáceres', 'Viajes en evento': 4, 'Viajes normal': 0.35, 'Proporción': 1141.57}, 7: {'City': 'Marbella', 'Viajes en evento': 3, 'Viajes normal': 0.77, 'Proporción': 388.24}, 8: {'City': 'Córdoba', 'Viajes en evento': 3, 'Viajes normal': 0.65, 'Proporción': 464.38}, 9: {'City': 'Salamanca', 'Viajes en evento': 3, 'Viajes normal': 0.45, 'Proporción': 661.98}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-cabo de plata-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Sevilla', 'Viajes en evento': 96, 'Viajes normal': 19.34, 'Proporción': 496.33}, 1: {'City': 'Madrid', 'Viajes en evento': 24, 'Viajes normal': 2.38, 'Proporción': 1009.44}, 2: {'City': 'Málaga', 'Viajes en evento': 27, 'Viajes normal': 5.26, 'Proporción': 513.64}, 3: {'City': 'Algeciras', 'Viajes en evento': 13, 'Viajes normal': 3.17, 'Proporción': 409.96}, 4: {'City': 'Mérida', 'Viajes en evento': 6, 'Viajes normal': 0.41, 'Proporción': 1476.11}, 5: {'City': 'Granada', 'Viajes en evento': 14, 'Viajes normal': 3.48, 'Proporción': 401.73}, 6: {'City': 'Badajoz', 'Viajes en evento': 10, 'Viajes normal': 0.49, 'Proporción': 2055.05}, 7: {'City': 'Marbella', 'Viajes en evento': 6, 'Viajes normal': 0.96, 'Proporción': 628.06}, 8: {'City': 'Tarifa', 'Viajes en evento': 6, 'Viajes normal': 0.37, 'Proporción': 1629.73}, 9: {'City': 'Jerez de la Frontera', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 10100.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-cabo de plata-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Sevilla', 'Viajes en evento': 60, 'Viajes normal': 16.99, 'Proporción': 353.23}, 1: {'City': 'Algeciras', 'Viajes en evento': 7, 'Viajes normal': 2.53, 'Proporción': 276.81}, 2: {'City': 'Málaga', 'Viajes en evento': 16, 'Viajes normal': 5.02, 'Proporción': 318.95}, 3: {'City': 'Granada', 'Viajes en evento': 17, 'Viajes normal': 3.96, 'Proporción': 429.42}, 4: {'City': 'Madrid', 'Viajes en evento': 9, 'Viajes normal': 2.66, 'Proporción': 338.22}, 5: {'City': 'Cáceres', 'Viajes en evento': 3, 'Viajes normal': 0.35, 'Proporción': 862.89}, 6: {'City': 'Mérida', 'Viajes en evento': 2, 'Viajes normal': 0.36, 'Proporción': 562.3}, 7: {'City': 'Huelva', 'Viajes en evento': 2, 'Viajes normal': 0.64, 'Proporción': 313.59}, 8: {'City': 'Salamanca', 'Viajes en evento': 1, 'Viajes normal': 0.4, 'Proporción': 250.88}, 9: {'City': 'León', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4200.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-cabo de plata-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Sevilla', 'Viajes en evento': 60, 'Viajes normal': 20.17, 'Proporción': 297.5}, 1: {'City': 'Málaga', 'Viajes en evento': 25, 'Viajes normal': 5.23, 'Proporción': 477.69}, 2: {'City': 'Algeciras', 'Viajes en evento': 13, 'Viajes normal': 3.13, 'Proporción': 415.56}, 3: {'City': 'Madrid', 'Viajes en evento': 14, 'Viajes normal': 2.45, 'Proporción': 570.59}, 4: {'City': 'Barbate', 'Viajes en evento': 4, 'Viajes normal': 0.05, 'Proporción': 7950.0}, 5: {'City': 'Granada', 'Viajes en evento': 9, 'Viajes normal': 3.78, 'Proporción': 237.84}, 6: {'City': 'Tarifa', 'Viajes en evento': 4, 'Viajes normal': 0.29, 'Proporción': 1358.49}, 7: {'City': 'Mérida', 'Viajes en evento': 3, 'Viajes normal': 0.52, 'Proporción': 571.62}, 8: {'City': 'Marbella', 'Viajes en evento': 5, 'Viajes normal': 0.75, 'Proporción': 666.67}, 9: {'City': 'Cáceres', 'Viajes en evento': 3, 'Viajes normal': 0.37, 'Proporción': 805.62}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_cabo de plata_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_cabo de plata_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BEJO', 'STEVE AOKI', 'BERET', 'NATOS Y WAOR', 'AYAX Y PROK', 'SFDK', 'RULO Y LA CONTRABANDA', 'LA RAIZ', 'MORODO', 'JUANITO MAKANDE', 'LAGRIMAS DE SANGRE', 'FYAHBWOY', 'TOTEKING', 'SOGE CULEBRA', 'DESAKATO', 'EL NIÑO DE LA HIPOTECA', 'ISEO & DODOSOUND', 'MIGUEL CAMPELLO', 'MUCHACHITO', 'EL CANIJO DE JEREZ', 'LITTLE PEPE', 'SHO-HAI', 'SINKOPE', "O'FUNK'ILLO", 'CAPITAN COBARDE', 'NARCO', 'TRASHTUCADA', 'BATRACIO'],
	 	 "artistas_evento_2019":['BEJO', 'MAKA', 'RELS B', 'NACH', 'DON PATRICIO', 'BAD GYAL', 'YELLOW CLAW', 'LOLA INDIGO', 'NATOS Y WAOR', 'MAIKEL DELACALLE', 'PENDULUM', 'JUANCHO MARQUES', 'RECYCLED J', 'KAYDEN CAIN', 'MS NINA', 'HARD GZ', 'ORISHAS', 'LA M.O.D.A', 'LA PEGATINA', 'ARCE', 'MUERDO', 'NIKONE', 'JUANITO MAKANDE', 'LA FUGA', 'FOYONE', 'RAPSUSKLEI', 'SOGE CULEBRA', 'MR.KILOMBO', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'GENERAL LEVY', 'ISEO & DODOSOUND', 'LOCOPLAYA', 'TALCO', 'WALLS', 'EL ULTIMO KE ZIERRE', 'FOREIGN BEGGARS', 'EL CANIJO DE JEREZ', 'DUBIOZA KOLEKTIV', 'SHO-HAI', 'ANTILOPEZ', 'FALSALARMA', 'GRITANDO EN SILENCIO', 'MARIO DIAZ', 'ESKORZO', "O'FUNK'ILLO", 'MACHETE EN BOCA', 'RAYDEN', 'CHE SUDAKA', 'LA REGADERA', 'HORA ZULU', 'BOHEMIAN BETYARS', 'MAFALDA', 'DJ SHEMMA', 'ALAMEDADOSOULNA', 'TODO EL RATO', 'MALAKA YOUTH', 'MUYAYO RIF', 'FERNANDO COSTA', 'BAYMONT BROSS Y ESTELA AMAYA', 'PAINT ANIMALS', 'DANTE Y BLAKE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_cabo de plata_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_cabo de plata_2019.jpg",
	 }, 
	 { 
	 	 "id": "8",
	 	 "nombre": "COOLTURAL FEST",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Almería",
	 	 "fecha_ini_2018": "17-08-2018",
	 	 "fecha_fin_2018": "19-08-2018",
	 	 "fecha_ini_2019": "15-08-2019",
	 	 "fecha_fin_2019": "18-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 95.25,
	 	 "D002_evento_media_viajes_2018": 64.46,
	 	 "D003_evento_media_viajes_dia_semana_2018": 93.38,
	 	 "D004_evento_viajes_2019": 111.2,
	 	 "D005_evento_media_viajes_2019": 72.28,
	 	 "D006_evento_media_viajes_dia_semana_2019": 64.65,

	 	 "D007_proporcion_asientos_evento_2018": 28.48,
	 	 "D008_proporcion_asientos_media_evento_2018": 17.15,
	 	 "D009_proporcion_asientos_evento_2019": 29.69,
	 	 "D010_proporcion_asientos_media_evento_2019": 17.68,

	 	 "D011_ocupacion_asientos_evento_2018": 0.03,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.65,
	 	 "D013_ocupacion_asientos_evento_2019": 0.04,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.62,

	 	 "D015_precio_evento_2018": 0.0565,
	 	 "D016_precio_medio_evento_2018": 0.0537,
	 	 "D017_precio_evento_2019": 0.0574,
	 	 "D018_precio_medio_evento_2019": 0.0551,

	 	 "D019_novatos_evento_2018": 35.75,
	 	 "D020_novatos_media_evento_2018": 22.41,
	 	 "D021_novatos_evento_2019": 27.6,
	 	 "D022_novatos_media_evento_2019": 19.64,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-cooltural fest-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Málaga', 'Viajes en evento': 78, 'Viajes normal': 12.04, 'Proporción': 647.78}, 1: {'City': 'Granada', 'Viajes en evento': 46, 'Viajes normal': 11.97, 'Proporción': 384.21}, 2: {'City': 'Murcia', 'Viajes en evento': 32, 'Viajes normal': 6.29, 'Proporción': 508.71}, 3: {'City': 'Madrid', 'Viajes en evento': 60, 'Viajes normal': 5.56, 'Proporción': 1078.29}, 4: {'City': 'Valencia', 'Viajes en evento': 15, 'Viajes normal': 3.08, 'Proporción': 486.96}, 5: {'City': 'Sevilla', 'Viajes en evento': 16, 'Viajes normal': 4.29, 'Proporción': 372.72}, 6: {'City': 'Alicante', 'Viajes en evento': 15, 'Viajes normal': 1.79, 'Proporción': 836.99}, 7: {'City': 'Jaén', 'Viajes en evento': 6, 'Viajes normal': 2.16, 'Proporción': 277.39}, 8: {'City': 'Motril', 'Viajes en evento': 3, 'Viajes normal': 0.86, 'Proporción': 347.92}, 9: {'City': 'Vera', 'Viajes en evento': 4, 'Viajes normal': 1.09, 'Proporción': 367.18}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-cooltural fest-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Granada', 'Viajes en evento': 54, 'Viajes normal': 12.83, 'Proporción': 420.82}, 1: {'City': 'Madrid', 'Viajes en evento': 85, 'Viajes normal': 6.17, 'Proporción': 1377.4}, 2: {'City': 'Málaga', 'Viajes en evento': 73, 'Viajes normal': 12.89, 'Proporción': 566.41}, 3: {'City': 'Murcia', 'Viajes en evento': 41, 'Viajes normal': 6.59, 'Proporción': 622.58}, 4: {'City': 'Jaén', 'Viajes en evento': 20, 'Viajes normal': 2.29, 'Proporción': 872.83}, 5: {'City': 'Valencia', 'Viajes en evento': 28, 'Viajes normal': 3.53, 'Proporción': 792.45}, 6: {'City': 'Sevilla', 'Viajes en evento': 44, 'Viajes normal': 4.66, 'Proporción': 944.42}, 7: {'City': 'Alicante', 'Viajes en evento': 13, 'Viajes normal': 1.53, 'Proporción': 848.69}, 8: {'City': 'Vera', 'Viajes en evento': 11, 'Viajes normal': 1.68, 'Proporción': 656.1}, 9: {'City': 'Motril', 'Viajes en evento': 3, 'Viajes normal': 0.99, 'Proporción': 301.99}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-cooltural fest-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Granada', 'Viajes en evento': 27, 'Viajes normal': 12.27, 'Proporción': 219.98}, 1: {'City': 'Málaga', 'Viajes en evento': 62, 'Viajes normal': 11.34, 'Proporción': 546.75}, 2: {'City': 'Murcia', 'Viajes en evento': 21, 'Viajes normal': 6.32, 'Proporción': 332.11}, 3: {'City': 'Madrid', 'Viajes en evento': 56, 'Viajes normal': 5.77, 'Proporción': 971.02}, 4: {'City': 'Valencia', 'Viajes en evento': 24, 'Viajes normal': 3.12, 'Proporción': 770.37}, 5: {'City': 'Jaén', 'Viajes en evento': 12, 'Viajes normal': 2.04, 'Proporción': 588.68}, 6: {'City': 'Motril', 'Viajes en evento': 8, 'Viajes normal': 0.68, 'Proporción': 1174.19}, 7: {'City': 'Alicante', 'Viajes en evento': 11, 'Viajes normal': 1.77, 'Proporción': 622.87}, 8: {'City': 'Sevilla', 'Viajes en evento': 14, 'Viajes normal': 4.24, 'Proporción': 330.21}, 9: {'City': 'Guadix', 'Viajes en evento': 2, 'Viajes normal': 0.23, 'Proporción': 872.29}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-cooltural fest-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Málaga', 'Viajes en evento': 97, 'Viajes normal': 12.25, 'Proporción': 792.05}, 1: {'City': 'Granada', 'Viajes en evento': 48, 'Viajes normal': 13.04, 'Proporción': 368.21}, 2: {'City': 'Murcia', 'Viajes en evento': 28, 'Viajes normal': 6.73, 'Proporción': 416.03}, 3: {'City': 'Madrid', 'Viajes en evento': 64, 'Viajes normal': 6.15, 'Proporción': 1040.82}, 4: {'City': 'Valencia', 'Viajes en evento': 26, 'Viajes normal': 3.31, 'Proporción': 785.99}, 5: {'City': 'Sevilla', 'Viajes en evento': 40, 'Viajes normal': 4.62, 'Proporción': 865.71}, 6: {'City': 'Jaén', 'Viajes en evento': 12, 'Viajes normal': 2.2, 'Proporción': 544.8}, 7: {'City': 'Motril', 'Viajes en evento': 7, 'Viajes normal': 0.85, 'Proporción': 824.81}, 8: {'City': 'Alicante', 'Viajes en evento': 10, 'Viajes normal': 1.56, 'Proporción': 642.4}, 9: {'City': 'Guadix', 'Viajes en evento': 8, 'Viajes normal': 0.41, 'Proporción': 1973.77}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_cooltural fest_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_cooltural fest_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MAGA', 'LOUDLY', 'DINERO', 'IZAL', 'CARLOS SADNESS', 'MARBAN', 'LA M.O.D.A', 'IVAN FERREIRO', 'SIDONIE', 'NIXON', 'VIVA SUECIA', 'VARRY BRAVA', 'SHINOVA', 'CARMEN BOZA', 'NEUMAN', 'ELYELLA', 'RUFUS T.FIREFLY', 'CHICA SOBRESALTO', 'NUNATAK', 'KITAI', 'SEXY ZEBRAS', 'AURORA & THE BETRAYERS', 'BADLANDS', 'EL IMPERIO DEL PERRO', 'MIREIA VILAR'],
	 	 "artistas_evento_2019":['TWISE', 'VETUSTA MORLA', 'SILOE', 'SECOND', 'ZAHARA', 'ARNAU GRISO', 'KUVE', 'MISS CAFFEINA', 'DEPEDRO', 'JENNY AND THE MEXICATS', 'NIXON', 'DORIAN', 'VIVA SUECIA', 'CAROLINA DURANTE', 'DELAPORTE', 'FULL', 'ELEFANTES', 'TU OTRA BONITA', 'EL COLUMPIO ASESINO', 'OJETE CALOR', 'CLUB DEL RIO', 'SIBERIA', 'LICHIS', 'VALIRA', 'PERRO', 'ALICE WONDER', 'ANGEL STANICH', 'TULSA', 'NUNATAK', 'KITAI', 'KILL ANISTON', 'LOS VINAGRES', 'PLAYA CUBERRIS', 'MUCHO', 'ARIZONA BABY', 'EMBUSTEROS', 'BRASI', 'NOPROCEDE', 'ELADIO Y LOS SERES QUERIDOS', 'JOANA SERRAT', 'THE CRAB APPLES', 'GIMNASTICA', 'LOS INVADERS', 'VUELO FIDJI', 'LOS MANISES', 'YO DIABLO', 'WI BOUZ', 'MR PERFUMME', 'BITCHES DEEJAIS', 'MONDO SONORO DJS', 'OCHOYMEDIO DJ'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_cooltural fest_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_cooltural fest_2019.jpg",
	 }, 
	 { 
	 	 "id": "9",
	 	 "nombre": "CRUILLA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Barcelona",
	 	 "fecha_ini_2018": "12-07-2018",
	 	 "fecha_fin_2018": "14-07-2018",
	 	 "fecha_ini_2019": "04-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 211.25,
	 	 "D002_evento_media_viajes_2018": 190.07,
	 	 "D003_evento_media_viajes_dia_semana_2018": 157.04,
	 	 "D004_evento_viajes_2019": 158.25,
	 	 "D005_evento_media_viajes_2019": 183.12,
	 	 "D006_evento_media_viajes_dia_semana_2019": 148.47,

	 	 "D007_proporcion_asientos_evento_2018": 40.45,
	 	 "D008_proporcion_asientos_media_evento_2018": 23.71,
	 	 "D009_proporcion_asientos_evento_2019": 33.22,
	 	 "D010_proporcion_asientos_media_evento_2019": 22.78,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.86,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.77,

	 	 "D015_precio_evento_2018": 0.0596,
	 	 "D016_precio_medio_evento_2018": 0.058,
	 	 "D017_precio_evento_2019": 0.0606,
	 	 "D018_precio_medio_evento_2019": 0.0603,

	 	 "D019_novatos_evento_2018": 74.25,
	 	 "D020_novatos_media_evento_2018": 91.49,
	 	 "D021_novatos_evento_2019": 21.5,
	 	 "D022_novatos_media_evento_2019": 77.17,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-cruilla-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 138, 'Viajes normal': 22.06, 'Proporción': 625.48}, 1: {'City': 'Valencia', 'Viajes en evento': 100, 'Viajes normal': 23.34, 'Proporción': 428.5}, 2: {'City': 'Lleida', 'Viajes en evento': 74, 'Viajes normal': 26.22, 'Proporción': 282.27}, 3: {'City': 'Perpignull', 'Viajes en evento': 43, 'Viajes normal': 7.61, 'Proporción': 564.77}, 4: {'City': 'Zaragoza', 'Viajes en evento': 24, 'Viajes normal': 4.96, 'Proporción': 483.98}, 5: {'City': 'Montpellier', 'Viajes en evento': 38, 'Viajes normal': 8.23, 'Proporción': 461.72}, 6: {'City': 'Tarragona', 'Viajes en evento': 7, 'Viajes normal': 1.74, 'Proporción': 401.73}, 7: {'City': 'Toulouse', 'Viajes en evento': 38, 'Viajes normal': 7.57, 'Proporción': 501.68}, 8: {'City': 'Lyon', 'Viajes en evento': 12, 'Viajes normal': 1.75, 'Proporción': 683.83}, 9: {'City': 'Castellón de la Plana ', 'Viajes en evento': 13, 'Viajes normal': 3.38, 'Proporción': 384.09}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-cruilla-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 94, 'Viajes normal': 19.59, 'Proporción': 479.87}, 1: {'City': 'Lleida', 'Viajes en evento': 75, 'Viajes normal': 25.85, 'Proporción': 290.19}, 2: {'City': 'Valencia', 'Viajes en evento': 86, 'Viajes normal': 20.96, 'Proporción': 410.3}, 3: {'City': 'Perpignull', 'Viajes en evento': 24, 'Viajes normal': 5.66, 'Proporción': 423.94}, 4: {'City': 'Zaragoza', 'Viajes en evento': 17, 'Viajes normal': 4.52, 'Proporción': 375.85}, 5: {'City': 'Montpellier', 'Viajes en evento': 27, 'Viajes normal': 6.05, 'Proporción': 446.09}, 6: {'City': 'Girona', 'Viajes en evento': 11, 'Viajes normal': 3.36, 'Proporción': 327.52}, 7: {'City': 'Tarragona', 'Viajes en evento': 6, 'Viajes normal': 1.56, 'Proporción': 384.0}, 8: {'City': 'Toulouse', 'Viajes en evento': 27, 'Viajes normal': 5.95, 'Proporción': 453.74}, 9: {'City': 'Castellón de la Plana ', 'Viajes en evento': 7, 'Viajes normal': 3.05, 'Proporción': 229.56}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-cruilla-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 138, 'Viajes normal': 23.98, 'Proporción': 575.59}, 1: {'City': 'Madrid', 'Viajes en evento': 153, 'Viajes normal': 23.15, 'Proporción': 660.81}, 2: {'City': 'Lleida', 'Viajes en evento': 102, 'Viajes normal': 26.91, 'Proporción': 379.09}, 3: {'City': 'Zaragoza', 'Viajes en evento': 27, 'Viajes normal': 4.68, 'Proporción': 577.33}, 4: {'City': 'Tarragona', 'Viajes en evento': 13, 'Viajes normal': 1.23, 'Proporción': 1054.44}, 5: {'City': 'Montpellier', 'Viajes en evento': 44, 'Viajes normal': 8.95, 'Proporción': 491.88}, 6: {'City': 'Perpignull', 'Viajes en evento': 29, 'Viajes normal': 8.71, 'Proporción': 332.86}, 7: {'City': 'Bilbao', 'Viajes en evento': 59, 'Viajes normal': 2.52, 'Proporción': 2343.46}, 8: {'City': 'Castellón de la Plana ', 'Viajes en evento': 22, 'Viajes normal': 3.15, 'Proporción': 699.48}, 9: {'City': 'Girona', 'Viajes en evento': 19, 'Viajes normal': 2.21, 'Proporción': 859.36}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-cruilla-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Lleida', 'Viajes en evento': 113, 'Viajes normal': 26.5, 'Proporción': 426.47}, 1: {'City': 'Valencia', 'Viajes en evento': 84, 'Viajes normal': 21.85, 'Proporción': 384.4}, 2: {'City': 'Madrid', 'Viajes en evento': 98, 'Viajes normal': 21.42, 'Proporción': 457.42}, 3: {'City': 'Zaragoza', 'Viajes en evento': 18, 'Viajes normal': 4.56, 'Proporción': 395.09}, 4: {'City': 'Pamplona', 'Viajes en evento': 44, 'Viajes normal': 2.67, 'Proporción': 1645.12}, 5: {'City': 'Perpignull', 'Viajes en evento': 13, 'Viajes normal': 6.64, 'Proporción': 195.74}, 6: {'City': 'Montpellier', 'Viajes en evento': 16, 'Viajes normal': 5.9, 'Proporción': 271.13}, 7: {'City': 'Tarragona', 'Viajes en evento': 5, 'Viajes normal': 1.2, 'Proporción': 417.58}, 8: {'City': 'Girona', 'Viajes en evento': 12, 'Viajes normal': 2.82, 'Proporción': 425.17}, 9: {'City': 'Castellón de la Plana ', 'Viajes en evento': 10, 'Viajes normal': 3.07, 'Proporción': 326.18}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_cruilla_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_cruilla_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['KYGO', 'BUNBURY', 'LP', 'BEN HOWARD', 'DAMIAN MARLEY', 'GILVERTO GIL', 'IZAL', 'BUGZY MALONE', 'SOJA', 'JESSIE WARE', 'CAMILLE', 'CHASE AND STATUS', 'THE ROOTS', 'N.E.R.D', 'JUSTICE', 'FATOUMATA DIAWARA', 'JACK WHITE', 'LA PEGATINA', 'LA M.O.D.A', 'DAVID BYRNE', 'LORI MEYERS', 'ORBITAL', 'JOAN DAUSA', 'ELS CATARRES', 'SEASICK STEVE', 'PROPHETS OF RAGE', 'BLAUMUT', 'ELEFANTES', 'WE THE LION', 'BELAKO', 'THE NEW RAEMON', 'RAMON MIRABET', 'THE LAST INTERNATIONALE', 'NURIA GRAHAM', 'NUDOZURDO', 'MI CAPITAN', 'JOANA SERRAT', 'LA FURIA DELS BAUS', 'BOMBA STEREO', 'AQUELARRE DE CERVERA'],
	 	 "artistas_evento_2019":['BLACK EYED PEAS', 'BASTILLE', 'DERMOT KENNEDY', 'AURORA', 'YEARS & YEARS', 'KYLIE MINOGUE', 'FOALS', 'JORGE DREXLER', 'MICHAEL KIWANUKA', 'NATOS Y WAOR', 'ZAZ', 'VETUSTA MORLA', 'PAROV STELAR', 'SEU JORGE', 'AYAX Y PROK', 'LOVE OF LESBIAN', 'GARBAGE', 'MARCELO D2', 'GANG OF YOUTHS', 'XOEL LOPEZ', 'DORIAN', 'TIKEN JAH FAKOLY', 'OUMOUS SANGARÉ', 'ISEO & DODOSOUND', 'BERRI TXARRAK', 'ELS PETS', 'LILDAMI', 'ELYELLA', 'EL PETIT DE CAL ERIL', 'CALA VENTO', 'DJ AMABLE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_cruilla_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_cruilla_2019.jpg",
	 }, 
	 { 
	 	 "id": "10",
	 	 "nombre": "DCODE",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Madrid",
	 	 "fecha_ini_2018": "08-09-2018",
	 	 "fecha_fin_2018": "09-09-2018",
	 	 "fecha_ini_2019": "07-09-2019",
	 	 "fecha_fin_2019": "08-09-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 1204.0,
	 	 "D002_evento_media_viajes_2018": 744.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 586.35,
	 	 "D004_evento_viajes_2019": 1244.67,
	 	 "D005_evento_media_viajes_2019": 804.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 580.58,

	 	 "D007_proporcion_asientos_evento_2018": 37.59,
	 	 "D008_proporcion_asientos_media_evento_2018": 26.7,
	 	 "D009_proporcion_asientos_evento_2019": 35.85,
	 	 "D010_proporcion_asientos_media_evento_2019": 28.01,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.8,

	 	 "D015_precio_evento_2018": 0.0503,
	 	 "D016_precio_medio_evento_2018": 0.0491,
	 	 "D017_precio_evento_2019": 0.051,
	 	 "D018_precio_medio_evento_2019": 0.0502,

	 	 "D019_novatos_evento_2018": 205.67,
	 	 "D020_novatos_media_evento_2018": 214.74,
	 	 "D021_novatos_evento_2019": 150.0,
	 	 "D022_novatos_media_evento_2019": 184.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-dcode-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 345, 'Viajes normal': 75.84, 'Proporción': 454.91}, 1: {'City': 'Murcia', 'Viajes en evento': 162, 'Viajes normal': 38.76, 'Proporción': 417.91}, 2: {'City': 'Albacete', 'Viajes en evento': 98, 'Viajes normal': 15.02, 'Proporción': 652.38}, 3: {'City': 'Sevilla', 'Viajes en evento': 75, 'Viajes normal': 21.66, 'Proporción': 346.26}, 4: {'City': 'Granada', 'Viajes en evento': 86, 'Viajes normal': 22.2, 'Proporción': 387.34}, 5: {'City': 'Zaragoza', 'Viajes en evento': 56, 'Viajes normal': 14.25, 'Proporción': 392.93}, 6: {'City': 'Burgos', 'Viajes en evento': 66, 'Viajes normal': 15.87, 'Proporción': 415.92}, 7: {'City': 'Alicante', 'Viajes en evento': 107, 'Viajes normal': 24.77, 'Proporción': 431.98}, 8: {'City': 'Salamanca', 'Viajes en evento': 118, 'Viajes normal': 25.28, 'Proporción': 466.78}, 9: {'City': 'Barcelona', 'Viajes en evento': 116, 'Viajes normal': 23.15, 'Proporción': 501.01}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-dcode-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 331, 'Viajes normal': 81.06, 'Proporción': 408.34}, 1: {'City': 'Murcia', 'Viajes en evento': 196, 'Viajes normal': 42.04, 'Proporción': 466.23}, 2: {'City': 'Granada', 'Viajes en evento': 91, 'Viajes normal': 21.68, 'Proporción': 419.66}, 3: {'City': 'Albacete', 'Viajes en evento': 86, 'Viajes normal': 15.92, 'Proporción': 540.05}, 4: {'City': 'Zaragoza', 'Viajes en evento': 66, 'Viajes normal': 15.0, 'Proporción': 440.0}, 5: {'City': 'Sevilla', 'Viajes en evento': 67, 'Viajes normal': 20.66, 'Proporción': 324.23}, 6: {'City': 'Burgos', 'Viajes en evento': 67, 'Viajes normal': 17.33, 'Proporción': 386.64}, 7: {'City': 'Alicante', 'Viajes en evento': 114, 'Viajes normal': 24.62, 'Proporción': 463.13}, 8: {'City': 'Salamanca', 'Viajes en evento': 126, 'Viajes normal': 28.08, 'Proporción': 448.73}, 9: {'City': 'Mérida', 'Viajes en evento': 42, 'Viajes normal': 11.9, 'Proporción': 352.8}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-dcode-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 337, 'Viajes normal': 74.61, 'Proporción': 451.69}, 1: {'City': 'Albacete', 'Viajes en evento': 165, 'Viajes normal': 13.73, 'Proporción': 1201.38}, 2: {'City': 'Murcia', 'Viajes en evento': 173, 'Viajes normal': 37.84, 'Proporción': 457.14}, 3: {'City': 'Granada', 'Viajes en evento': 82, 'Viajes normal': 21.29, 'Proporción': 385.1}, 4: {'City': 'Sevilla', 'Viajes en evento': 91, 'Viajes normal': 19.96, 'Proporción': 455.87}, 5: {'City': 'Salamanca', 'Viajes en evento': 157, 'Viajes normal': 25.84, 'Proporción': 607.69}, 6: {'City': 'Zaragoza', 'Viajes en evento': 65, 'Viajes normal': 13.25, 'Proporción': 490.59}, 7: {'City': 'Alicante', 'Viajes en evento': 111, 'Viajes normal': 24.09, 'Proporción': 460.82}, 8: {'City': 'Burgos', 'Viajes en evento': 73, 'Viajes normal': 15.47, 'Proporción': 471.84}, 9: {'City': 'Jaén', 'Viajes en evento': 52, 'Viajes normal': 9.58, 'Proporción': 542.6}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-dcode-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 334, 'Viajes normal': 80.15, 'Proporción': 416.73}, 1: {'City': 'Murcia', 'Viajes en evento': 177, 'Viajes normal': 41.38, 'Proporción': 427.73}, 2: {'City': 'Albacete', 'Viajes en evento': 134, 'Viajes normal': 14.29, 'Proporción': 937.54}, 3: {'City': 'Burgos', 'Viajes en evento': 74, 'Viajes normal': 16.48, 'Proporción': 449.02}, 4: {'City': 'Granada', 'Viajes en evento': 73, 'Viajes normal': 20.91, 'Proporción': 349.1}, 5: {'City': 'Salamanca', 'Viajes en evento': 159, 'Viajes normal': 28.94, 'Proporción': 549.34}, 6: {'City': 'Sevilla', 'Viajes en evento': 78, 'Viajes normal': 20.65, 'Proporción': 377.64}, 7: {'City': 'Alicante', 'Viajes en evento': 105, 'Viajes normal': 23.83, 'Proporción': 440.7}, 8: {'City': 'Zaragoza', 'Viajes en evento': 53, 'Viajes normal': 14.19, 'Proporción': 373.48}, 9: {'City': 'Mérida', 'Viajes en evento': 47, 'Viajes normal': 10.91, 'Proporción': 430.75}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_dcode_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_dcode_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['IMAGINE DRAGONS', 'BASTILLE', 'CLAIRO', 'JORJA SMITH', 'GRISES', 'SAM FENDER', 'IZAL', 'THE VACCINES', 'LA M.O.D.A', 'SIDONIE', 'KAKKMADDAFAKKA', 'ALBERT HAMMOND JR', 'VIVA SUECIA', 'BERRI TXARRAK', 'SHINOVA', 'TRIANGULO DE AMOR BIZARRO', 'VOLVER', 'NAT SIMONS', 'TERRY VS TORI', 'OCHOYMEDIO DJ'],
	 	 "artistas_evento_2019":['YUNGBLUD', 'TWO DOOR CINEMA CLUB', 'TOM ODELL', 'GERRY CINNAMON', 'AMARAL', 'CARAVAN PALACE', 'THE CARDIGANS', 'PICTURE THIS', 'KAISER CHIEFS', 'EELS', 'LA CASA AZUL', 'FIDLAR', 'MISS CAFFEINA', 'VIVA SUECIA', 'GABRIELA RICHARDSON', 'CAROLINA DURANTE', 'FULL', 'ST WOODS', 'ANTEROS', 'MIQUI BRIGHTSIDE', 'LEY DJ'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_dcode_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_dcode_2019.jpg",
	 }, 
	 { 
	 	 "id": "11",
	 	 "nombre": "DOWNLOAD FESTIVAL",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Madrid",
	 	 "fecha_ini_2018": "28-06-2018",
	 	 "fecha_fin_2018": "30-06-2018",
	 	 "fecha_ini_2019": "28-06-2019",
	 	 "fecha_fin_2019": "30-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 649.0,
	 	 "D002_evento_media_viajes_2018": 744.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 596.87,
	 	 "D004_evento_viajes_2019": 1074.0,
	 	 "D005_evento_media_viajes_2019": 804.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 973.71,

	 	 "D007_proporcion_asientos_evento_2018": 36.97,
	 	 "D008_proporcion_asientos_media_evento_2018": 26.7,
	 	 "D009_proporcion_asientos_evento_2019": 39.22,
	 	 "D010_proporcion_asientos_media_evento_2019": 28.01,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.8,

	 	 "D015_precio_evento_2018": 0.049,
	 	 "D016_precio_medio_evento_2018": 0.0491,
	 	 "D017_precio_evento_2019": 0.051,
	 	 "D018_precio_medio_evento_2019": 0.0502,

	 	 "D019_novatos_evento_2018": 211.5,
	 	 "D020_novatos_media_evento_2018": 214.74,
	 	 "D021_novatos_evento_2019": 220.0,
	 	 "D022_novatos_media_evento_2019": 184.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-download festival-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 309, 'Viajes normal': 75.84, 'Proporción': 407.45}, 1: {'City': 'Murcia', 'Viajes en evento': 146, 'Viajes normal': 38.76, 'Proporción': 376.63}, 2: {'City': 'Granada', 'Viajes en evento': 83, 'Viajes normal': 22.2, 'Proporción': 373.83}, 3: {'City': 'Zaragoza', 'Viajes en evento': 46, 'Viajes normal': 14.25, 'Proporción': 322.76}, 4: {'City': 'Burgos', 'Viajes en evento': 57, 'Viajes normal': 15.87, 'Proporción': 359.2}, 5: {'City': 'Alicante', 'Viajes en evento': 104, 'Viajes normal': 24.77, 'Proporción': 419.87}, 6: {'City': 'Albacete', 'Viajes en evento': 52, 'Viajes normal': 15.02, 'Proporción': 346.16}, 7: {'City': 'Sevilla', 'Viajes en evento': 57, 'Viajes normal': 21.66, 'Proporción': 263.15}, 8: {'City': 'Málaga', 'Viajes en evento': 39, 'Viajes normal': 12.3, 'Proporción': 317.18}, 9: {'City': 'Barcelona', 'Viajes en evento': 77, 'Viajes normal': 23.15, 'Proporción': 332.56}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-download festival-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 467, 'Viajes normal': 81.06, 'Proporción': 576.12}, 1: {'City': 'Murcia', 'Viajes en evento': 219, 'Viajes normal': 42.04, 'Proporción': 520.94}, 2: {'City': 'Zaragoza', 'Viajes en evento': 77, 'Viajes normal': 15.0, 'Proporción': 513.33}, 3: {'City': 'Sevilla', 'Viajes en evento': 112, 'Viajes normal': 20.66, 'Proporción': 541.99}, 4: {'City': 'Granada', 'Viajes en evento': 95, 'Viajes normal': 21.68, 'Proporción': 438.11}, 5: {'City': 'Burgos', 'Viajes en evento': 100, 'Viajes normal': 17.33, 'Proporción': 577.07}, 6: {'City': 'Albacete', 'Viajes en evento': 65, 'Viajes normal': 15.92, 'Proporción': 408.18}, 7: {'City': 'Mérida', 'Viajes en evento': 65, 'Viajes normal': 11.9, 'Proporción': 546.01}, 8: {'City': 'Alicante', 'Viajes en evento': 140, 'Viajes normal': 24.62, 'Proporción': 568.76}, 9: {'City': 'Salamanca', 'Viajes en evento': 123, 'Viajes normal': 28.08, 'Proporción': 438.05}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-download festival-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 389, 'Viajes normal': 74.61, 'Proporción': 521.39}, 1: {'City': 'Murcia', 'Viajes en evento': 195, 'Viajes normal': 37.84, 'Proporción': 515.28}, 2: {'City': 'Albacete', 'Viajes en evento': 66, 'Viajes normal': 13.73, 'Proporción': 480.55}, 3: {'City': 'Burgos', 'Viajes en evento': 130, 'Viajes normal': 15.47, 'Proporción': 840.27}, 4: {'City': 'Alicante', 'Viajes en evento': 146, 'Viajes normal': 24.09, 'Proporción': 606.12}, 5: {'City': 'Sevilla', 'Viajes en evento': 90, 'Viajes normal': 19.96, 'Proporción': 450.86}, 6: {'City': 'Granada', 'Viajes en evento': 109, 'Viajes normal': 21.29, 'Proporción': 511.9}, 7: {'City': 'Zaragoza', 'Viajes en evento': 55, 'Viajes normal': 13.25, 'Proporción': 415.12}, 8: {'City': 'Jaén', 'Viajes en evento': 61, 'Viajes normal': 9.58, 'Proporción': 636.51}, 9: {'City': 'Salamanca', 'Viajes en evento': 115, 'Viajes normal': 25.84, 'Proporción': 445.12}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-download festival-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 482, 'Viajes normal': 80.15, 'Proporción': 601.39}, 1: {'City': 'Murcia', 'Viajes en evento': 223, 'Viajes normal': 41.38, 'Proporción': 538.89}, 2: {'City': 'Burgos', 'Viajes en evento': 141, 'Viajes normal': 16.48, 'Proporción': 855.57}, 3: {'City': 'Sevilla', 'Viajes en evento': 107, 'Viajes normal': 20.65, 'Proporción': 518.04}, 4: {'City': 'Zaragoza', 'Viajes en evento': 103, 'Viajes normal': 14.19, 'Proporción': 725.82}, 5: {'City': 'Albacete', 'Viajes en evento': 91, 'Viajes normal': 14.29, 'Proporción': 636.69}, 6: {'City': 'Granada', 'Viajes en evento': 95, 'Viajes normal': 20.91, 'Proporción': 454.3}, 7: {'City': 'Mérida', 'Viajes en evento': 63, 'Viajes normal': 10.91, 'Proporción': 577.39}, 8: {'City': 'Alicante', 'Viajes en evento': 141, 'Viajes normal': 23.83, 'Proporción': 591.8}, 9: {'City': 'Salamanca', 'Viajes en evento': 136, 'Viajes normal': 28.94, 'Proporción': 469.87}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_download festival_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_download festival_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['GUNS N ROSES', 'OZZY OSBOURNE', 'AVENGED SEVENFOLD', 'SHINEDOWN', 'RISE AGAINST', 'VOLBEAT', 'MARILYN MANSON', 'BULLET FOR MY VALENTINE', 'JUDAS PRIEST', 'PARKWAY DRIVE', 'IN THIS MOMENT', 'A PERFECT CIRCLE', 'PENNY WISE', 'ARCH ENEMY', 'CARPERTER BRUT', 'THRICE', 'CLUTCH', 'UNDEROATH', 'KREATOR', 'MYRKUR', 'MOOSE BLOOD', 'TESSERACT', 'BARONES', 'CRIM', 'L7', 'BACKYARD BABIES', 'THE HELLACOPTERS', 'CROSSFAITH', 'CREEPER', 'MADBALL', 'CARCASS', 'ANKOR', 'EON', 'CATORCE', 'EGO KILL TALENT', 'IRON REAGAN', 'GALACTIC EMPIRE', "'77", 'EXHORDER', 'VIVA BELGRADO', 'CRISIX', 'ANGELUS APATRIDA', 'KAISER FRANZ JOSEF', 'APHONNIC', 'ADRIFT', 'THE PINK SLIPS', 'MELTDOWN', 'FOSCOR', 'ALTAIR', 'LEATHER HEART', 'TEETHING', 'HUMMANO', 'ANTI-KARAOKE', 'HOLY CUERVO DJ'],
	 	 "artistas_evento_2019":['SLIPKNOT', 'PAPA ROACH', 'TOOL', 'SABATON', 'SCORPIONS', 'SUM41', 'STONE TEMPLE PILOTS', 'AMON AMARTH', 'ENTER SHIKARI', 'ARCHITECTS', 'STATE CHAMPS', 'CHIDREN OF BODOM', 'RIVAL SONS', 'THE INTERRUPTERS', 'PERTURBATOR', 'ME FIRST AND THE GIMME GIMMES', 'FEVER', 'LEPROUS', 'BOSTON MANOR', 'SOULFLY', 'TURNSTILE', 'BERRI TXARRAK', 'TURBONEGRO', 'GRAVEYARD', 'BALA', 'RED FANG', 'COMEBACK KID', 'FIEND', 'BRASS AGAINST', 'BRUTUS', 'LIILY', 'THE BABOON SHOW', 'MANTAR', 'KONTRUST', 'TOUNDRA', 'LOVEBITES', 'ROLO TOMASSI', 'ANTEROS', 'MEGARA', 'VITA IMANA', 'THE WIZARDS', 'WILL HAVEN', 'CANNIBAL GRANDPA', 'EL ALTAR DEL HOLOCAUSTO', 'ASISTIS', 'LE TEMPS DU LOUP', 'BONES OF MINERVA', 'HIRANYA', 'WALKING WITH WOLVES', 'HOLY CUERVO DJ', 'BACK TO THE NINETIES DJ'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_download festival_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_download festival_2019.jpg",
	 }, 
	 { 
	 	 "id": "12",
	 	 "nombre": "DREAMBEACH",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Cuevas del Almanzora",
	 	 "fecha_ini_2018": "08-08-2018",
	 	 "fecha_fin_2018": "12-08-2018",
	 	 "fecha_ini_2019": "07-08-2019",
	 	 "fecha_fin_2019": "11-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 49.0,
	 	 "D002_evento_media_viajes_2018": 1.21,
	 	 "D003_evento_media_viajes_dia_semana_2018": 2.02,
	 	 "D004_evento_viajes_2019": 48.67,
	 	 "D005_evento_media_viajes_2019": 1.63,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.56,

	 	 "D007_proporcion_asientos_evento_2018": 37.89,
	 	 "D008_proporcion_asientos_media_evento_2018": 9.37,
	 	 "D009_proporcion_asientos_evento_2019": 36.19,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.99,

	 	 "D011_ocupacion_asientos_evento_2018": 1.05,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.65,
	 	 "D013_ocupacion_asientos_evento_2019": 1.19,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.53,

	 	 "D015_precio_evento_2018": 0.0581,
	 	 "D016_precio_medio_evento_2018": 0.057,
	 	 "D017_precio_evento_2019": 0.0622,
	 	 "D018_precio_medio_evento_2019": 0.0575,

	 	 "D019_novatos_evento_2018": 25.8,
	 	 "D020_novatos_media_evento_2018": 1.48,
	 	 "D021_novatos_evento_2019": 16.67,
	 	 "D022_novatos_media_evento_2019": 1.13,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-dreambeach-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 72, 'Viajes normal': 0.69, 'Proporción': 10440.0}, 1: {'City': 'Málaga', 'Viajes en evento': 22, 'Viajes normal': 0.46, 'Proporción': 4788.24}, 2: {'City': 'Almería', 'Viajes en evento': 21, 'Viajes normal': 0.17, 'Proporción': 12013.95}, 3: {'City': 'Murcia', 'Viajes en evento': 11, 'Viajes normal': 0.07, 'Proporción': 15100.0}, 4: {'City': 'Sevilla', 'Viajes en evento': 16, 'Viajes normal': 0.42, 'Proporción': 3800.0}, 5: {'City': 'Granada', 'Viajes en evento': 13, 'Viajes normal': 0.52, 'Proporción': 2520.0}, 6: {'City': 'Albacete', 'Viajes en evento': 4, 'Viajes normal': 0.04, 'Proporción': 10000.0}, 7: {'City': 'Alicante', 'Viajes en evento': 12, 'Viajes normal': 0.26, 'Proporción': 4533.33}, 8: {'City': 'Valencia', 'Viajes en evento': 8, 'Viajes normal': 0.22, 'Proporción': 3636.36}, 9: {'City': 'Fuenlabrada', 'Viajes en evento': 4, 'Viajes normal': 0.29, 'Proporción': 1400.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-dreambeach-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 62, 'Viajes normal': 0.76, 'Proporción': 8157.89}, 1: {'City': 'Murcia', 'Viajes en evento': 18, 'Viajes normal': 0.08, 'Proporción': 23220.0}, 2: {'City': 'Granada', 'Viajes en evento': 31, 'Viajes normal': 0.46, 'Proporción': 6787.93}, 3: {'City': 'Almería', 'Viajes en evento': 24, 'Viajes normal': 0.24, 'Proporción': 10050.0}, 4: {'City': 'Málaga', 'Viajes en evento': 33, 'Viajes normal': 0.47, 'Proporción': 7012.5}, 5: {'City': 'Sevilla', 'Viajes en evento': 13, 'Viajes normal': 0.33, 'Proporción': 3900.0}, 6: {'City': 'Albacete', 'Viajes en evento': 3, 'Viajes normal': 0.15, 'Proporción': 2040.0}, 7: {'City': 'Alicante', 'Viajes en evento': 11, 'Viajes normal': 0.35, 'Proporción': 3116.67}, 8: {'City': 'Córdoba', 'Viajes en evento': 6, 'Viajes normal': 0.19, 'Proporción': 3200.0}, 9: {'City': 'Jaén', 'Viajes en evento': 6, 'Viajes normal': 0.4, 'Proporción': 1500.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-dreambeach-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 34, 'Viajes normal': 1.12, 'Proporción': 3045.83}, 1: {'City': 'Málaga', 'Viajes en evento': 12, 'Viajes normal': 0.82, 'Proporción': 1459.46}, 2: {'City': 'Murcia', 'Viajes en evento': 6, 'Viajes normal': 0.1, 'Proporción': 5737.5}, 3: {'City': 'Sevilla', 'Viajes en evento': 6, 'Viajes normal': 0.72, 'Proporción': 833.33}, 4: {'City': 'Granada', 'Viajes en evento': 7, 'Viajes normal': 0.53, 'Proporción': 1324.32}, 5: {'City': 'Almería', 'Viajes en evento': 8, 'Viajes normal': 0.23, 'Proporción': 3424.56}, 6: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.16, 'Proporción': 620.0}, 7: {'City': 'Valencia', 'Viajes en evento': 6, 'Viajes normal': 0.22, 'Proporción': 2769.23}, 8: {'City': 'Alicante', 'Viajes en evento': 7, 'Viajes normal': 0.35, 'Proporción': 2006.67}, 9: {'City': 'Córdoba', 'Viajes en evento': 1, 'Viajes normal': 0.26, 'Proporción': 383.33}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-dreambeach-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 23, 'Viajes normal': 1.31, 'Proporción': 1751.14}, 1: {'City': 'Granada', 'Viajes en evento': 18, 'Viajes normal': 0.6, 'Proporción': 3000.0}, 2: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.2, 'Proporción': 1537.5}, 3: {'City': 'Sevilla', 'Viajes en evento': 3, 'Viajes normal': 0.54, 'Proporción': 553.85}, 4: {'City': 'Albacete', 'Viajes en evento': 3, 'Viajes normal': 0.13, 'Proporción': 2250.0}, 5: {'City': 'Málaga', 'Viajes en evento': 11, 'Viajes normal': 0.65, 'Proporción': 1683.67}, 6: {'City': 'Almería', 'Viajes en evento': 10, 'Viajes normal': 0.34, 'Proporción': 2918.92}, 7: {'City': 'Córdoba', 'Viajes en evento': 2, 'Viajes normal': 0.23, 'Proporción': 883.33}, 8: {'City': 'Alicante', 'Viajes en evento': 5, 'Viajes normal': 0.52, 'Proporción': 954.55}, 9: {'City': 'Roquetas de Mar', 'Viajes en evento': 2, 'Viajes normal': 0.22, 'Proporción': 920.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_dreambeach_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_dreambeach_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ALAN WALKER', 'MARTIN GARRIX', 'DON DIABLO', 'TIMMY TRUMPET', 'VINTAGE CULTURE', 'HARDWELL', 'KSHMR', 'AXWELL A INGROSSO', 'MANDY', 'NATOS Y WAOR', 'UMMET OZCAN', 'BRENNANT HEARTH', 'DA TWEEKAZ', 'VINI VICI', 'WILL SPARKS', 'MALA RODRIGUEZ', 'COONE', 'SFDK', 'NERVO', 'SUB ZERO PROJECT', 'SOLOMUN', 'FLUG', 'PATRICK TOPPING', 'BASS MODULATORS', 'CHOCOLATE PUMA', 'MACEO PLEX', 'TALE OF US', 'ZATOX', 'JAMIE JONES', 'THE MARTINEZ BROTHERS', 'KURA', 'AMELIE LENS', 'ADRIATIQUE', 'KORSAKOFF', 'ACE VENTURA', 'RECONDITE', 'VICTOR RUIZ', 'THA PLAYAH', 'OXIA', 'WALLY LOPEZ', 'PATRICE BAUMEL', 'KASE. O', 'GTA', 'BRIAN CROSS', 'AGENTS OF TIME', 'OTO', 'LOCO DICE', 'RICHIE HAWTIN', 'RICADO VILLALOBOS', 'CHRISTIAN SMITH', 'BLAWAN', 'ORBE', 'PACO OSUNA', 'OSCAR MULERO', 'JOSE AM', 'JP CANDELA', 'C SYSTEM', 'REBEKAH', 'EXIUM', 'VICENTE ONE MORE TIME', 'ARTURO GRAO', 'FRANK STORM', 'GONÇALO', 'MIGUEL GARJI', 'ANNA TUR', 'ASSAILANTS', 'HORACIO CRUZ', 'RAUL PACHECO', 'RUFO & YEYO'],
	 	 "artistas_evento_2019":['STEVE AOKI', 'ARMIN VAN BUUREN', 'ANIER', 'DON DIABLO', 'FISHER', 'BAD GYAL', 'MAHZA', 'W&W', 'PAUL KALKBRENNER', 'BORIS BREJCHA', 'FEDDE LE GRAND', 'COONE', 'INFECTED MUSHROOM', 'STEVE ANGELLO', 'KNIFE PARTY', 'JORIS VOORN', 'WARFACE', 'SUNNERY JAMES & RYAN MARCIANO', 'MAURICE WEST', 'ADAM BEYER', 'CHARLOTTE DE WITTE', 'THE MARTINEZ BROTHERS', 'HOLY GOOF', 'WADE', 'CLAUDE VONSTROKE', 'STEPHAN BODZIN', 'UMEK', 'NINA KRAVIZ', 'TODO HELDER', 'CHUS % CEBALLOS', 'EDU IMBERNON', 'BRIAN CROSS', 'DJ NANO', 'LOCO DICE', 'DIM3NSION', 'MARCO CAROLA', 'SETH TROXLER', 'STEVE LAWLER', 'JOSE AM', 'TODO EL RATO', 'FUSA NOCTA', 'GONÇALO', 'HORACIO CRUZ', 'TECHNIQUE INTERNATIONAL SOUND', 'RAUL PACHECO', 'TCHAMI X MALAA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_dreambeach_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_dreambeach_2019.jpg",
	 }, 
	 { 
	 	 "id": "13",
	 	 "nombre": "EXTREMÚSIKA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Cáceres",
	 	 "fecha_ini_2018": "12-04-2018",
	 	 "fecha_fin_2018": "14-04-2018",
	 	 "fecha_ini_2019": "01-05-2019",
	 	 "fecha_fin_2019": "04-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 69.75,
	 	 "D002_evento_media_viajes_2018": 51.79,
	 	 "D003_evento_media_viajes_dia_semana_2018": 44.56,
	 	 "D004_evento_viajes_2019": 69.8,
	 	 "D005_evento_media_viajes_2019": 60.13,
	 	 "D006_evento_media_viajes_dia_semana_2019": 53.48,

	 	 "D007_proporcion_asientos_evento_2018": 20.42,
	 	 "D008_proporcion_asientos_media_evento_2018": 10.44,
	 	 "D009_proporcion_asientos_evento_2019": 21.46,
	 	 "D010_proporcion_asientos_media_evento_2019": 12.67,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.5,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.54,

	 	 "D015_precio_evento_2018": 0.0524,
	 	 "D016_precio_medio_evento_2018": 0.0542,
	 	 "D017_precio_evento_2019": 0.0538,
	 	 "D018_precio_medio_evento_2019": 0.0547,

	 	 "D019_novatos_evento_2018": 17.5,
	 	 "D020_novatos_media_evento_2018": 25.51,
	 	 "D021_novatos_evento_2019": 5.8,
	 	 "D022_novatos_media_evento_2019": 21.19,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-extremúsika-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 90, 'Viajes normal': 12.68, 'Proporción': 709.66}, 1: {'City': 'Plasencia', 'Viajes en evento': 17, 'Viajes normal': 3.0, 'Proporción': 567.18}, 2: {'City': 'Mérida', 'Viajes en evento': 12, 'Viajes normal': 3.28, 'Proporción': 365.61}, 3: {'City': 'Salamanca', 'Viajes en evento': 33, 'Viajes normal': 4.47, 'Proporción': 737.6}, 4: {'City': 'Badajoz', 'Viajes en evento': 36, 'Viajes normal': 6.99, 'Proporción': 515.29}, 5: {'City': 'Sevilla', 'Viajes en evento': 21, 'Viajes normal': 5.68, 'Proporción': 369.4}, 6: {'City': 'Don Benito', 'Viajes en evento': 7, 'Viajes normal': 1.59, 'Proporción': 439.19}, 7: {'City': 'Valladolid', 'Viajes en evento': 3, 'Viajes normal': 0.81, 'Proporción': 370.17}, 8: {'City': 'Coria', 'Viajes en evento': 3, 'Viajes normal': 0.48, 'Proporción': 622.09}, 9: {'City': 'Navalmoral de la Mata', 'Viajes en evento': 2, 'Viajes normal': 0.69, 'Proporción': 291.2}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-extremúsika-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Salamanca', 'Viajes en evento': 35, 'Viajes normal': 4.45, 'Proporción': 786.4}, 1: {'City': 'Mérida', 'Viajes en evento': 22, 'Viajes normal': 3.54, 'Proporción': 621.56}, 2: {'City': 'Madrid', 'Viajes en evento': 100, 'Viajes normal': 14.15, 'Proporción': 706.65}, 3: {'City': 'Plasencia', 'Viajes en evento': 25, 'Viajes normal': 4.0, 'Proporción': 625.51}, 4: {'City': 'Badajoz', 'Viajes en evento': 42, 'Viajes normal': 8.81, 'Proporción': 476.6}, 5: {'City': 'Sevilla', 'Viajes en evento': 35, 'Viajes normal': 7.16, 'Proporción': 488.97}, 6: {'City': 'Valladolid', 'Viajes en evento': 6, 'Viajes normal': 0.95, 'Proporción': 633.45}, 7: {'City': 'Don Benito', 'Viajes en evento': 12, 'Viajes normal': 2.46, 'Proporción': 487.4}, 8: {'City': 'Villanueva de la Serena', 'Viajes en evento': 4, 'Viajes normal': 1.12, 'Proporción': 355.69}, 9: {'City': 'Navalmoral de la Mata', 'Viajes en evento': 1, 'Viajes normal': 0.51, 'Proporción': 196.73}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-extremúsika-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Mérida', 'Viajes en evento': 15, 'Viajes normal': 2.93, 'Proporción': 512.64}, 1: {'City': 'Badajoz', 'Viajes en evento': 28, 'Viajes normal': 6.36, 'Proporción': 440.14}, 2: {'City': 'Sevilla', 'Viajes en evento': 26, 'Viajes normal': 5.75, 'Proporción': 452.12}, 3: {'City': 'Salamanca', 'Viajes en evento': 21, 'Viajes normal': 5.33, 'Proporción': 394.09}, 4: {'City': 'Plasencia', 'Viajes en evento': 11, 'Viajes normal': 2.9, 'Proporción': 379.13}, 5: {'City': 'Madrid', 'Viajes en evento': 39, 'Viajes normal': 13.31, 'Proporción': 293.08}, 6: {'City': 'Don Benito', 'Viajes en evento': 9, 'Viajes normal': 1.35, 'Proporción': 664.33}, 7: {'City': 'Almendralejo', 'Viajes en evento': 5, 'Viajes normal': 0.65, 'Proporción': 765.96}, 8: {'City': 'Villanueva de la Serena', 'Viajes en evento': 8, 'Viajes normal': 0.99, 'Proporción': 807.25}, 9: {'City': 'Navalmoral de la Mata', 'Viajes en evento': 4, 'Viajes normal': 0.45, 'Proporción': 882.93}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-extremúsika-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Mérida', 'Viajes en evento': 28, 'Viajes normal': 3.03, 'Proporción': 925.22}, 1: {'City': 'Sevilla', 'Viajes en evento': 56, 'Viajes normal': 7.33, 'Proporción': 764.44}, 2: {'City': 'Badajoz', 'Viajes en evento': 44, 'Viajes normal': 8.29, 'Proporción': 531.0}, 3: {'City': 'Salamanca', 'Viajes en evento': 30, 'Viajes normal': 5.38, 'Proporción': 557.12}, 4: {'City': 'Plasencia', 'Viajes en evento': 26, 'Viajes normal': 3.85, 'Proporción': 674.98}, 5: {'City': 'Madrid', 'Viajes en evento': 56, 'Viajes normal': 14.62, 'Proporción': 383.16}, 6: {'City': 'Don Benito', 'Viajes en evento': 16, 'Viajes normal': 2.22, 'Proporción': 720.12}, 7: {'City': 'Almendralejo', 'Viajes en evento': 11, 'Viajes normal': 0.97, 'Proporción': 1134.38}, 8: {'City': 'Villanueva de la Serena', 'Viajes en evento': 13, 'Viajes normal': 1.09, 'Proporción': 1192.7}, 9: {'City': 'Coria', 'Viajes en evento': 6, 'Viajes normal': 0.47, 'Proporción': 1283.61}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_extremúsika_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_extremúsika_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['RAMONINOS', 'THE PRODIGY', 'MALA RODRIGUEZ', 'IRATXO', 'LA RAIZ', 'JUANITO MAKANDE', 'GREEN VALLEY', 'DESAKATO', 'TIERRA SANTA', 'MEDINA AZAHARA', 'TRIANA', 'ASLANDTICOS', 'GATILLAZO', 'ANTILOPEZ', 'SOZIEDAD ALKOHOLIKA', 'LENDAKARIS MUERTOS', 'SOBER', 'DEF CON DOS', 'EL DESVAN', 'RIOT PROPAGANDA', 'MARTIRES DEL COMPAS', 'HAMLET', 'PARABELLUM', 'BOCANADA', 'NARCO', 'ALAMEDA', 'INCONSCIENTES', 'ROSENDO', 'BOURBON KINGS', 'ULTIMA EXPERIENCIA', 'LOS JACOBOS', 'LOS MOJINOS ESCOCIOS', 'BELU SUSODICHO', 'DARKNESS BIZARRE', 'LADY JAQUE', 'CARLOS CHAQUEH', 'NOS NIÑOS OJOS ROJOS', 'OHNIA TRANSIT'],
	 	 "artistas_evento_2019":['KOMA', 'MAREA', 'C TANGANA', 'NATOS Y WAOR', 'LA PEGATINA', 'LULU', 'TOTEKING', 'DESAKATO', 'CELTAS CORTOS', 'EL NIÑO DE LA HIPOTECA', 'EL ULTIMO KE ZIERRE', 'SARATOGA', 'DUBIOZA KOLEKTIV', 'ASLANDTICOS', 'PORRETAS', 'SMOKING SOULS', 'LA SRA TOMASA', 'EL RENO RENARDO', 'HORA ZULU', 'RAMONCIN', 'BOCANADA', 'LUJURIA', 'ANGELUS APATRIDA', 'AMENOSKUARTO', 'SONOTONES', 'JOSETXU PIPERRAK', 'VUELO 505', 'CUATRO MADRES', 'LA IRA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_extremúsika_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_extremúsika_2019.jpg",
	 }, 
	 { 
	 	 "id": "14",
	 	 "nombre": "FESTARDOR",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "la Pobla de Vallbona",
	 	 "fecha_ini_2018": "11-10-2018",
	 	 "fecha_fin_2018": "13-10-2018",
	 	 "fecha_ini_2019": "09-10-2019",
	 	 "fecha_fin_2019": "12-10-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 11.333333333333334,
	 	 "D002_evento_media_viajes_2018": 0.34,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.05,
	 	 "D004_evento_viajes_2019": null,
	 	 "D005_evento_media_viajes_2019": 0.09,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.12,

	 	 "D007_proporcion_asientos_evento_2018": 40.0,
	 	 "D008_proporcion_asientos_media_evento_2018": 5.65,
	 	 "D009_proporcion_asientos_evento_2019": null,
	 	 "D010_proporcion_asientos_media_evento_2019": 1.14,

	 	 "D011_ocupacion_asientos_evento_2018": 1.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.6,
	 	 "D013_ocupacion_asientos_evento_2019": 0.0,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.09,

	 	 "D015_precio_evento_2018": 0.0557,
	 	 "D016_precio_medio_evento_2018": 0.0538,
	 	 "D017_precio_evento_2019": null,
	 	 "D018_precio_medio_evento_2019": 0.0583,

	 	 "D019_novatos_evento_2018": 2.33,
	 	 "D020_novatos_media_evento_2018": 0.82,
	 	 "D021_novatos_evento_2019": null,
	 	 "D022_novatos_media_evento_2019": 0.49,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festardor-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 9, 'Viajes normal': 0.24, 'Proporción': 3825.0}, 1: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.38, 'Proporción': 800.0}, 2: {'City': 'Murcia', 'Viajes en evento': 4, 'Viajes normal': 0.08, 'Proporción': 5200.0}, 3: {'City': 'San Vicente del Raspeig', 'Viajes en evento': 4, 'Viajes normal': 0.25, 'Proporción': 1600.0}, 4: {'City': 'Barcelona', 'Viajes en evento': 2, 'Viajes normal': 0.2, 'Proporción': 1000.0}, 5: {'City': 'Castellón de la Plana ', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1500.0}, 6: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Cerdanyola del Vallès', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}, 8: {'City': 'Tarragona', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Valladolid', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festardor-2019.html",
	 	 "tabla_origenes_evento_2019":{},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festardor-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festardor-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festardor_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festardor_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['TESA', 'NATOS Y WAOR', 'JUANCHO MARQUES', 'SFDK', 'SHARIF', 'LAGRIMAS DE SANGRE', 'RAPSUSKLEI', 'DESAKATO', 'EL NIÑO DE LA HIPOTECA', 'LOS DE MARRAS', 'BERRI TXARRAK', 'TALCO', 'EL ULTIMO KE ZIERRE', 'KOP', 'PORRETAS', 'GATILLAZO', 'SONS OF AGUIRRE', 'LENDAKARIS MUERTOS', 'SOZIEDAD ALKOHOLIKA', 'TREMENDA JAURIA', 'THE LOCOS', 'MALA REPUTACION', 'LOS BENITO', 'HORA ZULU', 'CACTUS', 'EL NOI DEL SUCRE', 'VADEBO', 'FUNKIWIS', 'NARCO', 'MAFALDA', 'ASPART', 'ATZEMBLA', 'EL TIO LA CARETA'],
	 	 "artistas_evento_2019":['NACH', 'SKA-P', 'SFDK', 'IRATXO', 'DUB INC', 'GREEN VALLEY', 'JUANITO MAKANDE', 'CELTAS CORTOS', 'DESAKATO', 'EL NIÑO DE LA HIPOTECA', 'LOS DE MARRAS', 'BUHOS', 'KOP', 'BENITO KAMELAS', 'SHO-HAI', 'SONS OF AGUIRRE', 'SOZIEDAD ALKOHOLIKA', 'GRITANDO EN SILENCIO', 'DEF CON DOS', 'KAOS URBANO', 'DESERIE', 'KONSUMO RESPETO', 'THE LOCOS', 'MALONDA', 'HORA ZULU', 'LA REGADERA', 'LA EXCEPCION', 'CHIMO BAYO', 'NARCO', 'FUNKIWIS', 'LA DESBANDADA', 'COMBO CALADA', 'EL TIO LA CARETA', 'HURACAN ROMANTICA', 'THE NIFTYS', 'MANTEQUILLA VOLADORA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festardor_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festardor_2019.jpg",
	 }, 
	 { 
	 	 "id": "15",
	 	 "nombre": "FESTIVAL DE LA LUZ",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Boimorto",
	 	 "fecha_ini_2018": "07-09-2018",
	 	 "fecha_fin_2018": "09-09-2018",
	 	 "fecha_ini_2019": "06-09-2019",
	 	 "fecha_fin_2019": "08-09-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 2.5,
	 	 "D002_evento_media_viajes_2018": 0.32,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.0,
	 	 "D004_evento_viajes_2019": 1.0,
	 	 "D005_evento_media_viajes_2019": 0.14,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.0,

	 	 "D007_proporcion_asientos_evento_2018": 61.54,
	 	 "D008_proporcion_asientos_media_evento_2018": 4.05,
	 	 "D009_proporcion_asientos_evento_2019": 18.18,
	 	 "D010_proporcion_asientos_media_evento_2019": 1.89,

	 	 "D011_ocupacion_asientos_evento_2018": 1.33,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.5,
	 	 "D013_ocupacion_asientos_evento_2019": 0.33,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.0,

	 	 "D015_precio_evento_2018": 0.0651,
	 	 "D016_precio_medio_evento_2018": 0.0631,
	 	 "D017_precio_evento_2019": 0.0456,
	 	 "D018_precio_medio_evento_2019": 0.0456,

	 	 "D019_novatos_evento_2018": 0.0,
	 	 "D020_novatos_media_evento_2018": 1.32,
	 	 "D021_novatos_evento_2019": 0.0,
	 	 "D022_novatos_media_evento_2019": 0.0,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festival de la luz-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Vigo', 'Viajes en evento': 2, 'Viajes normal': 1.0, 'Proporción': 200.0}, 1: {'City': 'Pontevedra', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 2: {'City': 'Caldas de Reis', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Poio', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festival de la luz-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Vigo', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}, 1: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festival de la luz-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Ourense', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Vigo', 'Viajes en evento': 2, 'Viajes normal': 1.0, 'Proporción': 200.0}, 2: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}, 3: {'City': 'Poio', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festival de la luz-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}, 1: {'City': 'Vigo', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festival de la luz_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festival de la luz_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['LOVE OF LESBIAN', 'KASE.O', 'ANDRES SUAREZ', 'RIFF RAFF', 'CONCHITA', 'SEGURIDAD SOCIAL', 'ESKORZO', 'MONARCHY', 'NUNATAK', 'SR CHINARRO', 'LOS VINAGRES', 'VARGAS BLUES BAND', 'OS RESENTIDOS', 'ELLIOTT MURPHY', 'JOSELE SANTIAGO', 'BRISA FENOY', 'MI CAPITAN', 'SEX MUSEUM', 'LOS MOTORES', 'JAVI MANEIRO'],
	 	 "artistas_evento_2019":['CEPEDA', 'MIKEL ERENTXUN', 'RIFF RAFF', 'LUZ CASAL', 'CELTAS CORTOS', 'DELAPORTE', 'GUSGUS', 'CARIÑO', 'DIAVLO', 'AMPARANOIA', 'ORQUESTA MONDRAGON', 'ANGEL STANICH', 'RAYDEN', 'PUTOCHINOMARICON', 'DEAD COMBO', 'KITAI', 'EGON SODA', 'ARIZONA BABY', 'FLORIDABLANCA', 'MOLINA MOLINA', 'PRESUMIDO', 'BFLECHA', 'BASANTA', 'BROKEN PEACH'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festival de la luz_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festival de la luz_2019.jpg",
	 }, 
	 { 
	 	 "id": "16",
	 	 "nombre": "FESTIVAL DE LOS SENTIDOS",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "La Roda",
	 	 "fecha_ini_2018": "15-06-2018",
	 	 "fecha_fin_2018": "17-06-2018",
	 	 "fecha_ini_2019": "14-06-2019",
	 	 "fecha_fin_2019": "16-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 5.5,
	 	 "D002_evento_media_viajes_2018": 1.41,
	 	 "D003_evento_media_viajes_dia_semana_2018": 3.1,
	 	 "D004_evento_viajes_2019": 6.0,
	 	 "D005_evento_media_viajes_2019": 2.29,
	 	 "D006_evento_media_viajes_dia_semana_2019": 4.76,

	 	 "D007_proporcion_asientos_evento_2018": 18.62,
	 	 "D008_proporcion_asientos_media_evento_2018": 0.97,
	 	 "D009_proporcion_asientos_evento_2019": 19.31,
	 	 "D010_proporcion_asientos_media_evento_2019": 1.77,

	 	 "D011_ocupacion_asientos_evento_2018": 0.05,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.19,
	 	 "D013_ocupacion_asientos_evento_2019": 0.05,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.16,

	 	 "D015_precio_evento_2018": 0.0561,
	 	 "D016_precio_medio_evento_2018": 0.0562,
	 	 "D017_precio_evento_2019": 0.054,
	 	 "D018_precio_medio_evento_2019": 0.0546,

	 	 "D019_novatos_evento_2018": 1.0,
	 	 "D020_novatos_media_evento_2018": 8.99,
	 	 "D021_novatos_evento_2019": 1.0,
	 	 "D022_novatos_media_evento_2019": 6.47,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festival de los sentidos-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 11, 'Viajes normal': 0.63, 'Proporción': 1745.65}, 1: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.11, 'Proporción': 951.43}, 2: {'City': 'Ciudad Real', 'Viajes en evento': 2, 'Viajes normal': 0.09, 'Proporción': 2347.37}, 3: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.15, 'Proporción': 1300.0}, 4: {'City': 'San Vicente del Raspeig', 'Viajes en evento': 3, 'Viajes normal': 0.17, 'Proporción': 1716.0}, 5: {'City': 'Almansa', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': 32800.0}, 6: {'City': 'Catarroja', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 7: {'City': 'Guadalajara', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 7400.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festival de los sentidos-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 15, 'Viajes normal': 1.06, 'Proporción': 1420.06}, 1: {'City': 'Murcia', 'Viajes en evento': 2, 'Viajes normal': 0.18, 'Proporción': 1127.66}, 2: {'City': 'Valencia', 'Viajes en evento': 3, 'Viajes normal': 0.19, 'Proporción': 1581.82}, 3: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 848.39}, 4: {'City': 'San Vicente del Raspeig', 'Viajes en evento': 1, 'Viajes normal': 0.18, 'Proporción': 554.55}, 5: {'City': 'Alcorcón', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 3300.0}, 6: {'City': 'Seseña', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festival de los sentidos-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 8, 'Viajes normal': 0.72, 'Proporción': 1112.64}, 1: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.13, 'Proporción': 779.55}, 2: {'City': 'Ciudad Real', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 2000.0}, 3: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.1, 'Proporción': 1041.94}, 4: {'City': 'Puertollano', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 3100.0}, 5: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.24, 'Proporción': 424.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festival de los sentidos-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 11, 'Viajes normal': 1.28, 'Proporción': 859.02}, 1: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 10133.33}, 2: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.27, 'Proporción': 364.47}, 3: {'City': 'Rivas-Vaciamadrid', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 2033.33}, 4: {'City': 'Valdemoro', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festival de los sentidos_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festival de los sentidos_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ROZALEN', 'IZAL', 'WAS', 'THE NOISES', 'VERMU', 'RUFUS T.FIREFLY', 'POLOCK', 'SIENNA', 'RUSOS BLANCOS', 'TRIANGULO INVERSO', 'JACOBO SERRA', 'RED GURD', 'SR ALIAGA', 'AMIGOS DEL ARTE'],
	 	 "artistas_evento_2019":['SECOND', 'LA CASA AZUL', 'KUVE', 'MISS CAFFEINA', 'NIXON', 'DORIAN', 'VEINTIUNO', 'ANGEL STANICH', 'RAYDEN', 'LA SONRISA DE JULIA', 'FLORIDABLANCA', 'JAMONES CON TACONES', 'SEET BARRIO', 'QUEEN UNIVERSE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festival de los sentidos_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festival de los sentidos_2019.jpg",
	 }, 
	 { 
	 	 "id": "17",
	 	 "nombre": "FESTIVAL DE ORTIGUEIRA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Ortigueira",
	 	 "fecha_ini_2018": "12-07-2018",
	 	 "fecha_fin_2018": "15-07-2018",
	 	 "fecha_ini_2019": "11-07-2019",
	 	 "fecha_fin_2019": "14-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 53.5,
	 	 "D002_evento_media_viajes_2018": 1.79,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.21,
	 	 "D004_evento_viajes_2019": 72.8,
	 	 "D005_evento_media_viajes_2019": 3.08,
	 	 "D006_evento_media_viajes_dia_semana_2019": 8.38,

	 	 "D007_proporcion_asientos_evento_2018": 65.24,
	 	 "D008_proporcion_asientos_media_evento_2018": 21.6,
	 	 "D009_proporcion_asientos_evento_2019": 63.26,
	 	 "D010_proporcion_asientos_media_evento_2019": 29.09,

	 	 "D011_ocupacion_asientos_evento_2018": 1.71,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.85,
	 	 "D013_ocupacion_asientos_evento_2019": 2.79,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.84,

	 	 "D015_precio_evento_2018": 0.0585,
	 	 "D016_precio_medio_evento_2018": 0.0564,
	 	 "D017_precio_evento_2019": 0.0604,
	 	 "D018_precio_medio_evento_2019": 0.0605,

	 	 "D019_novatos_evento_2018": 21.5,
	 	 "D020_novatos_media_evento_2018": 1.36,
	 	 "D021_novatos_evento_2019": 30.6,
	 	 "D022_novatos_media_evento_2019": 2.04,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festival de ortigueira-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 95, 'Viajes normal': 1.68, 'Proporción': 5671.64}, 1: {'City': 'Lugo', 'Viajes en evento': 14, 'Viajes normal': 0.19, 'Proporción': 7420.0}, 2: {'City': 'A Coruña', 'Viajes en evento': 17, 'Viajes normal': 0.27, 'Proporción': 6276.92}, 3: {'City': 'Santiago de Compostela', 'Viajes en evento': 17, 'Viajes normal': 0.4, 'Proporción': 4300.0}, 4: {'City': 'Gijón', 'Viajes en evento': 14, 'Viajes normal': 0.29, 'Proporción': 4900.0}, 5: {'City': 'Vigo', 'Viajes en evento': 11, 'Viajes normal': 0.44, 'Proporción': 2475.0}, 6: {'City': 'Ponferrada', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2600.0}, 7: {'City': 'Astorga', 'Viajes en evento': 1, 'Viajes normal': 0.11, 'Proporción': 900.0}, 8: {'City': 'Alcalá de Henares', 'Viajes en evento': 3, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Tordesillas', 'Viajes en evento': 2, 'Viajes normal': 0.29, 'Proporción': 700.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festival de ortigueira-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 145, 'Viajes normal': 2.6, 'Proporción': 5576.92}, 1: {'City': 'Lugo', 'Viajes en evento': 14, 'Viajes normal': 0.14, 'Proporción': 9800.0}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 34, 'Viajes normal': 0.49, 'Proporción': 6947.83}, 3: {'City': 'Gijón', 'Viajes en evento': 24, 'Viajes normal': 0.57, 'Proporción': 4200.0}, 4: {'City': 'Vigo', 'Viajes en evento': 12, 'Viajes normal': 0.38, 'Proporción': 3200.0}, 5: {'City': 'A Coruña', 'Viajes en evento': 15, 'Viajes normal': 0.22, 'Proporción': 6857.14}, 6: {'City': 'Ponferrada', 'Viajes en evento': 5, 'Viajes normal': 0.14, 'Proporción': 3666.67}, 7: {'City': 'Collado Villalba', 'Viajes en evento': 8, 'Viajes normal': 0.75, 'Proporción': 1066.67}, 8: {'City': 'Las Rozas de Madrid', 'Viajes en evento': 9, 'Viajes normal': 1.0, 'Proporción': 900.0}, 9: {'City': 'León', 'Viajes en evento': 6, 'Viajes normal': 1.0, 'Proporción': 600.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festival de ortigueira-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 39, 'Viajes normal': 2.89, 'Proporción': 1350.0}, 1: {'City': 'Lugo', 'Viajes en evento': 4, 'Viajes normal': 0.33, 'Proporción': 1200.0}, 2: {'City': 'A Coruña', 'Viajes en evento': 9, 'Viajes normal': 0.48, 'Proporción': 1890.0}, 3: {'City': 'Santiago de Compostela', 'Viajes en evento': 11, 'Viajes normal': 0.44, 'Proporción': 2475.0}, 4: {'City': 'Vigo', 'Viajes en evento': 4, 'Viajes normal': 1.0, 'Proporción': 400.0}, 5: {'City': 'Gijón', 'Viajes en evento': 4, 'Viajes normal': 0.77, 'Proporción': 520.0}, 6: {'City': 'Pontevedra', 'Viajes en evento': 3, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Ponferrada', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2800.0}, 8: {'City': 'Móstoles', 'Viajes en evento': 3, 'Viajes normal': 1.67, 'Proporción': 180.0}, 9: {'City': 'Las Rozas de Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.25, 'Proporción': 1200.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festival de ortigueira-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 66, 'Viajes normal': 3.51, 'Proporción': 1880.23}, 1: {'City': 'Santiago de Compostela', 'Viajes en evento': 15, 'Viajes normal': 0.8, 'Proporción': 1875.0}, 2: {'City': 'Lugo', 'Viajes en evento': 5, 'Viajes normal': 0.26, 'Proporción': 1928.57}, 3: {'City': 'Pontevedra', 'Viajes en evento': 6, 'Viajes normal': 0.83, 'Proporción': 720.0}, 4: {'City': 'Vigo', 'Viajes en evento': 3, 'Viajes normal': 0.8, 'Proporción': 375.0}, 5: {'City': 'Ponferrada', 'Viajes en evento': 2, 'Viajes normal': 0.15, 'Proporción': 1320.0}, 6: {'City': 'León', 'Viajes en evento': 3, 'Viajes normal': 1.2, 'Proporción': 250.0}, 7: {'City': 'Gijón', 'Viajes en evento': 2, 'Viajes normal': 1.17, 'Proporción': 171.43}, 8: {'City': 'Tordesillas', 'Viajes en evento': 1, 'Viajes normal': 0.29, 'Proporción': 350.0}, 9: {'City': 'A Coruña', 'Viajes en evento': 5, 'Viajes normal': 0.42, 'Proporción': 1176.47}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festival de ortigueira_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festival de ortigueira_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['RUBEN ALBA', 'KILA', 'LUNASA', 'BELTAINE', 'MILLARDOIRO', 'CELTIC SOCIAL CLUB', "D'ABAIXO", 'THE TAVERNERS', 'BRUMAFOLK', 'THE NATIONAL YOUTH PIPE BAND OF SCOTLAND', 'YVES LAMBERT PROJECT', 'GABRIEL G DIGES', 'AIRIÑOS DO FENE'],
	 	 "artistas_evento_2019":['FLOOK', 'BELTAINE', 'GWENDAL', 'TANXUNGUEIRAS', 'PROJECT SMOK', 'SHOOGLENIFTY', 'MOVING HEARTS', 'LEILIA', 'STOLEN NOTES', 'PEOPLES FORD BOGHALL', 'ANXO LORENZO BAND', 'BAGAD KELENN PONDI', 'DOMINIC GRAHAM SCHOOL OF IRISH DANCE', 'A.C.F. O FIADEIRO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festival de ortigueira_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festival de ortigueira_2019.jpg",
	 }, 
	 { 
	 	 "id": "18",
	 	 "nombre": "FESTIVAL GIGANTE",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Guadalajara",
	 	 "fecha_ini_2018": "30-08-2018",
	 	 "fecha_fin_2018": "01-09-2018",
	 	 "fecha_ini_2019": "29-08-2019",
	 	 "fecha_fin_2019": "31-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 4.0,
	 	 "D002_evento_media_viajes_2018": 1.84,
	 	 "D003_evento_media_viajes_dia_semana_2018": 1.25,
	 	 "D004_evento_viajes_2019": 4.67,
	 	 "D005_evento_media_viajes_2019": 2.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 1.58,

	 	 "D007_proporcion_asientos_evento_2018": 17.2,
	 	 "D008_proporcion_asientos_media_evento_2018": 0.92,
	 	 "D009_proporcion_asientos_evento_2019": 9.6,
	 	 "D010_proporcion_asientos_media_evento_2019": 1.23,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.16,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.14,

	 	 "D015_precio_evento_2018": 0.0547,
	 	 "D016_precio_medio_evento_2018": 0.056,
	 	 "D017_precio_evento_2019": 0.0564,
	 	 "D018_precio_medio_evento_2019": 0.0552,

	 	 "D019_novatos_evento_2018": 2.33,
	 	 "D020_novatos_media_evento_2018": 12.84,
	 	 "D021_novatos_evento_2019": 3.33,
	 	 "D022_novatos_media_evento_2019": 11.68,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festival gigante-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Zaragoza', 'Viajes en evento': 5, 'Viajes normal': 0.32, 'Proporción': 1573.28}, 1: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 834.15}, 2: {'City': 'Bailén', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4800.0}, 3: {'City': 'Marbella', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 537.14}, 5: {'City': 'Badajoz', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 2966.67}, 6: {'City': 'Ferrol', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Moncofa', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festival gigante-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Zaragoza', 'Viajes en evento': 7, 'Viajes normal': 0.43, 'Proporción': 1612.12}, 1: {'City': 'Lleida', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1338.1}, 2: {'City': 'Córdoba', 'Viajes en evento': 1, 'Viajes normal': 0.09, 'Proporción': 1100.0}, 3: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.32, 'Proporción': 309.86}, 4: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.24, 'Proporción': 413.24}, 5: {'City': 'Logroño', 'Viajes en evento': 1, 'Viajes normal': 0.14, 'Proporción': 738.89}, 6: {'City': 'Salou', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1275.0}, 7: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.24, 'Proporción': 420.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festival gigante-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.33, 'Proporción': 598.36}, 1: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1738.1}, 2: {'City': 'Albacete', 'Viajes en evento': 4, 'Viajes normal': 0.37, 'Proporción': 1072.53}, 3: {'City': 'Oropesa del Mar', 'Viajes en evento': 2, 'Viajes normal': 0.14, 'Proporción': 1466.67}, 4: {'City': 'Castellón de la Plana ', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2700.0}, 5: {'City': 'Pamplona', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 1892.86}, 6: {'City': 'Cuenca', 'Viajes en evento': 1, 'Viajes normal': 0.13, 'Proporción': 765.22}, 7: {'City': 'Talavera de la Reina', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4175.0}, 8: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.24, 'Proporción': 423.81}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festival gigante-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.58, 'Proporción': 343.5}, 1: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.25, 'Proporción': 404.69}, 2: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.3, 'Proporción': 338.46}, 3: {'City': 'Cuenca', 'Viajes en evento': 2, 'Viajes normal': 0.08, 'Proporción': 2500.0}, 4: {'City': 'Jaén', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1790.0}, 5: {'City': 'Granada', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1278.57}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festival gigante_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festival gigante_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BUNBURY', 'LEONE', 'DESPISTAOS', 'LA M.O.D.A', 'EL KANKA', 'KUVE', 'NIXON', 'DORIAN', 'VIVA SUECIA', 'NOVEDADES CARMINHA', 'POL 3.14', 'FULL', 'CORREOS', 'ELEFANTES', 'ELYELLA', 'RUBEN POZO', 'RON GALLO', 'AMATRIA', 'RUFUS T.FIREFLY', 'ANGEL STANICH', 'KITAI', 'LAGARTIJA NICK', 'EMBUSTEROS', 'SEXY ZEBRAS', 'DESVARIADOS', 'VILLANUEVA', 'THE GROOVES', 'PASAJERO', 'TEXXCOCO', 'GOTELE', 'SAID MUTI', 'AGORAPHOBIA', 'LUIS BREA Y EL MIEDO'],
	 	 "artistas_evento_2019":['ROZALEN', 'CARLOS SADNESS', 'SECOND', 'ZAHARA', 'SIDECARS', 'COOPER', 'LA HABITACION ROJA', 'DEPEDRO', 'NIXON', 'GLOTON', 'WE ARE SCIENTISTS', 'SHINOVA', 'COLECTIVO PANAMERA', 'LADILLA RUSA', 'BELUGA', 'RAYDEN', 'LOS VINAGRES', 'MONTERROSA', 'EMBUSTEROS', 'FLORIDABLANCA', 'WISEMEN PROJECT', 'GLITCH GYALS', 'DETERGENTE LIQUIDO', 'LAS CHILLERS', 'JULIETA 21', 'EVA RYJLEN', 'OCTUBRE POLAR', 'NOVIO CABALLO', 'TREE HOUSE', 'THE MORGANS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festival gigante_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festival gigante_2019.jpg",
	 }, 
	 { 
	 	 "id": "19",
	 	 "nombre": "FESTIVERN",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Tavernes de la Valldigna",
	 	 "fecha_ini_2018": "29-12-2018",
	 	 "fecha_fin_2018": "31-12-2018",
	 	 "fecha_ini_2019": "28-12-2019",
	 	 "fecha_fin_2019": "31-12-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 6.5,
	 	 "D002_evento_media_viajes_2018": 0.67,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.6,
	 	 "D004_evento_viajes_2019": null,
	 	 "D005_evento_media_viajes_2019": 0.96,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.54,

	 	 "D007_proporcion_asientos_evento_2018": 39.5,
	 	 "D008_proporcion_asientos_media_evento_2018": 4.68,
	 	 "D009_proporcion_asientos_evento_2019": null,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.14,

	 	 "D011_ocupacion_asientos_evento_2018": 0.22,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.51,
	 	 "D013_ocupacion_asientos_evento_2019": 0.0,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.53,

	 	 "D015_precio_evento_2018": 0.0555,
	 	 "D016_precio_medio_evento_2018": 0.0539,
	 	 "D017_precio_evento_2019": null,
	 	 "D018_precio_medio_evento_2019": 0.0549,

	 	 "D019_novatos_evento_2018": 3.5,
	 	 "D020_novatos_media_evento_2018": 1.66,
	 	 "D021_novatos_evento_2019": null,
	 	 "D022_novatos_media_evento_2019": 1.32,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-festivern-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Barcelona', 'Viajes en evento': 7, 'Viajes normal': 0.61, 'Proporción': 1145.45}, 1: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.04, 'Proporción': 5475.0}, 2: {'City': 'Castellón de la Plana ', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Alcoi', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 2200.0}, 4: {'City': 'Madrid', 'Viajes en evento': 4, 'Viajes normal': 0.3, 'Proporción': 1340.74}, 5: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1200.0}, 6: {'City': 'Girona', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Lleida', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 8: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1216.67}, 9: {'City': 'Cambrils', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-festivern-2019.html",
	 	 "tabla_origenes_evento_2019":{},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-festivern-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 3, 'Viajes normal': 0.07, 'Proporción': 4114.29}, 1: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.93, 'Proporción': 107.41}, 2: {'City': 'Alcoi', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 2000.0}, 3: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.51, 'Proporción': 196.3}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-festivern-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_festivern_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_festivern_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['PLAN B', 'SFDK', 'IRA', 'MALDATS', 'ZOO', 'DESAKATO', 'ELS CATARRES', 'BUHOS', 'TALCO', 'SMOKING SOULS', 'TREMENDA JAURIA', 'LA FUMIGA', 'TRIBADE', 'JAZZWOMAN', 'NEW YORK SKA', 'MACHETE EN BOCA', 'CACTUS', 'NARCO', 'INERCIA', 'BALKAN PARADISE', 'ADALA', 'ATUPA', 'MAFALDA', 'AUXILI', 'LA SELVA SUR', 'NOVUS ORDO', 'THE DANCE CRASHERS', 'METALL I SO', 'LA HIJAS DE LA CUMBIA'],
	 	 "artistas_evento_2019":['SUU', 'ANIER', 'PLAN B', 'TESA', 'HARD GZ', 'OQUES GRASSES', 'GREEN VALLEY', 'NATIVA', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'HUNTZA', 'LILDAMI', 'SOZIEDAD ALKOHOLIKA', 'LA FUMIGA', 'DEF CON DOS', 'EL DILUVI', 'SKAKEITAN', 'XAVI SARRIA', 'PUPILLES', 'EBRI KNIGHT', 'PIRATS', 'DAKIDARRIA', 'SENYOR OCA', 'NARCO', 'FUNKIWIS', 'ATUPA', 'BALKAN BOMBA', 'EL TIO LA CARETA', 'NIUS DE NIT', 'ARSENIC', 'REACCIO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_festivern_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_festivern_2019.jpg",
	 }, 
	 { 
	 	 "id": "20",
	 	 "nombre": "FIB",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Benicasim",
	 	 "fecha_ini_2018": "19-07-2018",
	 	 "fecha_fin_2018": "22-07-2018",
	 	 "fecha_ini_2019": "18-07-2019",
	 	 "fecha_fin_2019": "21-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 31.8,
	 	 "D002_evento_media_viajes_2018": 3.46,
	 	 "D003_evento_media_viajes_dia_semana_2018": 5.62,
	 	 "D004_evento_viajes_2019": 27.8,
	 	 "D005_evento_media_viajes_2019": 4.26,
	 	 "D006_evento_media_viajes_dia_semana_2019": 5.74,

	 	 "D007_proporcion_asientos_evento_2018": 45.09,
	 	 "D008_proporcion_asientos_media_evento_2018": 8.47,
	 	 "D009_proporcion_asientos_evento_2019": 40.1,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.12,

	 	 "D011_ocupacion_asientos_evento_2018": 0.23,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.69,
	 	 "D013_ocupacion_asientos_evento_2019": 0.19,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.62,

	 	 "D015_precio_evento_2018": 0.0612,
	 	 "D016_precio_medio_evento_2018": 0.0573,
	 	 "D017_precio_evento_2019": 0.0635,
	 	 "D018_precio_medio_evento_2019": 0.0587,

	 	 "D019_novatos_evento_2018": 10.4,
	 	 "D020_novatos_media_evento_2018": 5.35,
	 	 "D021_novatos_evento_2019": 9.2,
	 	 "D022_novatos_media_evento_2019": 4.3,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-fib-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 64, 'Viajes normal': 1.54, 'Proporción': 4149.28}, 1: {'City': 'Valencia', 'Viajes en evento': 31, 'Viajes normal': 0.42, 'Proporción': 7329.46}, 2: {'City': 'Barcelona', 'Viajes en evento': 23, 'Viajes normal': 0.77, 'Proporción': 2998.21}, 3: {'City': 'Zaragoza', 'Viajes en evento': 7, 'Viajes normal': 0.54, 'Proporción': 1295.52}, 4: {'City': 'Alicante', 'Viajes en evento': 5, 'Viajes normal': 0.36, 'Proporción': 1375.0}, 5: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.36, 'Proporción': 835.71}, 6: {'City': 'Elche', 'Viajes en evento': 3, 'Viajes normal': 0.02, 'Proporción': 12300.0}, 7: {'City': 'Pamplona', 'Viajes en evento': 1, 'Viajes normal': 0.33, 'Proporción': 300.0}, 8: {'City': 'el Prat de Llobregat', 'Viajes en evento': 2, 'Viajes normal': 0.18, 'Proporción': 1088.89}, 9: {'City': 'Benidorm', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-fib-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 56, 'Viajes normal': 1.73, 'Proporción': 3243.56}, 1: {'City': 'Valencia', 'Viajes en evento': 22, 'Viajes normal': 0.38, 'Proporción': 5738.14}, 2: {'City': 'Barcelona', 'Viajes en evento': 16, 'Viajes normal': 0.8, 'Proporción': 1994.67}, 3: {'City': 'Zaragoza', 'Viajes en evento': 6, 'Viajes normal': 0.41, 'Proporción': 1460.0}, 4: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.31, 'Proporción': 964.29}, 5: {'City': 'Teruel', 'Viajes en evento': 1, 'Viajes normal': 0.1, 'Proporción': 1050.0}, 6: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.2, 'Proporción': 1466.67}, 7: {'City': 'Tarragona', 'Viajes en evento': 3, 'Viajes normal': 0.09, 'Proporción': 3471.43}, 8: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.25, 'Proporción': 400.0}, 9: {'City': 'Sagunto', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 12100.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-fib-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 30, 'Viajes normal': 1.77, 'Proporción': 1696.72}, 1: {'City': 'Valencia', 'Viajes en evento': 10, 'Viajes normal': 0.67, 'Proporción': 1494.85}, 2: {'City': 'Barcelona', 'Viajes en evento': 7, 'Viajes normal': 0.8, 'Proporción': 873.94}, 3: {'City': 'Zaragoza', 'Viajes en evento': 4, 'Viajes normal': 0.48, 'Proporción': 828.07}, 4: {'City': 'Murcia', 'Viajes en evento': 2, 'Viajes normal': 0.37, 'Proporción': 545.45}, 5: {'City': 'Alicante', 'Viajes en evento': 2, 'Viajes normal': 0.48, 'Proporción': 420.69}, 6: {'City': 'Pamplona', 'Viajes en evento': 2, 'Viajes normal': 0.39, 'Proporción': 509.09}, 7: {'City': 'Manises', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 2100.0}, 8: {'City': 'Torrelodones', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Alberic', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-fib-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 27, 'Viajes normal': 2.02, 'Proporción': 1338.75}, 1: {'City': 'Valencia', 'Viajes en evento': 12, 'Viajes normal': 0.6, 'Proporción': 1997.37}, 2: {'City': 'Barcelona', 'Viajes en evento': 8, 'Viajes normal': 1.01, 'Proporción': 790.96}, 3: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.63, 'Proporción': 316.28}, 4: {'City': 'Gandia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Tarragona', 'Viajes en evento': 2, 'Viajes normal': 0.13, 'Proporción': 1578.95}, 6: {'City': 'Córdoba', 'Viajes en evento': 1, 'Viajes normal': 0.11, 'Proporción': 950.0}, 7: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 520.0}, 8: {'City': 'Fuenlabrada', 'Viajes en evento': 1, 'Viajes normal': 0.22, 'Proporción': 450.0}, 9: {'City': 'Málaga', 'Viajes en evento': 1, 'Viajes normal': 0.35, 'Proporción': 283.33}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_fib_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_fib_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['TRAVIS SCOTT', 'BASTILLE', 'THE KILLERS', 'J HUS', 'C TANGANA', 'TWO DOOR CINEMA CLUB', 'THE KOOKS', 'CATFISH AND THE BOTTLEMEN', 'PET SHOP BOYS', 'NOTHING BUT THIEVES', 'GIGGS', 'IZAL', 'ERIC PRYDZ', 'AMABLE', 'METRONOMY', 'JESSIE WARE', 'CHASE AND STATUS', 'LIAM GALLAGHER', 'PRINCESS NOKIA', 'MADNESS', 'CASHMERE CAT', 'JUSTICE', 'THE VACCINES', 'BELLE AND SEBASTIAN', 'WOLF ALICE', 'PARQUET COURTS', 'EVERYTHING EVERYTHING', 'THE CHARLATANS', 'DORIAN', 'THE HORROS', 'ANNA CALVI', 'SHAME', 'LOS PUNSETES', 'TULSA', 'MONARCHY', 'TOUNDRA', 'KING KHAN', 'ODDISEE & GOOD COMPNY'],
	 	 "artistas_evento_2019":['LANA DEL REY', 'THE 1975', 'JESS GLYNNE', 'KINGS OF LEON', 'GEORGE EZRA', 'AJ TRACEY', 'KODALINE', 'MARINA', 'GERRY CINNAMON', 'BAKERMAT', 'FATBOY SLIM', 'GORGON CITY', 'FRANZ FERDINAND', 'VETUSTA MORLA', 'OCTAVIAN', 'BLOSSOMS', 'ACTION BRONSON', 'YOU ME AT SIX', 'YELLOW DAYS', 'THE HUNNA', 'LA M.O.D.A', 'CUPIDO', 'CAROLINA DURANTE', 'BLACK LIPS', 'CARIÑO', 'BELAKO', 'KREPT X KONAN'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_fib_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_fib_2019.jpg",
	 }, 
	 { 
	 	 "id": "21",
	 	 "nombre": "FiV",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Vilalba",
	 	 "fecha_ini_2018": "27-04-2018",
	 	 "fecha_fin_2018": "28-04-2018",
	 	 "fecha_ini_2019": "26-04-2019",
	 	 "fecha_fin_2019": "27-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 15.0,
	 	 "D002_evento_media_viajes_2018": 0.78,
	 	 "D003_evento_media_viajes_dia_semana_2018": 1.88,
	 	 "D004_evento_viajes_2019": 2.11,
	 	 "D005_evento_media_viajes_2019": 1.0,
	 	 "D006_evento_media_viajes_dia_semana_2019": 2.5,

	 	 "D007_proporcion_asientos_evento_2018": 44.23,
	 	 "D008_proporcion_asientos_media_evento_2018": 2.34,
	 	 "D009_proporcion_asientos_evento_2019": 38.6,
	 	 "D010_proporcion_asientos_media_evento_2019": 3.45,

	 	 "D011_ocupacion_asientos_evento_2018": 0.16,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.26,
	 	 "D013_ocupacion_asientos_evento_2019": 0.54,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.24,

	 	 "D015_precio_evento_2018": 0.06,
	 	 "D016_precio_medio_evento_2018": 0.0573,
	 	 "D017_precio_evento_2019": 0.0609,
	 	 "D018_precio_medio_evento_2019": 0.0604,

	 	 "D019_novatos_evento_2018": 6.0,
	 	 "D020_novatos_media_evento_2018": 1.81,
	 	 "D021_novatos_evento_2019": 0.37,
	 	 "D022_novatos_media_evento_2019": 1.45,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-fiv-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Santiago de Compostela', 'Viajes en evento': 8, 'Viajes normal': 0.17, 'Proporción': 4595.74}, 1: {'City': 'Vigo', 'Viajes en evento': 6, 'Viajes normal': 0.07, 'Proporción': 9000.0}, 2: {'City': 'A Coruña', 'Viajes en evento': 4, 'Viajes normal': 0.11, 'Proporción': 3553.85}, 3: {'City': 'Madrid', 'Viajes en evento': 2, 'Viajes normal': 0.23, 'Proporción': 879.07}, 4: {'City': 'Gijón', 'Viajes en evento': 4, 'Viajes normal': 0.03, 'Proporción': 14266.67}, 5: {'City': 'Ferrol', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 11650.0}, 6: {'City': 'Ponferrada', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 17100.0}, 7: {'City': 'Bilbao', 'Viajes en evento': 1, 'Viajes normal': 0.23, 'Proporción': 429.63}, 8: {'City': 'Ourense', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Tapia de Casariego', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-fiv-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Gijón', 'Viajes en evento': 18, 'Viajes normal': 0.05, 'Proporción': 38700.0}, 1: {'City': 'Santiago de Compostela', 'Viajes en evento': 26, 'Viajes normal': 0.17, 'Proporción': 15178.38}, 2: {'City': 'A Coruña', 'Viajes en evento': 18, 'Viajes normal': 0.11, 'Proporción': 16971.43}, 3: {'City': 'Madrid', 'Viajes en evento': 13, 'Viajes normal': 0.29, 'Proporción': 4431.82}, 4: {'City': 'Bilbao', 'Viajes en evento': 10, 'Viajes normal': 0.27, 'Proporción': 3666.67}, 5: {'City': 'Ribadeo', 'Viajes en evento': 3, 'Viajes normal': 0.03, 'Proporción': 10000.0}, 6: {'City': 'Vigo', 'Viajes en evento': 5, 'Viajes normal': 0.19, 'Proporción': 2590.91}, 7: {'City': 'Oviedo', 'Viajes en evento': 4, 'Viajes normal': 0.16, 'Proporción': 2514.29}, 8: {'City': 'Laredo', 'Viajes en evento': 2, 'Viajes normal': 0.25, 'Proporción': 800.0}, 9: {'City': 'Poio', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-fiv-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'A Coruña', 'Viajes en evento': 1, 'Viajes normal': 0.15, 'Proporción': 685.71}, 1: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.27, 'Proporción': 366.67}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-fiv-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 12, 'Viajes normal': 0.31, 'Proporción': 3851.16}, 1: {'City': 'Gijón', 'Viajes en evento': 8, 'Viajes normal': 0.14, 'Proporción': 5837.04}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 10, 'Viajes normal': 0.34, 'Proporción': 2935.48}, 3: {'City': 'Bilbao', 'Viajes en evento': 10, 'Viajes normal': 0.27, 'Proporción': 3655.17}, 4: {'City': 'A Coruña', 'Viajes en evento': 7, 'Viajes normal': 0.2, 'Proporción': 3519.44}, 5: {'City': 'Vigo', 'Viajes en evento': 5, 'Viajes normal': 0.26, 'Proporción': 1923.08}, 6: {'City': 'Oviedo', 'Viajes en evento': 5, 'Viajes normal': 0.14, 'Proporción': 3461.54}, 7: {'City': 'Ribadeo', 'Viajes en evento': 3, 'Viajes normal': 0.05, 'Proporción': 6180.0}, 8: {'City': 'Avilés', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Castro-Urdiales', 'Viajes en evento': 2, 'Viajes normal': 0.4, 'Proporción': 500.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_fiv_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_fiv_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['CARLOS SADNESS', 'LORI MEYERS', 'XOEL LOPEZ', 'NOVEDADES CARMINHA', 'SHINOVA', 'EL COLUMPIO ASESINO', 'OSCARMINA', 'NUNATAK', 'DAVID KANO', 'ESCUCHANDO ELEFANTES', 'POSTERGEL'],
	 	 "artistas_evento_2019":['SILOE', 'LA M.O.D.A', 'IVAN FERREIRO', 'MISS CAFFEINA', 'DEPEDRO', 'SIDONIE', 'LOS PLANETAS', 'DORIAN', 'MAREM LADSON', 'LA SONRISA DE JULIA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_fiv_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_fiv_2019.jpg",
	 }, 
	 { 
	 	 "id": "22",
	 	 "nombre": "GRANADA SOUND",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Granada",
	 	 "fecha_ini_2018": "21-09-2018",
	 	 "fecha_fin_2018": "22-09-2018",
	 	 "fecha_ini_2019": "20-09-2019",
	 	 "fecha_fin_2019": "20-09-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 394.6666666666667,
	 	 "D002_evento_media_viajes_2018": 236.04,
	 	 "D003_evento_media_viajes_dia_semana_2018": 339.85,
	 	 "D004_evento_viajes_2019": 516.5,
	 	 "D005_evento_media_viajes_2019": 263.64,
	 	 "D006_evento_media_viajes_dia_semana_2019": 368.55,

	 	 "D007_proporcion_asientos_evento_2018": 31.77,
	 	 "D008_proporcion_asientos_media_evento_2018": 20.15,
	 	 "D009_proporcion_asientos_evento_2019": 32.7,
	 	 "D010_proporcion_asientos_media_evento_2019": 21.08,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.7,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.68,

	 	 "D015_precio_evento_2018": 0.0537,
	 	 "D016_precio_medio_evento_2018": 0.0532,
	 	 "D017_precio_evento_2019": 0.0518,
	 	 "D018_precio_medio_evento_2019": 0.0542,

	 	 "D019_novatos_evento_2018": 106.0,
	 	 "D020_novatos_media_evento_2018": 73.58,
	 	 "D021_novatos_evento_2019": 105.0,
	 	 "D022_novatos_media_evento_2019": 62.79,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-granada sound-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Málaga', 'Viajes en evento': 148, 'Viajes normal': 38.5, 'Proporción': 384.4}, 1: {'City': 'Madrid', 'Viajes en evento': 193, 'Viajes normal': 21.29, 'Proporción': 906.39}, 2: {'City': 'Jaén', 'Viajes en evento': 63, 'Viajes normal': 10.24, 'Proporción': 615.17}, 3: {'City': 'Sevilla', 'Viajes en evento': 110, 'Viajes normal': 25.19, 'Proporción': 436.6}, 4: {'City': 'Murcia', 'Viajes en evento': 61, 'Viajes normal': 10.97, 'Proporción': 556.07}, 5: {'City': 'Almería', 'Viajes en evento': 67, 'Viajes normal': 12.27, 'Proporción': 545.87}, 6: {'City': 'Baza', 'Viajes en evento': 23, 'Viajes normal': 5.03, 'Proporción': 457.49}, 7: {'City': 'Valencia', 'Viajes en evento': 34, 'Viajes normal': 7.79, 'Proporción': 436.36}, 8: {'City': 'Córdoba', 'Viajes en evento': 42, 'Viajes normal': 8.04, 'Proporción': 522.67}, 9: {'City': 'El Ejido', 'Viajes en evento': 21, 'Viajes normal': 3.72, 'Proporción': 564.43}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-granada sound-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 175, 'Viajes normal': 20.91, 'Proporción': 836.87}, 1: {'City': 'Málaga', 'Viajes en evento': 132, 'Viajes normal': 40.19, 'Proporción': 328.46}, 2: {'City': 'Jaén', 'Viajes en evento': 58, 'Viajes normal': 13.47, 'Proporción': 430.68}, 3: {'City': 'Sevilla', 'Viajes en evento': 85, 'Viajes normal': 25.42, 'Proporción': 334.41}, 4: {'City': 'Murcia', 'Viajes en evento': 45, 'Viajes normal': 10.63, 'Proporción': 423.27}, 5: {'City': 'Almería', 'Viajes en evento': 52, 'Viajes normal': 13.04, 'Proporción': 398.89}, 6: {'City': 'Baza', 'Viajes en evento': 22, 'Viajes normal': 6.61, 'Proporción': 332.9}, 7: {'City': 'Córdoba', 'Viajes en evento': 35, 'Viajes normal': 8.62, 'Proporción': 405.8}, 8: {'City': 'Valencia', 'Viajes en evento': 31, 'Viajes normal': 7.73, 'Proporción': 400.85}, 9: {'City': 'Motril', 'Viajes en evento': 8, 'Viajes normal': 3.61, 'Proporción': 221.57}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-granada sound-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Málaga', 'Viajes en evento': 139, 'Viajes normal': 35.28, 'Proporción': 394.0}, 1: {'City': 'Jaén', 'Viajes en evento': 44, 'Viajes normal': 10.44, 'Proporción': 421.3}, 2: {'City': 'Sevilla', 'Viajes en evento': 83, 'Viajes normal': 24.59, 'Proporción': 337.47}, 3: {'City': 'Madrid', 'Viajes en evento': 64, 'Viajes normal': 22.2, 'Proporción': 288.25}, 4: {'City': 'Murcia', 'Viajes en evento': 37, 'Viajes normal': 11.16, 'Proporción': 331.49}, 5: {'City': 'Almería', 'Viajes en evento': 47, 'Viajes normal': 11.97, 'Proporción': 392.56}, 6: {'City': 'Baza', 'Viajes en evento': 32, 'Viajes normal': 4.45, 'Proporción': 719.65}, 7: {'City': 'Córdoba', 'Viajes en evento': 32, 'Viajes normal': 8.01, 'Proporción': 399.32}, 8: {'City': 'Valencia', 'Viajes en evento': 29, 'Viajes normal': 7.99, 'Proporción': 363.0}, 9: {'City': 'Motril', 'Viajes en evento': 13, 'Viajes normal': 2.02, 'Proporción': 644.7}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-granada sound-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Málaga', 'Viajes en evento': 104, 'Viajes normal': 38.32, 'Proporción': 271.38}, 1: {'City': 'Jaén', 'Viajes en evento': 35, 'Viajes normal': 13.83, 'Proporción': 253.03}, 2: {'City': 'Sevilla', 'Viajes en evento': 70, 'Viajes normal': 25.71, 'Proporción': 272.26}, 3: {'City': 'Madrid', 'Viajes en evento': 37, 'Viajes normal': 21.68, 'Proporción': 170.63}, 4: {'City': 'Murcia', 'Viajes en evento': 27, 'Viajes normal': 10.81, 'Proporción': 249.79}, 5: {'City': 'Almería', 'Viajes en evento': 34, 'Viajes normal': 12.83, 'Proporción': 264.96}, 6: {'City': 'Baza', 'Viajes en evento': 24, 'Viajes normal': 5.78, 'Proporción': 415.02}, 7: {'City': 'Córdoba', 'Viajes en evento': 22, 'Viajes normal': 8.37, 'Proporción': 262.89}, 8: {'City': 'Lorca', 'Viajes en evento': 25, 'Viajes normal': 2.76, 'Proporción': 904.76}, 9: {'City': 'Valencia', 'Viajes en evento': 19, 'Viajes normal': 7.72, 'Proporción': 246.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_granada sound_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_granada sound_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['CARLOS SADNESS', 'EL KANKA', 'MANDO DIAO', 'LA CASA AZUL', 'SIDECARS', 'JUANITO MAKANDE', 'DORIAN', 'VIVA SUECIA', 'FULL', 'ELEFANTES', 'NIÑOS MUTANTES', 'NEUMAN', 'RUFUS T.FIREFLY', 'ANGEL STANICH', 'RAYDEN', 'NANCYS RUBIAS', 'SEXY ZEBRAS', 'CORIZONAS', 'RUSOS BLANCOS', 'GRYSTAL FIGHTERS'],
	 	 "artistas_evento_2019":['MORGAN', 'VETUSTA MORLA', 'SECOND', 'LA M.O.D.A', 'ZAHARA', 'KUVE', 'IVAN FERREIRO', 'CUPIDO', 'DEPEDRO', 'ANNI B SWEET', 'CAROLINA DURANTE', 'NOVEDADES CARMINHA', 'LEON BENAVENTE', 'SHINOVA', 'CARMEN BOZA', 'AMATRIA', 'ELYELLA', 'ALICE WONDER', 'ST WOODS', 'MUCHO', 'MOLINA MOLINA', 'VILLANUEVA', 'RUSOS BLANCOS', 'LEY DJ', 'JACOBO SERRA', 'GIMNASTICA', 'RADIO PALMER'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_granada sound_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_granada sound_2019.jpg",
	 }, 
	 { 
	 	 "id": "23",
	 	 "nombre": "INTERESTELAR",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Sevilla",
	 	 "fecha_ini_2018": "18-05-2018",
	 	 "fecha_fin_2018": "19-05-2018",
	 	 "fecha_ini_2019": "24-05-2019",
	 	 "fecha_fin_2019": "25-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 291.3333333333333,
	 	 "D002_evento_media_viajes_2018": 276.67,
	 	 "D003_evento_media_viajes_dia_semana_2018": 377.75,
	 	 "D004_evento_viajes_2019": 387.33,
	 	 "D005_evento_media_viajes_2019": 318.33,
	 	 "D006_evento_media_viajes_dia_semana_2019": 410.9,

	 	 "D007_proporcion_asientos_evento_2018": 25.59,
	 	 "D008_proporcion_asientos_media_evento_2018": 17.64,
	 	 "D009_proporcion_asientos_evento_2019": 23.94,
	 	 "D010_proporcion_asientos_media_evento_2019": 18.52,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.74,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.71,

	 	 "D015_precio_evento_2018": 0.0511,
	 	 "D016_precio_medio_evento_2018": 0.053,
	 	 "D017_precio_evento_2019": 0.0535,
	 	 "D018_precio_medio_evento_2019": 0.0539,

	 	 "D019_novatos_evento_2018": 74.67,
	 	 "D020_novatos_media_evento_2018": 100.76,
	 	 "D021_novatos_evento_2019": 116.33,
	 	 "D022_novatos_media_evento_2019": 87.31,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-interestelar-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 99, 'Viajes normal': 19.96, 'Proporción': 495.95}, 1: {'City': 'Córdoba', 'Viajes en evento': 59, 'Viajes normal': 17.87, 'Proporción': 330.14}, 2: {'City': 'Huelva', 'Viajes en evento': 47, 'Viajes normal': 15.87, 'Proporción': 296.24}, 3: {'City': 'Málaga', 'Viajes en evento': 87, 'Viajes normal': 25.47, 'Proporción': 341.56}, 4: {'City': 'Granada', 'Viajes en evento': 81, 'Viajes normal': 24.59, 'Proporción': 329.34}, 5: {'City': 'Jerez de la Frontera', 'Viajes en evento': 31, 'Viajes normal': 10.39, 'Proporción': 298.39}, 6: {'City': 'Mérida', 'Viajes en evento': 25, 'Viajes normal': 7.0, 'Proporción': 357.28}, 7: {'City': 'Cádiz', 'Viajes en evento': 54, 'Viajes normal': 16.99, 'Proporción': 317.9}, 8: {'City': 'Algeciras', 'Viajes en evento': 43, 'Viajes normal': 12.29, 'Proporción': 349.94}, 9: {'City': 'Badajoz', 'Viajes en evento': 29, 'Viajes normal': 6.92, 'Proporción': 418.88}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-interestelar-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Córdoba', 'Viajes en evento': 73, 'Viajes normal': 22.64, 'Proporción': 322.37}, 1: {'City': 'Madrid', 'Viajes en evento': 163, 'Viajes normal': 20.65, 'Proporción': 789.17}, 2: {'City': 'Huelva', 'Viajes en evento': 66, 'Viajes normal': 21.16, 'Proporción': 311.89}, 3: {'City': 'Valencia', 'Viajes en evento': 76, 'Viajes normal': 3.06, 'Proporción': 2480.96}, 4: {'City': 'Málaga', 'Viajes en evento': 120, 'Viajes normal': 31.72, 'Proporción': 378.27}, 5: {'City': 'Granada', 'Viajes en evento': 88, 'Viajes normal': 25.71, 'Proporción': 342.27}, 6: {'City': 'Mérida', 'Viajes en evento': 21, 'Viajes normal': 8.21, 'Proporción': 255.77}, 7: {'City': 'Jerez de la Frontera', 'Viajes en evento': 36, 'Viajes normal': 13.12, 'Proporción': 274.49}, 8: {'City': 'Cádiz', 'Viajes en evento': 56, 'Viajes normal': 20.17, 'Proporción': 277.67}, 9: {'City': 'Cáceres', 'Viajes en evento': 19, 'Viajes normal': 7.33, 'Proporción': 259.36}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-interestelar-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Córdoba', 'Viajes en evento': 90, 'Viajes normal': 17.5, 'Proporción': 514.33}, 1: {'City': 'Huelva', 'Viajes en evento': 49, 'Viajes normal': 15.22, 'Proporción': 321.96}, 2: {'City': 'Granada', 'Viajes en evento': 91, 'Viajes normal': 25.19, 'Proporción': 361.19}, 3: {'City': 'Málaga', 'Viajes en evento': 80, 'Viajes normal': 25.78, 'Proporción': 310.28}, 4: {'City': 'Jerez de la Frontera', 'Viajes en evento': 35, 'Viajes normal': 9.52, 'Proporción': 367.84}, 5: {'City': 'Cádiz', 'Viajes en evento': 55, 'Viajes normal': 16.52, 'Proporción': 333.03}, 6: {'City': 'Mérida', 'Viajes en evento': 21, 'Viajes normal': 6.14, 'Proporción': 342.19}, 7: {'City': 'Madrid', 'Viajes en evento': 41, 'Viajes normal': 21.66, 'Proporción': 189.29}, 8: {'City': 'Almonte', 'Viajes en evento': 17, 'Viajes normal': 1.3, 'Proporción': 1309.57}, 9: {'City': 'Lisboa', 'Viajes en evento': 38, 'Viajes normal': 5.26, 'Proporción': 723.05}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-interestelar-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Córdoba', 'Viajes en evento': 116, 'Viajes normal': 22.53, 'Proporción': 514.95}, 1: {'City': 'Huelva', 'Viajes en evento': 79, 'Viajes normal': 20.49, 'Proporción': 385.49}, 2: {'City': 'Málaga', 'Viajes en evento': 120, 'Viajes normal': 30.67, 'Proporción': 391.21}, 3: {'City': 'Granada', 'Viajes en evento': 91, 'Viajes normal': 25.42, 'Proporción': 358.02}, 4: {'City': 'Jerez de la Frontera', 'Viajes en evento': 51, 'Viajes normal': 11.88, 'Proporción': 429.35}, 5: {'City': 'Mérida', 'Viajes en evento': 33, 'Viajes normal': 7.5, 'Proporción': 439.81}, 6: {'City': 'Cádiz', 'Viajes en evento': 67, 'Viajes normal': 19.34, 'Proporción': 346.39}, 7: {'City': 'Madrid', 'Viajes en evento': 46, 'Viajes normal': 20.66, 'Proporción': 222.6}, 8: {'City': 'Algeciras', 'Viajes en evento': 53, 'Viajes normal': 13.71, 'Proporción': 386.56}, 9: {'City': 'Cáceres', 'Viajes en evento': 25, 'Viajes normal': 7.16, 'Proporción': 349.26}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_interestelar_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_interestelar_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ARCO', 'CARLOS SADNESS', 'SFDK', 'EL KANKA', 'LA CASA AZUL', 'SIDECARS', 'JUANITO MAKANDE', 'COQUE MALLA', 'LORI MEYERS', 'DORIAN', 'KIKO VENENO', 'FULL', 'ELEFANTES', 'NIÑOS MUTANTES', 'RUFUS T.FIREFLY', 'ANGEL STANICH', 'TULSA', 'RAYDEN', 'NANCYS RUBIAS', 'ARIZONA BABY', 'SEXY ZEBRAS', 'JOSELE SANTIAGO', 'VILLANUEVA', 'THE GROOVES', 'LOS BENGALA', 'BATRACIO'],
	 	 "artistas_evento_2019":['MORGAN', 'ROZALEN', 'VETUSTA MORLA', 'DANI FERNANDEZ', 'SECOND', 'LA M.O.D.A', 'ZAHARA', 'IVAN FERREIRO', 'MIKEL ERENTXUN', 'FANGORIA', 'MUERDO', 'TOTEKING', 'DEPEDRO', 'NIXON', 'SHINOVA', 'CARMEN BOZA', 'TU OTRA BONITA', 'SHOTTA', 'ALICE WONDER', 'MUEVELOREINA', 'LOS VINAGRES'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_interestelar_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_interestelar_2019.jpg",
	 }, 
	 { 
	 	 "id": "24",
	 	 "nombre": "JUERGAS ROCK",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Adra",
	 	 "fecha_ini_2018": "01-08-2018",
	 	 "fecha_fin_2018": "04-08-2018",
	 	 "fecha_ini_2019": "31-07-2019",
	 	 "fecha_fin_2019": "03-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 17.0,
	 	 "D002_evento_media_viajes_2018": 2.01,
	 	 "D003_evento_media_viajes_dia_semana_2018": 1.37,
	 	 "D004_evento_viajes_2019": 15.0,
	 	 "D005_evento_media_viajes_2019": 3.44,
	 	 "D006_evento_media_viajes_dia_semana_2019": 3.68,

	 	 "D007_proporcion_asientos_evento_2018": 40.0,
	 	 "D008_proporcion_asientos_media_evento_2018": 6.19,
	 	 "D009_proporcion_asientos_evento_2019": 30.77,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.41,

	 	 "D011_ocupacion_asientos_evento_2018": 0.19,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.43,
	 	 "D013_ocupacion_asientos_evento_2019": 0.15,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.38,

	 	 "D015_precio_evento_2018": 0.0559,
	 	 "D016_precio_medio_evento_2018": 0.0614,
	 	 "D017_precio_evento_2019": 0.0587,
	 	 "D018_precio_medio_evento_2019": 0.0613,

	 	 "D019_novatos_evento_2018": 1.6,
	 	 "D020_novatos_media_evento_2018": 1.84,
	 	 "D021_novatos_evento_2019": 2.6,
	 	 "D022_novatos_media_evento_2019": 1.39,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-juergas rock-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Granada', 'Viajes en evento': 20, 'Viajes normal': 0.99, 'Proporción': 2016.9}, 1: {'City': 'Murcia', 'Viajes en evento': 8, 'Viajes normal': 0.13, 'Proporción': 6240.0}, 2: {'City': 'Madrid', 'Viajes en evento': 16, 'Viajes normal': 0.26, 'Proporción': 6133.33}, 3: {'City': 'Málaga', 'Viajes en evento': 7, 'Viajes normal': 0.32, 'Proporción': 2202.44}, 4: {'City': 'Almería', 'Viajes en evento': 2, 'Viajes normal': 0.01, 'Proporción': 28100.0}, 5: {'City': 'Lorca', 'Viajes en evento': 3, 'Viajes normal': 0.04, 'Proporción': 8100.0}, 6: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}, 7: {'City': 'Córdoba', 'Viajes en evento': 3, 'Viajes normal': 0.16, 'Proporción': 1900.0}, 8: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.11, 'Proporción': 900.0}, 9: {'City': 'Las Rozas de Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-juergas rock-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Granada', 'Viajes en evento': 18, 'Viajes normal': 1.58, 'Proporción': 1140.52}, 1: {'City': 'Málaga', 'Viajes en evento': 14, 'Viajes normal': 0.67, 'Proporción': 2074.7}, 2: {'City': 'Madrid', 'Viajes en evento': 13, 'Viajes normal': 0.32, 'Proporción': 4039.29}, 3: {'City': 'Murcia', 'Viajes en evento': 7, 'Viajes normal': 0.12, 'Proporción': 5653.85}, 4: {'City': 'Almería', 'Viajes en evento': 2, 'Viajes normal': 0.02, 'Proporción': 10680.0}, 5: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.28, 'Proporción': 710.0}, 6: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Jerez de la Frontera', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 2100.0}, 8: {'City': 'Alcorcón', 'Viajes en evento': 2, 'Viajes normal': 0.33, 'Proporción': 600.0}, 9: {'City': 'Vera', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-juergas rock-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Granada', 'Viajes en evento': 3, 'Viajes normal': 1.36, 'Proporción': 220.41}, 1: {'City': 'Málaga', 'Viajes en evento': 3, 'Viajes normal': 0.34, 'Proporción': 883.33}, 2: {'City': 'Rincón de la Victoria', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1800.0}, 3: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-juergas rock-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Granada', 'Viajes en evento': 6, 'Viajes normal': 2.3, 'Proporción': 261.4}, 1: {'City': 'Málaga', 'Viajes en evento': 4, 'Viajes normal': 0.8, 'Proporción': 497.87}, 2: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.15, 'Proporción': 673.33}, 4: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.52, 'Proporción': 193.88}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_juergas rock_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_juergas rock_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ROZALEN', 'BAD RELIGION', 'LA RAIZ', 'LOQUILLO', 'GREEN VALLEY', 'ZOO', 'JUANITO MAKANDE', 'THE SKATALITES', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'LOS DE MARRAS', 'PONCHO K', 'RUPATRUPA', 'GATILLAZO', 'KAOTIKO', 'LENDAKARIS MUERTOS', 'SOZIEDAD ALKOHOLIKA', 'SEGISMUNDO TOXICOMANO', 'HAMLET', 'PARABELLUM', 'NARCO', 'TRASHTUCADA', 'JUANTXO SKALARI', 'LA PHAZE', 'ALPARGATA'],
	 	 "artistas_evento_2019":['KOMA', 'ANIER', 'AYAX Y PROK', 'MACACO', 'SEPULTURA', 'LAGRIMAS DE SANGRE', 'DESAKATO', 'BOIKOT', 'BERRI TXARRAK', 'TALCO', 'EL CANIJO DE JEREZ', 'FRAC', 'GATILLAZO', 'SONS OF AGUIRRE', 'PORRETAS', 'SOZIEDAD ALKOHOLIKA', 'TREMENDA JAURIA', 'GRITANDO EN SILENCIO', 'HORA ZULU', 'NARCO', 'TRASHTUCADA', 'MAFALDA', 'JAMONES CON TACONES', 'ALAMEDADOSOULNA', 'NO POTABLE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_juergas rock_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_juergas rock_2019.jpg",
	 }, 
	 { 
	 	 "id": "25",
	 	 "nombre": "LES ARTS",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Valencia",
	 	 "fecha_ini_2018": "08-06-2018",
	 	 "fecha_fin_2018": "09-06-2018",
	 	 "fecha_ini_2019": "07-06-2019",
	 	 "fecha_fin_2019": "09-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 337.3333333333333,
	 	 "D002_evento_media_viajes_2018": 277.67,
	 	 "D003_evento_media_viajes_dia_semana_2018": 419.35,
	 	 "D004_evento_viajes_2019": 396.0,
	 	 "D005_evento_media_viajes_2019": 302.76,
	 	 "D006_evento_media_viajes_dia_semana_2019": 441.26,

	 	 "D007_proporcion_asientos_evento_2018": 37.48,
	 	 "D008_proporcion_asientos_media_evento_2018": 24.83,
	 	 "D009_proporcion_asientos_evento_2019": 38.04,
	 	 "D010_proporcion_asientos_media_evento_2019": 26.1,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.87,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.86,

	 	 "D015_precio_evento_2018": 0.0535,
	 	 "D016_precio_medio_evento_2018": 0.0546,
	 	 "D017_precio_evento_2019": 0.0559,
	 	 "D018_precio_medio_evento_2019": 0.0558,

	 	 "D019_novatos_evento_2018": 52.67,
	 	 "D020_novatos_media_evento_2018": 96.51,
	 	 "D021_novatos_evento_2019": 49.75,
	 	 "D022_novatos_media_evento_2019": 85.64,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-les arts-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 361, 'Viajes normal': 74.61, 'Proporción': 483.86}, 1: {'City': 'Barcelona', 'Viajes en evento': 87, 'Viajes normal': 23.98, 'Proporción': 362.87}, 2: {'City': 'Alicante', 'Viajes en evento': 59, 'Viajes normal': 18.29, 'Proporción': 322.62}, 3: {'City': 'Murcia', 'Viajes en evento': 43, 'Viajes normal': 13.88, 'Proporción': 309.81}, 4: {'City': 'Zaragoza', 'Viajes en evento': 45, 'Viajes normal': 10.6, 'Proporción': 424.53}, 5: {'City': 'Castellón de la Plana ', 'Viajes en evento': 11, 'Viajes normal': 2.89, 'Proporción': 380.57}, 6: {'City': 'Tarragona', 'Viajes en evento': 10, 'Viajes normal': 3.36, 'Proporción': 297.72}, 7: {'City': 'Alcoi', 'Viajes en evento': 21, 'Viajes normal': 6.73, 'Proporción': 311.97}, 8: {'City': 'Elche', 'Viajes en evento': 18, 'Viajes normal': 4.37, 'Proporción': 411.65}, 9: {'City': 'Teruel', 'Viajes en evento': 9, 'Viajes normal': 4.53, 'Proporción': 198.73}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-les arts-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 494, 'Viajes normal': 80.15, 'Proporción': 616.36}, 1: {'City': 'Barcelona', 'Viajes en evento': 160, 'Viajes normal': 21.85, 'Proporción': 732.2}, 2: {'City': 'Alicante', 'Viajes en evento': 83, 'Viajes normal': 19.66, 'Proporción': 422.22}, 3: {'City': 'Murcia', 'Viajes en evento': 74, 'Viajes normal': 15.38, 'Proporción': 481.09}, 4: {'City': 'Castellón de la Plana ', 'Viajes en evento': 18, 'Viajes normal': 2.98, 'Proporción': 604.64}, 5: {'City': 'Zaragoza', 'Viajes en evento': 65, 'Viajes normal': 12.0, 'Proporción': 541.52}, 6: {'City': 'Tarragona', 'Viajes en evento': 21, 'Viajes normal': 3.09, 'Proporción': 679.87}, 7: {'City': 'Elche', 'Viajes en evento': 25, 'Viajes normal': 5.17, 'Proporción': 483.15}, 8: {'City': 'Teruel', 'Viajes en evento': 24, 'Viajes normal': 5.21, 'Proporción': 460.32}, 9: {'City': 'Dénia', 'Viajes en evento': 33, 'Viajes normal': 8.92, 'Proporción': 370.05}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-les arts-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 216, 'Viajes normal': 75.84, 'Proporción': 284.82}, 1: {'City': 'Alicante', 'Viajes en evento': 54, 'Viajes normal': 18.23, 'Proporción': 296.26}, 2: {'City': 'Murcia', 'Viajes en evento': 57, 'Viajes normal': 13.28, 'Proporción': 429.15}, 3: {'City': 'Barcelona', 'Viajes en evento': 59, 'Viajes normal': 23.34, 'Proporción': 252.82}, 4: {'City': 'Zaragoza', 'Viajes en evento': 28, 'Viajes normal': 10.65, 'Proporción': 262.86}, 5: {'City': 'Castellón de la Plana ', 'Viajes en evento': 7, 'Viajes normal': 2.68, 'Proporción': 261.25}, 6: {'City': 'Dénia', 'Viajes en evento': 33, 'Viajes normal': 8.34, 'Proporción': 395.83}, 7: {'City': 'Tarragona', 'Viajes en evento': 15, 'Viajes normal': 3.34, 'Proporción': 449.14}, 8: {'City': 'Elche', 'Viajes en evento': 21, 'Viajes normal': 4.39, 'Proporción': 477.87}, 9: {'City': 'Alcoi', 'Viajes en evento': 19, 'Viajes normal': 6.83, 'Proporción': 278.29}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-les arts-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 422, 'Viajes normal': 81.06, 'Proporción': 520.61}, 1: {'City': 'Alicante', 'Viajes en evento': 81, 'Viajes normal': 18.99, 'Proporción': 426.61}, 2: {'City': 'Barcelona', 'Viajes en evento': 99, 'Viajes normal': 20.96, 'Proporción': 472.32}, 3: {'City': 'Zaragoza', 'Viajes en evento': 51, 'Viajes normal': 11.84, 'Proporción': 430.91}, 4: {'City': 'Murcia', 'Viajes en evento': 70, 'Viajes normal': 15.11, 'Proporción': 463.41}, 5: {'City': 'Castellón de la Plana ', 'Viajes en evento': 10, 'Viajes normal': 2.93, 'Proporción': 341.19}, 6: {'City': 'Dénia', 'Viajes en evento': 48, 'Viajes normal': 8.58, 'Proporción': 559.51}, 7: {'City': 'Elche', 'Viajes en evento': 33, 'Viajes normal': 5.24, 'Proporción': 629.76}, 8: {'City': 'Teruel', 'Viajes en evento': 20, 'Viajes normal': 4.88, 'Proporción': 409.98}, 9: {'City': 'Tarragona', 'Viajes en evento': 12, 'Viajes normal': 3.25, 'Proporción': 368.86}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_les arts_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_les arts_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MORGAN', 'CARLOS SADNESS', 'LA M.O.D.A', 'CRYSTAL FIGHTERS', 'MANDO DIAO', 'SIDECARS', 'BELY BASARTE', 'COQUE MALLA', 'LA HABITACION ROJA', 'LORI MEYERS', 'DORIAN', 'VIVA SUECIA', 'FULL', 'ELEFANTES', 'EL COLUMPIO ASESINO', 'AMATRIA', 'RUFUS T.FIREFLY', 'ANGEL STANICH', 'RAYDEN', 'NANCYS RUBIAS', 'BEAROID'],
	 	 "artistas_evento_2019":['ROZALEN', 'VETUSTA MORLA', 'SILOE', 'SOFIA ELLAR', 'SECOND', 'AMAIA', 'LA M.O.D.A', 'ZAHARA', 'IVAN FERREIRO', 'FANGORIA', 'ZOO', 'DORIAN', 'CAROLINA DURANTE', 'DELAPORTE', 'SHINOVA', 'CARIÑO', 'CARMEN BOZA', 'ELYELLA', 'VALIRA', 'PUTOCHINOMARICON', 'LA PLATA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_les arts_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_les arts_2019.jpg",
	 }, 
	 { 
	 	 "id": "26",
	 	 "nombre": "LEYENDAS DEL ROCK",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Villena",
	 	 "fecha_ini_2018": "08-08-2018",
	 	 "fecha_fin_2018": "11-08-2018",
	 	 "fecha_ini_2019": "07-08-2019",
	 	 "fecha_fin_2019": "10-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 9.8,
	 	 "D002_evento_media_viajes_2018": 4.78,
	 	 "D003_evento_media_viajes_dia_semana_2018": 4.02,
	 	 "D004_evento_viajes_2019": 14.2,
	 	 "D005_evento_media_viajes_2019": 6.01,
	 	 "D006_evento_media_viajes_dia_semana_2019": 5.59,

	 	 "D007_proporcion_asientos_evento_2018": 18.81,
	 	 "D008_proporcion_asientos_media_evento_2018": 3.4,
	 	 "D009_proporcion_asientos_evento_2019": 28.41,
	 	 "D010_proporcion_asientos_media_evento_2019": 4.6,

	 	 "D011_ocupacion_asientos_evento_2018": 0.04,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.31,
	 	 "D013_ocupacion_asientos_evento_2019": 0.06,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.3,

	 	 "D015_precio_evento_2018": 0.0548,
	 	 "D016_precio_medio_evento_2018": 0.0566,
	 	 "D017_precio_evento_2019": 0.0555,
	 	 "D018_precio_medio_evento_2019": 0.0555,

	 	 "D019_novatos_evento_2018": 5.4,
	 	 "D020_novatos_media_evento_2018": 9.55,
	 	 "D021_novatos_evento_2019": 4.0,
	 	 "D022_novatos_media_evento_2019": 6.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-leyendas del rock-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Albacete', 'Viajes en evento': 6, 'Viajes normal': 0.29, 'Proporción': 2060.38}, 1: {'City': 'Madrid', 'Viajes en evento': 17, 'Viajes normal': 2.17, 'Proporción': 784.12}, 2: {'City': 'Valencia', 'Viajes en evento': 7, 'Viajes normal': 0.93, 'Proporción': 753.85}, 3: {'City': 'Murcia', 'Viajes en evento': 4, 'Viajes normal': 0.11, 'Proporción': 3754.84}, 4: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.17, 'Proporción': 1171.43}, 5: {'City': 'Castellón de la Plana ', 'Viajes en evento': 2, 'Viajes normal': 0.04, 'Proporción': 4533.33}, 6: {'City': 'Granada', 'Viajes en evento': 3, 'Viajes normal': 0.28, 'Proporción': 1057.89}, 7: {'City': 'Alcalá de Henares', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1566.67}, 8: {'City': 'Bilbao', 'Viajes en evento': 2, 'Viajes normal': 0.13, 'Proporción': 1500.0}, 9: {'City': 'Córdoba', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2500.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-leyendas del rock-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 36, 'Viajes normal': 2.79, 'Proporción': 1289.36}, 1: {'City': 'Valencia', 'Viajes en evento': 9, 'Viajes normal': 1.2, 'Proporción': 750.83}, 2: {'City': 'Murcia', 'Viajes en evento': 4, 'Viajes normal': 0.11, 'Proporción': 3648.0}, 3: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.32, 'Proporción': 312.37}, 4: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.19, 'Proporción': 1040.0}, 5: {'City': 'Zaragoza', 'Viajes en evento': 4, 'Viajes normal': 0.35, 'Proporción': 1150.0}, 6: {'City': 'La Roda', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': 23800.0}, 7: {'City': 'Gijón', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 8: {'City': 'Granada', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 533.33}, 9: {'City': 'Pozuelo de Alarcón', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1200.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-leyendas del rock-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 12, 'Viajes normal': 2.31, 'Proporción': 519.81}, 1: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 1.08, 'Proporción': 185.28}, 2: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.38, 'Proporción': 265.69}, 3: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.29, 'Proporción': 340.91}, 4: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.16, 'Proporción': 606.25}, 5: {'City': 'Rivas-Vaciamadrid', 'Viajes en evento': 1, 'Viajes normal': 0.13, 'Proporción': 766.67}, 6: {'City': 'San Fernulldo', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2600.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-leyendas del rock-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 10, 'Viajes normal': 2.97, 'Proporción': 337.04}, 1: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.37, 'Proporción': 268.14}, 2: {'City': 'Valencia', 'Viajes en evento': 3, 'Viajes normal': 1.52, 'Proporción': 197.83}, 3: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1584.21}, 4: {'City': 'Barcelona', 'Viajes en evento': 1, 'Viajes normal': 0.21, 'Proporción': 466.67}, 5: {'City': 'Fuenlabrada', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 525.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_leyendas del rock_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_leyendas del rock_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['NIGHTWISH', 'THUNDER', 'POWERWOLF', 'MR BIG', 'SEPULTURA', 'WASP', 'CHILDREN OF BODOM', 'FOZZY', 'SUICIDAL TENDENCIES', 'SAXON', 'KAMELOC', 'WARCRY', 'VANTANTO', 'RUSISAS', 'WARLOCK', 'STRAVAGANZZA', 'ABBATH', 'ROSENDO'],
	 	 "artistas_evento_2019":['THINKLIZZY', 'OBUS', 'APOCALYPTICA', 'RATA BLANCA', 'ALESTORM', 'AVALANCH', 'AIRBOURNE', 'HAMMERFALL', 'LACULA COIL', 'BEAST IN BLACK', 'AVANTASIA', 'AVATAR', 'WARCRY', 'TIERRA SANTA', 'SARATOGA', 'MEDINA AZAHARA', 'METAL CHURCH', 'CANDLEMASS', 'MOJINOS ESCOZIOS', 'ROSE TATTOO', 'DEEDSNIDER', 'SAUROM'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_leyendas del rock_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_leyendas del rock_2019.jpg",
	 }, 
	 { 
	 	 "id": "27",
	 	 "nombre": "LOW FESTIVAL",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Benidorm",
	 	 "fecha_ini_2018": "27-07-2018",
	 	 "fecha_fin_2018": "29-07-2018",
	 	 "fecha_ini_2019": "26-07-2019",
	 	 "fecha_fin_2019": "28-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 77.0,
	 	 "D002_evento_media_viajes_2018": 16.52,
	 	 "D003_evento_media_viajes_dia_semana_2018": 30.4,
	 	 "D004_evento_viajes_2019": 66.5,
	 	 "D005_evento_media_viajes_2019": 18.14,
	 	 "D006_evento_media_viajes_dia_semana_2019": 32.24,

	 	 "D007_proporcion_asientos_evento_2018": 34.97,
	 	 "D008_proporcion_asientos_media_evento_2018": 12.04,
	 	 "D009_proporcion_asientos_evento_2019": 37.46,
	 	 "D010_proporcion_asientos_media_evento_2019": 13.17,

	 	 "D011_ocupacion_asientos_evento_2018": 0.09,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.68,
	 	 "D013_ocupacion_asientos_evento_2019": 0.08,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.67,

	 	 "D015_precio_evento_2018": 0.0608,
	 	 "D016_precio_medio_evento_2018": 0.0571,
	 	 "D017_precio_evento_2019": 0.0612,
	 	 "D018_precio_medio_evento_2019": 0.0589,

	 	 "D019_novatos_evento_2018": 32.75,
	 	 "D020_novatos_media_evento_2018": 12.13,
	 	 "D021_novatos_evento_2019": 24.5,
	 	 "D022_novatos_media_evento_2019": 10.98,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-low festival-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 124, 'Viajes normal': 4.98, 'Proporción': 2488.2}, 1: {'City': 'Valencia', 'Viajes en evento': 57, 'Viajes normal': 5.19, 'Proporción': 1099.05}, 2: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.22, 'Proporción': 1378.48}, 3: {'City': 'Albacete', 'Viajes en evento': 8, 'Viajes normal': 0.42, 'Proporción': 1893.88}, 4: {'City': 'Murcia', 'Viajes en evento': 17, 'Viajes normal': 0.73, 'Proporción': 2321.79}, 5: {'City': 'Barcelona', 'Viajes en evento': 15, 'Viajes normal': 0.75, 'Proporción': 2002.62}, 6: {'City': 'Zaragoza', 'Viajes en evento': 12, 'Viajes normal': 0.45, 'Proporción': 2674.29}, 7: {'City': 'Alcoi', 'Viajes en evento': 8, 'Viajes normal': 0.46, 'Proporción': 1722.67}, 8: {'City': 'Granada', 'Viajes en evento': 7, 'Viajes normal': 0.61, 'Proporción': 1151.61}, 9: {'City': 'Almansa', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1473.33}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-low festival-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 108, 'Viajes normal': 5.79, 'Proporción': 1866.52}, 1: {'City': 'Valencia', 'Viajes en evento': 42, 'Viajes normal': 4.87, 'Proporción': 862.12}, 2: {'City': 'Alicante', 'Viajes en evento': 6, 'Viajes normal': 0.28, 'Proporción': 2171.43}, 3: {'City': 'Albacete', 'Viajes en evento': 12, 'Viajes normal': 0.43, 'Proporción': 2768.5}, 4: {'City': 'Murcia', 'Viajes en evento': 14, 'Viajes normal': 0.74, 'Proporción': 1890.32}, 5: {'City': 'Granada', 'Viajes en evento': 7, 'Viajes normal': 0.63, 'Proporción': 1106.76}, 6: {'City': 'Zaragoza', 'Viajes en evento': 9, 'Viajes normal': 0.42, 'Proporción': 2148.75}, 7: {'City': 'Gandia', 'Viajes en evento': 3, 'Viajes normal': 0.17, 'Proporción': 1740.0}, 8: {'City': 'Elche', 'Viajes en evento': 2, 'Viajes normal': 0.2, 'Proporción': 1003.85}, 9: {'City': 'Barcelona', 'Viajes en evento': 8, 'Viajes normal': 0.6, 'Proporción': 1340.54}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-low festival-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 65, 'Viajes normal': 5.26, 'Proporción': 1236.8}, 1: {'City': 'Valencia', 'Viajes en evento': 37, 'Viajes normal': 5.32, 'Proporción': 695.42}, 2: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 536.76}, 3: {'City': 'Murcia', 'Viajes en evento': 14, 'Viajes normal': 0.83, 'Proporción': 1684.83}, 4: {'City': 'Albacete', 'Viajes en evento': 2, 'Viajes normal': 0.36, 'Proporción': 550.4}, 5: {'City': 'Barcelona', 'Viajes en evento': 6, 'Viajes normal': 0.7, 'Proporción': 851.52}, 6: {'City': 'Granada', 'Viajes en evento': 3, 'Viajes normal': 0.6, 'Proporción': 500.57}, 7: {'City': 'Alcoi', 'Viajes en evento': 7, 'Viajes normal': 0.46, 'Proporción': 1512.75}, 8: {'City': 'Zaragoza', 'Viajes en evento': 3, 'Viajes normal': 0.51, 'Proporción': 584.87}, 9: {'City': 'Sevilla', 'Viajes en evento': 3, 'Viajes normal': 0.25, 'Proporción': 1191.89}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-low festival-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 61, 'Viajes normal': 5.96, 'Proporción': 1023.96}, 1: {'City': 'Valencia', 'Viajes en evento': 18, 'Viajes normal': 5.4, 'Proporción': 333.25}, 2: {'City': 'Albacete', 'Viajes en evento': 3, 'Viajes normal': 0.53, 'Proporción': 561.54}, 3: {'City': 'Murcia', 'Viajes en evento': 5, 'Viajes normal': 0.83, 'Proporción': 602.46}, 4: {'City': 'Granada', 'Viajes en evento': 4, 'Viajes normal': 0.61, 'Proporción': 660.65}, 5: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.33, 'Proporción': 609.3}, 6: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.49, 'Proporción': 407.41}, 7: {'City': 'Barcelona', 'Viajes en evento': 5, 'Viajes normal': 0.63, 'Proporción': 788.46}, 8: {'City': 'Bilbao', 'Viajes en evento': 3, 'Viajes normal': 0.19, 'Proporción': 1600.0}, 9: {'City': 'Torrevieja', 'Viajes en evento': 2, 'Viajes normal': 0.21, 'Proporción': 950.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_low festival_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_low festival_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['PHOENIX', 'THE CHEMICAL BROTHERS', 'IZAL', 'BIFFY CLYRO', 'CARLOS SADNESS', 'EDITORS', 'LA M.O.D.A', 'KERO KERO BONITO', 'IVAN FERREIRO', 'LA BIEN QUERIDA', 'VITALIC', 'LA HABITACION ROJA', 'JAVIERA MENA', 'KAKKMADDAFAKKA', 'LOS PLANETAS', 'NOVEDADES CARMINHA', 'LEON BENAVENTE', 'ALEX METRIC', 'VINTAGE TROUBLE', 'NIÑOS MUTANTES', 'ELYELLA', 'AMATRIA', 'WOODS', 'PERRO', 'VIVE LA FETE', 'DERBY MOTORETA BURRITO KACHIMBA', 'EGON SODA', 'LA PLATA', 'SIENNA', 'MODELO DE RESPUESTA POLAR', 'LAS CHILLERS'],
	 	 "artistas_evento_2019":['BASTILLE', 'FOALS', 'NEW ORDER', 'VETUSTA MORLA', 'SECOND', 'THE VACCINES', 'ZAHARA', 'LA CASA AZUL', 'AIRBAG', 'CUPIDO', 'MISS CAFFEINA', 'FANGORIA', 'CUT COPY', 'XOEL LOPEZ', 'DORIAN', 'YUKSEK', 'VIVA SUECIA', 'CHANCHA VIA CIRCUITO', 'CAROLINA DURANTE', 'LA ZOWI', 'LADYTRON', 'VARRY BRAVA', 'FULL', 'CARIÑO', 'FISCHERSPOONER', 'LAS LIGAS MENORES', 'THE NEW RAEMON', 'ALICE WONDER', 'NUNATAK', 'REX THE DOG', 'MUCHO', 'MASTODONTE', 'LEY DJ', 'YO DIABLO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_low festival_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_low festival_2019.jpg",
	 }, 
	 { 
	 	 "id": "28",
	 	 "nombre": "MAD COOL",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Madrid",
	 	 "fecha_ini_2018": "12-07-2018",
	 	 "fecha_fin_2018": "14-07-2018",
	 	 "fecha_ini_2019": "11-07-2019",
	 	 "fecha_fin_2019": "13-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 728.25,
	 	 "D002_evento_media_viajes_2018": 744.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 596.87,
	 	 "D004_evento_viajes_2019": 605.0,
	 	 "D005_evento_media_viajes_2019": 804.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 603.14,

	 	 "D007_proporcion_asientos_evento_2018": 38.38,
	 	 "D008_proporcion_asientos_media_evento_2018": 26.7,
	 	 "D009_proporcion_asientos_evento_2019": 35.24,
	 	 "D010_proporcion_asientos_media_evento_2019": 28.01,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.8,

	 	 "D015_precio_evento_2018": 0.0498,
	 	 "D016_precio_medio_evento_2018": 0.0491,
	 	 "D017_precio_evento_2019": 0.0505,
	 	 "D018_precio_medio_evento_2019": 0.0502,

	 	 "D019_novatos_evento_2018": 185.5,
	 	 "D020_novatos_media_evento_2018": 214.74,
	 	 "D021_novatos_evento_2019": 135.5,
	 	 "D022_novatos_media_evento_2019": 184.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-mad cool-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 333, 'Viajes normal': 75.84, 'Proporción': 439.09}, 1: {'City': 'Murcia', 'Viajes en evento': 164, 'Viajes normal': 38.76, 'Proporción': 423.07}, 2: {'City': 'Zaragoza', 'Viajes en evento': 63, 'Viajes normal': 14.25, 'Proporción': 442.04}, 3: {'City': 'Barcelona', 'Viajes en evento': 153, 'Viajes normal': 23.15, 'Proporción': 660.81}, 4: {'City': 'Albacete', 'Viajes en evento': 55, 'Viajes normal': 15.02, 'Proporción': 366.13}, 5: {'City': 'Granada', 'Viajes en evento': 65, 'Viajes normal': 22.2, 'Proporción': 292.76}, 6: {'City': 'Sevilla', 'Viajes en evento': 65, 'Viajes normal': 21.66, 'Proporción': 300.09}, 7: {'City': 'Burgos', 'Viajes en evento': 54, 'Viajes normal': 15.87, 'Proporción': 340.3}, 8: {'City': 'Alicante', 'Viajes en evento': 100, 'Viajes normal': 24.77, 'Proporción': 403.72}, 9: {'City': 'Málaga', 'Viajes en evento': 60, 'Viajes normal': 12.3, 'Proporción': 487.97}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-mad cool-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 289, 'Viajes normal': 81.06, 'Proporción': 356.53}, 1: {'City': 'Murcia', 'Viajes en evento': 150, 'Viajes normal': 42.04, 'Proporción': 356.81}, 2: {'City': 'Zaragoza', 'Viajes en evento': 43, 'Viajes normal': 15.0, 'Proporción': 286.67}, 3: {'City': 'Albacete', 'Viajes en evento': 47, 'Viajes normal': 15.92, 'Proporción': 295.15}, 4: {'City': 'Burgos', 'Viajes en evento': 60, 'Viajes normal': 17.33, 'Proporción': 346.24}, 5: {'City': 'Granada', 'Viajes en evento': 57, 'Viajes normal': 21.68, 'Proporción': 262.86}, 6: {'City': 'Sevilla', 'Viajes en evento': 57, 'Viajes normal': 20.66, 'Proporción': 275.84}, 7: {'City': 'Alicante', 'Viajes en evento': 69, 'Viajes normal': 24.62, 'Proporción': 280.32}, 8: {'City': 'Málaga', 'Viajes en evento': 32, 'Viajes normal': 12.45, 'Proporción': 257.01}, 9: {'City': 'Mérida', 'Viajes en evento': 31, 'Viajes normal': 11.9, 'Proporción': 260.4}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-mad cool-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 397, 'Viajes normal': 74.61, 'Proporción': 532.11}, 1: {'City': 'Murcia', 'Viajes en evento': 181, 'Viajes normal': 37.84, 'Proporción': 478.28}, 2: {'City': 'Albacete', 'Viajes en evento': 72, 'Viajes normal': 13.73, 'Proporción': 524.24}, 3: {'City': 'Burgos', 'Viajes en evento': 70, 'Viajes normal': 15.47, 'Proporción': 452.45}, 4: {'City': 'Alicante', 'Viajes en evento': 161, 'Viajes normal': 24.09, 'Proporción': 668.39}, 5: {'City': 'Sevilla', 'Viajes en evento': 93, 'Viajes normal': 19.96, 'Proporción': 465.89}, 6: {'City': 'Granada', 'Viajes en evento': 86, 'Viajes normal': 21.29, 'Proporción': 403.89}, 7: {'City': 'Bilbao', 'Viajes en evento': 184, 'Viajes normal': 21.23, 'Proporción': 866.8}, 8: {'City': 'Zaragoza', 'Viajes en evento': 53, 'Viajes normal': 13.25, 'Proporción': 400.02}, 9: {'City': 'Barcelona', 'Viajes en evento': 138, 'Viajes normal': 22.06, 'Proporción': 625.48}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-mad cool-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 425, 'Viajes normal': 80.15, 'Proporción': 530.27}, 1: {'City': 'Murcia', 'Viajes en evento': 215, 'Viajes normal': 41.38, 'Proporción': 519.55}, 2: {'City': 'Albacete', 'Viajes en evento': 71, 'Viajes normal': 14.29, 'Proporción': 496.75}, 3: {'City': 'Burgos', 'Viajes en evento': 84, 'Viajes normal': 16.48, 'Proporción': 509.7}, 4: {'City': 'Granada', 'Viajes en evento': 92, 'Viajes normal': 20.91, 'Proporción': 439.96}, 5: {'City': 'Alicante', 'Viajes en evento': 151, 'Viajes normal': 23.83, 'Proporción': 633.77}, 6: {'City': 'Sevilla', 'Viajes en evento': 83, 'Viajes normal': 20.65, 'Proporción': 401.85}, 7: {'City': 'Bilbao', 'Viajes en evento': 176, 'Viajes normal': 21.58, 'Proporción': 815.61}, 8: {'City': 'Barcelona', 'Viajes en evento': 125, 'Viajes normal': 19.59, 'Proporción': 638.12}, 9: {'City': 'Ortigueira', 'Viajes en evento': 145, 'Viajes normal': 2.6, 'Proporción': 5576.92}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_mad cool_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_mad cool_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['POST MALONE', 'DUA LIPA', 'MAGA', 'TAME IMPALA', 'ARTIC MONKEYS', 'PEARL JAM', 'JACK JOHNSON', 'JAMES BAY', 'LEON BRIDGES', 'MGMT', 'DEPECHE MODE', 'SNOW PATROL', 'ALICE IN CHAINS', 'QUEENS OF THE STONE AGE', 'BEN HOWARD', 'KALEO', 'NINE INCH NAILS', 'MASSIVE ATTACK', 'FRANZ FERDINAND', 'KASABIAN', 'PAUL KALKBRENNER', 'SAMPHA', 'JET', 'JUSTICE', 'YO LA TENGO', 'KASE.O', 'THE WHITE BUFFALO', 'EELS', 'WASHED OUT', 'JACK WHITE', 'PERFUME GENIUS', 'LA M.O.D.A', 'FUTURE ISLANS', 'UNDERWORLD', 'IVAN FERREIRO', 'FIDLAR', 'MAYA JANE COLES', 'PORCHES', 'FRIENDLY FIRES', 'CAROLINA DURANTE', 'AT THE DRIVE IN', 'ELYELLA', 'RUFUS T.FIREFLY', 'JAPANDROIDS', 'ANGEL STANICH', 'RICHIE HAWTIN', 'TOUNDRA', 'MODELO DE RESPUESTA POLAR', 'NIÑA COYOTE ETA CHICO TORNADO', 'CORA NOVOA', 'PLEET FOXES'],
	 	 "artistas_evento_2019":['DISCLOSURE', 'THE 1975', 'BON IVER', 'THE CURE', 'JORJA SMITH', 'MARINA', 'YEARS & YEARS', 'VAMPIRE WEEKEND', 'KAYTRANADA', 'THE SMASHING PUMPKINS', 'VINCE STAPLES', 'MS. LAURYN HILL', 'THE NATIONAL', 'TASH SULTANA', 'NAO', 'EMPIRE OF THE SUN', 'AMERICAN AUTHORS', 'ROBYN', 'GRETA VAN FLEET', 'BONOBO', 'THE CHEMICAL BROTHERS', 'IGGY POP', 'VETUSTA MORLA', 'ERIC PRYDZ', 'ALMA', 'SHARON VAN ETTEN', 'WOLFMOTHER', 'JON HOPKINS', 'MILES KANE', 'CAT POWER', 'NOEL GALLAGHER', 'THE HIVES', 'MOGWAI', 'LA DISPUTE', 'GOSSIP', 'ROLLING BLACKOUTS', 'DELAPORTE', 'PROPHETS OF RAGE', 'PERRY FARRELL'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_mad cool_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_mad cool_2019.jpg",
	 }, 
	 { 
	 	 "id": "29",
	 	 "nombre": "MALLORCA LIVE",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Palma",
	 	 "fecha_ini_2018": "11-05-2018",
	 	 "fecha_fin_2018": "12-05-2018",
	 	 "fecha_ini_2019": "10-05-2019",
	 	 "fecha_fin_2019": "11-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": null,
	 	 "D002_evento_media_viajes_2018": 0.14,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.11,
	 	 "D004_evento_viajes_2019": null,
	 	 "D005_evento_media_viajes_2019": 0.1,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.08,

	 	 "D007_proporcion_asientos_evento_2018": null,
	 	 "D008_proporcion_asientos_media_evento_2018": 2.2,
	 	 "D009_proporcion_asientos_evento_2019": null,
	 	 "D010_proporcion_asientos_media_evento_2019": 2.59,

	 	 "D011_ocupacion_asientos_evento_2018": 0.0,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.7,
	 	 "D013_ocupacion_asientos_evento_2019": 0.0,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.58,

	 	 "D015_precio_evento_2018": null,
	 	 "D016_precio_medio_evento_2018": 0.0754,
	 	 "D017_precio_evento_2019": null,
	 	 "D018_precio_medio_evento_2019": 0.0751,

	 	 "D019_novatos_evento_2018": null,
	 	 "D020_novatos_media_evento_2018": 0.58,
	 	 "D021_novatos_evento_2019": null,
	 	 "D022_novatos_media_evento_2019": 0.33,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-mallorca live-2018.html",
	 	 "tabla_origenes_evento_2018":{},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-mallorca live-2019.html",
	 	 "tabla_origenes_evento_2019":{},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-mallorca live-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-mallorca live-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_mallorca live_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_mallorca live_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MORGAN', 'BAD GYAL', 'THE PRODIGY', 'IZAL', 'KASE.O', 'MACACO', 'SOLOMUN', 'PRIMAL SCREAM', 'LA CASA AZUL', 'LA RAIZ', 'VITALIC', 'HENRIK SCHWARZ', 'CHANCHA VIA CIRCUITO', 'BLACK LIPS', '!!! CHK CHK CHK', 'NINA KRAVIZ', 'MUCHACHITO', 'L.A.', 'EL COLUMPIO ASESINO', 'POLOCK', 'OSO LEONE', 'SEXY SADIE', 'DJ COCO'],
	 	 "artistas_evento_2019":['TWO DOOR CINEMA CLUB', 'JAMIROQUAI', 'BABASONICOS', 'VETUSTA MORLA', 'DELLAFUENTE', 'AYAX Y PROK', "HER'S", 'THE VACCINES', 'SECOND', 'AMAIA', 'LA M.O.D.A', 'LA PEGATINA', 'MAYA JANE COLES', 'EL BUHO', 'FUEL FANDANGO', 'VIVA SUECIA', 'NOVEDADES CARMINHA', 'LAURENT GARNIER', 'DENGUE DENGUE DENGUE', 'DIXON', 'RAMON MIRABET', 'BAIUCA', 'MATHEW JONSON', 'LEY DJ', 'THE MANI-LAS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_mallorca live_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_mallorca live_2019.jpg",
	 }, 
	 { 
	 	 "id": "30",
	 	 "nombre": "MAREAROCK",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Alicante",
	 	 "fecha_ini_2018": "13-04-2018",
	 	 "fecha_fin_2018": "14-04-2018",
	 	 "fecha_ini_2019": "13-04-2019",
	 	 "fecha_fin_2019": "14-04-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 88.0,
	 	 "D002_evento_media_viajes_2018": 86.64,
	 	 "D003_evento_media_viajes_dia_semana_2018": 129.25,
	 	 "D004_evento_viajes_2019": 148.33,
	 	 "D005_evento_media_viajes_2019": 90.83,
	 	 "D006_evento_media_viajes_dia_semana_2019": 85.0,

	 	 "D007_proporcion_asientos_evento_2018": 30.7,
	 	 "D008_proporcion_asientos_media_evento_2018": 16.88,
	 	 "D009_proporcion_asientos_evento_2019": 34.62,
	 	 "D010_proporcion_asientos_media_evento_2019": 16.88,

	 	 "D011_ocupacion_asientos_evento_2018": 0.01,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.74,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.69,

	 	 "D015_precio_evento_2018": 0.0554,
	 	 "D016_precio_medio_evento_2018": 0.056,
	 	 "D017_precio_evento_2019": 0.0567,
	 	 "D018_precio_medio_evento_2019": 0.0575,

	 	 "D019_novatos_evento_2018": 17.33,
	 	 "D020_novatos_media_evento_2018": 40.93,
	 	 "D021_novatos_evento_2019": 29.67,
	 	 "D022_novatos_media_evento_2019": 36.85,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-marearock-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 65, 'Viajes normal': 18.23, 'Proporción': 356.61}, 1: {'City': 'Madrid', 'Viajes en evento': 76, 'Viajes normal': 24.09, 'Proporción': 315.51}, 2: {'City': 'Murcia', 'Viajes en evento': 12, 'Viajes normal': 5.09, 'Proporción': 235.74}, 3: {'City': 'Albacete', 'Viajes en evento': 18, 'Viajes normal': 3.55, 'Proporción': 506.94}, 4: {'City': 'Granada', 'Viajes en evento': 13, 'Viajes normal': 5.25, 'Proporción': 247.52}, 5: {'City': 'Alcoi', 'Viajes en evento': 7, 'Viajes normal': 1.89, 'Proporción': 369.75}, 6: {'City': 'Almería', 'Viajes en evento': 8, 'Viajes normal': 1.77, 'Proporción': 453.0}, 7: {'City': 'Barcelona', 'Viajes en evento': 7, 'Viajes normal': 2.65, 'Proporción': 264.05}, 8: {'City': 'Dénia', 'Viajes en evento': 5, 'Viajes normal': 1.85, 'Proporción': 270.58}, 9: {'City': 'Málaga', 'Viajes en evento': 5, 'Viajes normal': 2.13, 'Proporción': 234.72}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-marearock-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 81, 'Viajes normal': 18.99, 'Proporción': 426.61}, 1: {'City': 'Madrid', 'Viajes en evento': 125, 'Viajes normal': 23.83, 'Proporción': 524.64}, 2: {'City': 'Murcia', 'Viajes en evento': 28, 'Viajes normal': 6.0, 'Proporción': 466.67}, 3: {'City': 'Albacete', 'Viajes en evento': 15, 'Viajes normal': 3.26, 'Proporción': 459.68}, 4: {'City': 'Granada', 'Viajes en evento': 31, 'Viajes normal': 4.86, 'Proporción': 637.62}, 5: {'City': 'Alcoi', 'Viajes en evento': 10, 'Viajes normal': 2.13, 'Proporción': 469.86}, 6: {'City': 'Barcelona', 'Viajes en evento': 12, 'Viajes normal': 2.43, 'Proporción': 492.86}, 7: {'City': 'Zaragoza', 'Viajes en evento': 11, 'Viajes normal': 1.82, 'Proporción': 605.95}, 8: {'City': 'Málaga', 'Viajes en evento': 10, 'Viajes normal': 2.49, 'Proporción': 400.8}, 9: {'City': 'Almería', 'Viajes en evento': 8, 'Viajes normal': 1.56, 'Proporción': 513.92}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-marearock-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 43, 'Viajes normal': 18.29, 'Proporción': 235.13}, 1: {'City': 'Madrid', 'Viajes en evento': 65, 'Viajes normal': 24.77, 'Proporción': 262.42}, 2: {'City': 'Murcia', 'Viajes en evento': 14, 'Viajes normal': 4.79, 'Proporción': 292.17}, 3: {'City': 'Albacete', 'Viajes en evento': 12, 'Viajes normal': 3.27, 'Proporción': 366.83}, 4: {'City': 'Granada', 'Viajes en evento': 11, 'Viajes normal': 5.24, 'Proporción': 210.1}, 5: {'City': 'Alcoi', 'Viajes en evento': 5, 'Viajes normal': 1.91, 'Proporción': 262.21}, 6: {'City': 'Barcelona', 'Viajes en evento': 7, 'Viajes normal': 2.73, 'Proporción': 256.53}, 7: {'City': 'Benidorm', 'Viajes en evento': 3, 'Viajes normal': 0.22, 'Proporción': 1378.48}, 8: {'City': 'Sevilla', 'Viajes en evento': 11, 'Viajes normal': 2.08, 'Proporción': 527.88}, 9: {'City': 'Zaragoza', 'Viajes en evento': 5, 'Viajes normal': 1.74, 'Proporción': 287.36}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-marearock-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 67, 'Viajes normal': 19.66, 'Proporción': 340.83}, 1: {'City': 'Madrid', 'Viajes en evento': 74, 'Viajes normal': 24.62, 'Proporción': 300.63}, 2: {'City': 'Murcia', 'Viajes en evento': 29, 'Viajes normal': 5.71, 'Proporción': 507.83}, 3: {'City': 'Albacete', 'Viajes en evento': 16, 'Viajes normal': 3.26, 'Proporción': 491.31}, 4: {'City': 'Granada', 'Viajes en evento': 23, 'Viajes normal': 4.81, 'Proporción': 478.58}, 5: {'City': 'Alcoi', 'Viajes en evento': 6, 'Viajes normal': 2.37, 'Proporción': 252.98}, 6: {'City': 'Benidorm', 'Viajes en evento': 1, 'Viajes normal': 0.28, 'Proporción': 361.9}, 7: {'City': 'Málaga', 'Viajes en evento': 12, 'Viajes normal': 2.5, 'Proporción': 480.64}, 8: {'City': 'Dénia', 'Viajes en evento': 6, 'Viajes normal': 2.04, 'Proporción': 294.65}, 9: {'City': 'Zaragoza', 'Viajes en evento': 8, 'Viajes normal': 1.85, 'Proporción': 433.45}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_marearock_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_marearock_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['LA RAIZ', 'DESAKATO', 'GATILLAZO', 'SONS OF AGUIRRE', 'KONSUMO RESPETO', 'TRASHTUCADA', 'KONTAMINACIO', 'PURA MANDANGA'],
	 	 "artistas_evento_2019":['TANAKA', 'EBRI KNIGHT', 'ARPAVIEJAS', 'DSKARRILA', 'EL KAMION DE LA BASURA', 'ALKAYATA', 'MUGROMAN', 'IVAN SERRANO', 'ESCARLATES', 'TRANSPOTIN Y LA BOLETOBANDA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_marearock_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_marearock_2019.jpg",
	 }, 
	 { 
	 	 "id": "31",
	 	 "nombre": "MEO SUDOESTE",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Beja",
	 	 "fecha_ini_2018": "07-08-2018",
	 	 "fecha_fin_2018": "11-08-2018",
	 	 "fecha_ini_2019": "06-08-2019",
	 	 "fecha_fin_2019": "10-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": null,
	 	 "D002_evento_media_viajes_2018": 0.04,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.0,
	 	 "D004_evento_viajes_2019": null,
	 	 "D005_evento_media_viajes_2019": 0.05,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.0,

	 	 "D007_proporcion_asientos_evento_2018": null,
	 	 "D008_proporcion_asientos_media_evento_2018": 0.34,
	 	 "D009_proporcion_asientos_evento_2019": null,
	 	 "D010_proporcion_asientos_media_evento_2019": 0.35,

	 	 "D011_ocupacion_asientos_evento_2018": 0.0,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.25,
	 	 "D013_ocupacion_asientos_evento_2019": 0.0,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.17,

	 	 "D015_precio_evento_2018": null,
	 	 "D016_precio_medio_evento_2018": 0.0527,
	 	 "D017_precio_evento_2019": null,
	 	 "D018_precio_medio_evento_2019": 0.0574,

	 	 "D019_novatos_evento_2018": null,
	 	 "D020_novatos_media_evento_2018": 1.09,
	 	 "D021_novatos_evento_2019": null,
	 	 "D022_novatos_media_evento_2019": 0.8,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-meo sudoeste-2018.html",
	 	 "tabla_origenes_evento_2018":{},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-meo sudoeste-2019.html",
	 	 "tabla_origenes_evento_2019":{},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-meo sudoeste-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-meo sudoeste-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_meo sudoeste_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_meo sudoeste_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['J BALVIN', 'SHAWN MENDES', 'MARSHMELLO', 'JASON DERULO', 'LIL PUMP', 'WIZKID', 'DON DIABLO', 'HARDWELL', 'DESIIGNER', 'MC FIOTI', 'PIRUKA', 'DIOGO PICARRA', 'BLAYA', 'KARETUS'],
	 	 "artistas_evento_2019":['POST MALONE', 'ANITTA', 'RUSS', '6LACK', 'STEVE AOKI', 'RITA ORA', 'TIMMY TRUMPET', 'YEARS & YEARS', 'VITOR KLEY', 'VINI VICI', 'JOSS STONE', 'WET BED GANG', 'KURA', 'CAROLINA DESLANDES', 'JIMMY P', 'BLAYA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_meo sudoeste_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_meo sudoeste_2019.jpg",
	 }, 
	 { 
	 	 "id": "32",
	 	 "nombre": "NOROESTE ESTRELLA GALICIA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "A Coruña",
	 	 "fecha_ini_2018": "07-08-2018",
	 	 "fecha_fin_2018": "12-08-2018",
	 	 "fecha_ini_2019": "06-08-2019",
	 	 "fecha_fin_2019": "11-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 52.714285714285715,
	 	 "D002_evento_media_viajes_2018": 46.82,
	 	 "D003_evento_media_viajes_dia_semana_2018": 36.88,
	 	 "D004_evento_viajes_2019": 59.57,
	 	 "D005_evento_media_viajes_2019": 51.94,
	 	 "D006_evento_media_viajes_dia_semana_2019": 33.52,

	 	 "D007_proporcion_asientos_evento_2018": 32.07,
	 	 "D008_proporcion_asientos_media_evento_2018": 19.45,
	 	 "D009_proporcion_asientos_evento_2019": 35.51,
	 	 "D010_proporcion_asientos_media_evento_2019": 20.51,

	 	 "D011_ocupacion_asientos_evento_2018": 0.04,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.59,
	 	 "D013_ocupacion_asientos_evento_2019": 0.04,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.59,

	 	 "D015_precio_evento_2018": 0.0555,
	 	 "D016_precio_medio_evento_2018": 0.0548,
	 	 "D017_precio_evento_2019": 0.0595,
	 	 "D018_precio_medio_evento_2019": 0.0564,

	 	 "D019_novatos_evento_2018": 11.43,
	 	 "D020_novatos_media_evento_2018": 16.18,
	 	 "D021_novatos_evento_2019": 9.0,
	 	 "D022_novatos_media_evento_2019": 14.26,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-noroeste estrella galicia-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Lugo', 'Viajes en evento': 41, 'Viajes normal': 8.25, 'Proporción': 497.01}, 1: {'City': 'Madrid', 'Viajes en evento': 103, 'Viajes normal': 9.77, 'Proporción': 1054.56}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 14, 'Viajes normal': 2.93, 'Proporción': 478.46}, 3: {'City': 'Ponferrada', 'Viajes en evento': 12, 'Viajes normal': 1.79, 'Proporción': 669.72}, 4: {'City': 'Gijón', 'Viajes en evento': 34, 'Viajes normal': 1.96, 'Proporción': 1731.89}, 5: {'City': 'Vigo', 'Viajes en evento': 24, 'Viajes normal': 4.87, 'Proporción': 492.41}, 6: {'City': 'Astorga', 'Viajes en evento': 4, 'Viajes normal': 0.12, 'Proporción': 3322.22}, 7: {'City': 'Ourense', 'Viajes en evento': 16, 'Viajes normal': 3.05, 'Proporción': 523.96}, 8: {'City': 'León', 'Viajes en evento': 5, 'Viajes normal': 0.88, 'Proporción': 565.97}, 9: {'City': 'Pontevedra', 'Viajes en evento': 8, 'Viajes normal': 0.72, 'Proporción': 1113.33}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-noroeste estrella galicia-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 112, 'Viajes normal': 10.53, 'Proporción': 1063.67}, 1: {'City': 'Lugo', 'Viajes en evento': 48, 'Viajes normal': 9.29, 'Proporción': 516.53}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 18, 'Viajes normal': 2.76, 'Proporción': 652.39}, 3: {'City': 'Vigo', 'Viajes en evento': 41, 'Viajes normal': 5.82, 'Proporción': 704.98}, 4: {'City': 'Gijón', 'Viajes en evento': 38, 'Viajes normal': 2.0, 'Proporción': 1896.81}, 5: {'City': 'Ponferrada', 'Viajes en evento': 8, 'Viajes normal': 1.75, 'Proporción': 457.14}, 6: {'City': 'Ourense', 'Viajes en evento': 16, 'Viajes normal': 3.47, 'Proporción': 460.89}, 7: {'City': 'León', 'Viajes en evento': 6, 'Viajes normal': 0.93, 'Proporción': 644.88}, 8: {'City': 'Pontevedra', 'Viajes en evento': 7, 'Viajes normal': 0.81, 'Proporción': 867.77}, 9: {'City': 'Bilbao', 'Viajes en evento': 9, 'Viajes normal': 1.26, 'Proporción': 715.73}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-noroeste estrella galicia-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Lugo', 'Viajes en evento': 46, 'Viajes normal': 7.79, 'Proporción': 590.16}, 1: {'City': 'Madrid', 'Viajes en evento': 86, 'Viajes normal': 9.36, 'Proporción': 919.18}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 12, 'Viajes normal': 2.59, 'Proporción': 463.49}, 3: {'City': 'Vigo', 'Viajes en evento': 37, 'Viajes normal': 5.0, 'Proporción': 740.0}, 4: {'City': 'Gijón', 'Viajes en evento': 30, 'Viajes normal': 2.04, 'Proporción': 1467.9}, 5: {'City': 'Ponferrada', 'Viajes en evento': 9, 'Viajes normal': 1.45, 'Proporción': 622.16}, 6: {'City': 'León', 'Viajes en evento': 9, 'Viajes normal': 0.79, 'Proporción': 1137.4}, 7: {'City': 'Bilbao', 'Viajes en evento': 8, 'Viajes normal': 1.55, 'Proporción': 515.74}, 8: {'City': 'Ourense', 'Viajes en evento': 9, 'Viajes normal': 2.97, 'Proporción': 302.53}, 9: {'City': 'Porto', 'Viajes en evento': 10, 'Viajes normal': 0.57, 'Proporción': 1766.13}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-noroeste estrella galicia-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 83, 'Viajes normal': 9.86, 'Proporción': 841.91}, 1: {'City': 'Lugo', 'Viajes en evento': 42, 'Viajes normal': 9.44, 'Proporción': 444.88}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 11, 'Viajes normal': 2.39, 'Proporción': 460.61}, 3: {'City': 'Vigo', 'Viajes en evento': 53, 'Viajes normal': 5.85, 'Proporción': 905.24}, 4: {'City': 'Gijón', 'Viajes en evento': 34, 'Viajes normal': 2.1, 'Proporción': 1622.22}, 5: {'City': 'Ourense', 'Viajes en evento': 23, 'Viajes normal': 3.01, 'Proporción': 763.29}, 6: {'City': 'Ponferrada', 'Viajes en evento': 8, 'Viajes normal': 1.32, 'Proporción': 608.0}, 7: {'City': 'Ribadeo', 'Viajes en evento': 9, 'Viajes normal': 1.05, 'Proporción': 859.23}, 8: {'City': 'Pontevedra', 'Viajes en evento': 5, 'Viajes normal': 0.77, 'Proporción': 651.11}, 9: {'City': 'León', 'Viajes en evento': 8, 'Viajes normal': 0.9, 'Proporción': 884.9}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_noroeste estrella galicia_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_noroeste estrella galicia_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MORGAN', 'BOODOO', 'BELLE AND SEBASTIAN', 'NATHY PELUSO', 'NENEH CHERRY', 'TEMPLES', 'VIVA SUECIA', 'MORDEM', 'ANGEL STANICH', 'JAMES HOLDEN', 'TULSA', 'MARIA DEL MAR BONET', 'LA PLATA', 'CAPSULA', 'THE PRETENDERS', 'ESPEJOS Y DIAMANTES'],
	 	 "artistas_evento_2019":['ZU', 'PATTIY SMITH', 'NATHY PELUSO', 'ANNI B SWEET', 'JOE CREPUSCULO', 'LOS PUNSETES', 'BELAKO', 'PACO IBAÑEZ', 'DERBY MOTORETA BURRITO KACHIMBA', 'LOS VINAGRES', 'HOLLY MIRANDA', 'VOLCANES', 'ESCUCHANDO ELEFANTES', 'JULIO RESENDE', 'BASANTA', 'LADRILLO Y MUJER'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_noroeste estrella galicia_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_noroeste estrella galicia_2019.jpg",
	 }, 
	 { 
	 	 "id": "33",
	 	 "nombre": "O SON DO CAMIÑO",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Santiago de Compostela",
	 	 "fecha_ini_2018": "28-06-2018",
	 	 "fecha_fin_2018": "30-06-2018",
	 	 "fecha_ini_2019": "13-06-2019",
	 	 "fecha_fin_2019": "15-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 78.25,
	 	 "D002_evento_media_viajes_2018": 48.31,
	 	 "D003_evento_media_viajes_dia_semana_2018": 47.13,
	 	 "D004_evento_viajes_2019": 90.25,
	 	 "D005_evento_media_viajes_2019": 53.35,
	 	 "D006_evento_media_viajes_dia_semana_2019": 54.21,

	 	 "D007_proporcion_asientos_evento_2018": 33.18,
	 	 "D008_proporcion_asientos_media_evento_2018": 15.38,
	 	 "D009_proporcion_asientos_evento_2019": 29.96,
	 	 "D010_proporcion_asientos_media_evento_2019": 15.24,

	 	 "D011_ocupacion_asientos_evento_2018": 0.03,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.54,
	 	 "D013_ocupacion_asientos_evento_2019": 0.03,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.56,

	 	 "D015_precio_evento_2018": 0.0571,
	 	 "D016_precio_medio_evento_2018": 0.0577,
	 	 "D017_precio_evento_2019": 0.0601,
	 	 "D018_precio_medio_evento_2019": 0.06,

	 	 "D019_novatos_evento_2018": 18.25,
	 	 "D020_novatos_media_evento_2018": 17.65,
	 	 "D021_novatos_evento_2019": 19.75,
	 	 "D022_novatos_media_evento_2019": 15.09,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-o son do camiño-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Vigo', 'Viajes en evento': 59, 'Viajes normal': 7.68, 'Proporción': 767.74}, 1: {'City': 'A Coruña', 'Viajes en evento': 22, 'Viajes normal': 2.59, 'Proporción': 849.74}, 2: {'City': 'Ourense', 'Viajes en evento': 32, 'Viajes normal': 7.23, 'Proporción': 442.59}, 3: {'City': 'Madrid', 'Viajes en evento': 45, 'Viajes normal': 4.42, 'Proporción': 1017.06}, 4: {'City': 'Gijón', 'Viajes en evento': 28, 'Viajes normal': 1.55, 'Proporción': 1801.47}, 5: {'City': 'Lugo', 'Viajes en evento': 25, 'Viajes normal': 5.42, 'Proporción': 461.09}, 6: {'City': 'Ferrol', 'Viajes en evento': 18, 'Viajes normal': 3.14, 'Proporción': 572.8}, 7: {'City': 'Ribeira', 'Viajes en evento': 5, 'Viajes normal': 1.14, 'Proporción': 438.38}, 8: {'City': 'Porto', 'Viajes en evento': 14, 'Viajes normal': 1.54, 'Proporción': 908.91}, 9: {'City': 'Poio', 'Viajes en evento': 2, 'Viajes normal': 0.14, 'Proporción': 1455.32}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-o son do camiño-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Vigo', 'Viajes en evento': 66, 'Viajes normal': 8.35, 'Proporción': 790.86}, 1: {'City': 'A Coruña', 'Viajes en evento': 36, 'Viajes normal': 2.39, 'Proporción': 1507.44}, 2: {'City': 'Ourense', 'Viajes en evento': 54, 'Viajes normal': 8.47, 'Proporción': 637.51}, 3: {'City': 'Madrid', 'Viajes en evento': 45, 'Viajes normal': 5.1, 'Proporción': 882.59}, 4: {'City': 'Lugo', 'Viajes en evento': 29, 'Viajes normal': 6.44, 'Proporción': 450.03}, 5: {'City': 'Gijón', 'Viajes en evento': 26, 'Viajes normal': 1.63, 'Proporción': 1590.97}, 6: {'City': 'Ferrol', 'Viajes en evento': 18, 'Viajes normal': 3.21, 'Proporción': 560.53}, 7: {'City': 'Porto', 'Viajes en evento': 18, 'Viajes normal': 1.55, 'Proporción': 1160.9}, 8: {'City': 'Pontevedra', 'Viajes en evento': 1, 'Viajes normal': 0.41, 'Proporción': 242.4}, 9: {'City': 'Ribadeo', 'Viajes en evento': 6, 'Viajes normal': 1.2, 'Proporción': 501.89}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-o son do camiño-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Vigo', 'Viajes en evento': 24, 'Viajes normal': 8.22, 'Proporción': 292.1}, 1: {'City': 'A Coruña', 'Viajes en evento': 18, 'Viajes normal': 2.93, 'Proporción': 615.17}, 2: {'City': 'Ourense', 'Viajes en evento': 23, 'Viajes normal': 7.61, 'Proporción': 302.41}, 3: {'City': 'Lugo', 'Viajes en evento': 22, 'Viajes normal': 4.99, 'Proporción': 440.48}, 4: {'City': 'Gijón', 'Viajes en evento': 18, 'Viajes normal': 1.55, 'Proporción': 1159.78}, 5: {'City': 'Madrid', 'Viajes en evento': 21, 'Viajes normal': 3.63, 'Proporción': 578.33}, 6: {'City': 'Ferrol', 'Viajes en evento': 11, 'Viajes normal': 3.52, 'Proporción': 312.45}, 7: {'City': 'Ribeira', 'Viajes en evento': 5, 'Viajes normal': 0.96, 'Proporción': 522.88}, 8: {'City': 'Porto', 'Viajes en evento': 16, 'Viajes normal': 1.33, 'Proporción': 1202.78}, 9: {'City': 'Pontevedra', 'Viajes en evento': 2, 'Viajes normal': 0.3, 'Proporción': 667.89}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-o son do camiño-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Vigo', 'Viajes en evento': 29, 'Viajes normal': 9.21, 'Proporción': 314.97}, 1: {'City': 'A Coruña', 'Viajes en evento': 8, 'Viajes normal': 2.76, 'Proporción': 289.95}, 2: {'City': 'Ourense', 'Viajes en evento': 34, 'Viajes normal': 9.17, 'Proporción': 370.73}, 3: {'City': 'Lugo', 'Viajes en evento': 29, 'Viajes normal': 6.31, 'Proporción': 459.65}, 4: {'City': 'Ferrol', 'Viajes en evento': 14, 'Viajes normal': 3.49, 'Proporción': 401.13}, 5: {'City': 'Gijón', 'Viajes en evento': 14, 'Viajes normal': 1.44, 'Proporción': 975.06}, 6: {'City': 'Porto', 'Viajes en evento': 20, 'Viajes normal': 1.07, 'Proporción': 1870.75}, 7: {'City': 'Pontevedra', 'Viajes en evento': 1, 'Viajes normal': 0.41, 'Proporción': 243.2}, 8: {'City': 'Ribadeo', 'Viajes en evento': 4, 'Viajes normal': 0.98, 'Proporción': 408.96}, 9: {'City': 'Oviedo', 'Viajes en evento': 6, 'Viajes normal': 1.17, 'Proporción': 512.1}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_o son do camiño_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_o son do camiño_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MARTIN GARRIX', 'MORGAN', 'THE KILLERS', 'DON DIABLO', 'LOST FREQUENCIES', 'C TANGANA', 'RESIDENTE', 'TWO DOOR CINEMA CLUB', 'LENNY KRAVITZ', 'JAMIROQUAI', 'FRANZ FERDINAND', 'CARLOS SADNESS', 'LA M.O.D.A', 'MANDO DIAO', 'ARCE', 'NOVEDADES CARMINHA', 'LEON BENAVENTE', 'BALA', 'TALISCO', 'L.A.', 'TRIANGULO DE AMOR BIZARRO', 'REPUBLICA', 'RUFUS T.FIREFLY', 'THE LAST INTERNATIONALE', 'TOUNDRA', 'SEXY ZEBRAS', 'THE GIFT', 'ELADIO Y LOS SERES QUERIDOS', 'MALANDROMEDA', 'AGORAPHOBIA', 'SONIDOS MANS'],
	 	 "artistas_evento_2019":['DAVID GUETTA', 'BLACK EYED PEAS', 'BASTILLE', 'ROSALIA', 'DIMITRI VEGAS', 'BERET', 'BAD GYAL', 'IGGY POP', 'VETUSTA MORLA', 'DIE ANTWOORD', 'AYAX Y PROK', 'BLOC PARTY', 'SECOND', 'THE HIVES', 'IVAN FERREIRO', 'ROYAL REPUBLIC', 'RICHARD ASHCROFT', 'VARRY BRAVA', 'FULL', 'GRAVEYARD', 'SHINOVA', 'CARIÑO', 'ELYELLA', 'MAREM LADSON', 'THE ZOMBIE KIDS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_o son do camiño_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_o son do camiño_2019.jpg",
	 }, 
	 { 
	 	 "id": "34",
	 	 "nombre": "PINTOR ROCK",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "el Morell",
	 	 "fecha_ini_2018": "01-11-2018",
	 	 "fecha_fin_2018": "03-11-2018",
	 	 "fecha_ini_2019": "31-10-2019",
	 	 "fecha_fin_2019": "02-11-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 3.6666666666666665,
	 	 "D002_evento_media_viajes_2018": 0.47,
	 	 "D003_evento_media_viajes_dia_semana_2018": 2.67,
	 	 "D004_evento_viajes_2019": 6.0,
	 	 "D005_evento_media_viajes_2019": 0.87,
	 	 "D006_evento_media_viajes_dia_semana_2019": 3.67,

	 	 "D007_proporcion_asientos_evento_2018": 38.3,
	 	 "D008_proporcion_asientos_media_evento_2018": 5.57,
	 	 "D009_proporcion_asientos_evento_2019": 32.31,
	 	 "D010_proporcion_asientos_media_evento_2019": 7.36,

	 	 "D011_ocupacion_asientos_evento_2018": 1.12,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.44,
	 	 "D013_ocupacion_asientos_evento_2019": 1.31,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.69,

	 	 "D015_precio_evento_2018": 0.0604,
	 	 "D016_precio_medio_evento_2018": 0.0632,
	 	 "D017_precio_evento_2019": 0.0559,
	 	 "D018_precio_medio_evento_2019": 0.0513,

	 	 "D019_novatos_evento_2018": 0.67,
	 	 "D020_novatos_media_evento_2018": 0.97,
	 	 "D021_novatos_evento_2019": 3.0,
	 	 "D022_novatos_media_evento_2019": 1.27,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-pintor rock-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Zaragoza', 'Viajes en evento': 3, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Barcelona', 'Viajes en evento': 4, 'Viajes normal': 0.67, 'Proporción': 600.0}, 2: {'City': 'Bilbao', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.25, 'Proporción': 800.0}, 4: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.5, 'Proporción': 200.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-pintor rock-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Barcelona', 'Viajes en evento': 6, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 2: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 3: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Calella', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 5: {'City': 'Manresa', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-pintor rock-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-pintor rock-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_pintor rock_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_pintor rock_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['LA M.O.D.A', 'DESAKATO', 'REINCIDENTES', 'BERRI TXARRAK', 'CRIM', 'TALCO', 'KOP', 'NON SERVIUM', 'GATIBU', 'SONS OF AGUIRRE', 'GATILLAZO', 'SOZIEDAD ALKOHOLIKA', 'DEF CON DOS', 'THE BABOON SHOW', 'XAVI SARRIA', 'MANOLO KABEZABOLO', 'MALOS VICIOS', 'EL NOI DEL SUCRE', 'NARCO', 'NO KONFORME', 'RAT-ZINGER', 'RADIOCRIMEN', 'VENDETTA', 'MISS OCTUBRE', 'EUKZ', 'MILENRAMA', 'BOURBON KINGS'],
	 	 "artistas_evento_2019":['KOMA', 'SKA-P', 'IRA', 'LAGRIMAS DE SANGRE', 'DESAKATO', 'CRIM', 'EL ULTIMO KE ZIERRE', 'KOP', 'KAOTIKO', 'GATIBU', 'SOZIEDAD ALKOHOLIKA', 'SEGISMUNDO TOXICOMANO', 'GRITANDO EN SILENCIO', 'THE BABOON SHOW', 'DEF CON DOS', 'KONSUMO RESPETO', 'EBRI KNIGHT', 'ARPAVIEJAS', 'PARABELLUM', 'NARCO', 'JUANTXO SKALARI', 'MAFALDA', 'LA DESBANDADA', 'ANIMALES MUERTOS', 'THE GUILTY BRIGADE', 'DISTORSION', 'JOSETXU PIPERRAK', 'DSKARRILA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_pintor rock_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_pintor rock_2019.jpg",
	 }, 
	 { 
	 	 "id": "35",
	 	 "nombre": "PORTAMÉRICA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Caldas de Reis",
	 	 "fecha_ini_2018": "05-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "04-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 3.3333333333333335,
	 	 "D002_evento_media_viajes_2018": 0.21,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.25,
	 	 "D004_evento_viajes_2019": 3.33,
	 	 "D005_evento_media_viajes_2019": 0.45,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.33,

	 	 "D007_proporcion_asientos_evento_2018": 50.0,
	 	 "D008_proporcion_asientos_media_evento_2018": 1.95,
	 	 "D009_proporcion_asientos_evento_2019": 33.33,
	 	 "D010_proporcion_asientos_media_evento_2019": 4.52,

	 	 "D011_ocupacion_asientos_evento_2018": 0.31,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.2,
	 	 "D013_ocupacion_asientos_evento_2019": 0.29,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.18,

	 	 "D015_precio_evento_2018": 0.0633,
	 	 "D016_precio_medio_evento_2018": 0.0617,
	 	 "D017_precio_evento_2019": 0.0605,
	 	 "D018_precio_medio_evento_2019": 0.0601,

	 	 "D019_novatos_evento_2018": 0.67,
	 	 "D020_novatos_media_evento_2018": 0.71,
	 	 "D021_novatos_evento_2019": 0.0,
	 	 "D022_novatos_media_evento_2019": 0.36,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-portamérica-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 4, 'Viajes normal': 0.16, 'Proporción': 2500.0}, 1: {'City': 'Ourense', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 2133.33}, 2: {'City': 'Gijón', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Vigo', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4800.0}, 4: {'City': 'Carreño', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 5: {'City': 'Ferrol', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-portamérica-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'A Coruña', 'Viajes en evento': 4, 'Viajes normal': 0.12, 'Proporción': 3200.0}, 1: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.16, 'Proporción': 1875.0}, 2: {'City': 'Ourense', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1400.0}, 3: {'City': 'Culleredo', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 4: {'City': 'Lugo', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 800.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-portamérica-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-portamérica-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_portamérica_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_portamérica_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MIRANDA', 'LOS AUTENTICOS DECADENTES', 'LOS CALIGARIS', 'VETUSTA MORLA', 'IZAL', 'LA PEGATINA', 'WAS', 'PARDO', 'JENNY AND THE MEXICATS', 'LA HABITACION ROJA', 'XOEL LOPEZ', 'VIVA SUECIA', 'IMELDA MAY', 'LA VIDA BOHEME', 'SHINOVA', 'NEUMAN', 'EL COLUMPIO ASESINO', 'ELYELLA', 'PUSSY RIOT', 'LOS CORONAS', 'MEXRRISSEY', 'JACOBO SERRA', 'LUIS BREA Y EL MIEDO'],
	 	 "artistas_evento_2019":['MORGAN', 'MON LAFERTE', 'ANDRES CALAMARO', 'ROZALEN', 'CARLOS SADNESS', 'MADNESS', 'ZAHARA', 'AMAIA', 'LA CASA AZUL', 'MONSIEUR PERINE', 'SIDECARS', 'EL MATO A UN POLICIA MOTORIZADO', 'DEPEDRO', 'CAROLINA DURANTE', 'LOS ESPIRITUS', 'ELEFANTES', 'DE LA PURISIMA', 'TRENDING TOPIS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_portamérica_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_portamérica_2019.jpg",
	 }, 
	 { 
	 	 "id": "36",
	 	 "nombre": "PRIMAVERA SOUND",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Barcelona",
	 	 "fecha_ini_2018": "30-05-2018",
	 	 "fecha_fin_2018": "03-06-2018",
	 	 "fecha_ini_2019": "30-05-2019",
	 	 "fecha_fin_2019": "01-06-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 164.0,
	 	 "D002_evento_media_viajes_2018": 190.07,
	 	 "D003_evento_media_viajes_dia_semana_2018": 142.37,
	 	 "D004_evento_viajes_2019": 180.0,
	 	 "D005_evento_media_viajes_2019": 183.12,
	 	 "D006_evento_media_viajes_dia_semana_2019": 148.47,

	 	 "D007_proporcion_asientos_evento_2018": 35.62,
	 	 "D008_proporcion_asientos_media_evento_2018": 23.71,
	 	 "D009_proporcion_asientos_evento_2019": 35.62,
	 	 "D010_proporcion_asientos_media_evento_2019": 22.78,

	 	 "D011_ocupacion_asientos_evento_2018": 0.03,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.86,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.77,

	 	 "D015_precio_evento_2018": 0.0567,
	 	 "D016_precio_medio_evento_2018": 0.058,
	 	 "D017_precio_evento_2019": 0.0624,
	 	 "D018_precio_medio_evento_2019": 0.0603,

	 	 "D019_novatos_evento_2018": 25.67,
	 	 "D020_novatos_media_evento_2018": 91.49,
	 	 "D021_novatos_evento_2019": 54.25,
	 	 "D022_novatos_media_evento_2019": 77.17,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-primavera sound-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Lleida', 'Viajes en evento': 154, 'Viajes normal': 26.22, 'Proporción': 587.42}, 1: {'City': 'Valencia', 'Viajes en evento': 116, 'Viajes normal': 23.34, 'Proporción': 497.07}, 2: {'City': 'Madrid', 'Viajes en evento': 135, 'Viajes normal': 22.06, 'Proporción': 611.88}, 3: {'City': 'Perpignull', 'Viajes en evento': 59, 'Viajes normal': 7.61, 'Proporción': 774.92}, 4: {'City': 'Montpellier', 'Viajes en evento': 39, 'Viajes normal': 8.23, 'Proporción': 473.87}, 5: {'City': 'Zaragoza', 'Viajes en evento': 23, 'Viajes normal': 4.96, 'Proporción': 463.81}, 6: {'City': 'Toulouse', 'Viajes en evento': 63, 'Viajes normal': 7.57, 'Proporción': 831.73}, 7: {'City': 'Tarragona', 'Viajes en evento': 9, 'Viajes normal': 1.74, 'Proporción': 516.51}, 8: {'City': 'Castellón de la Plana ', 'Viajes en evento': 19, 'Viajes normal': 3.38, 'Proporción': 561.36}, 9: {'City': 'Girona', 'Viajes en evento': 13, 'Viajes normal': 2.7, 'Proporción': 481.73}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-primavera sound-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Perpignull', 'Viajes en evento': 40, 'Viajes normal': 5.66, 'Proporción': 706.57}, 1: {'City': 'Madrid', 'Viajes en evento': 113, 'Viajes normal': 19.59, 'Proporción': 576.86}, 2: {'City': 'Lleida', 'Viajes en evento': 90, 'Viajes normal': 25.85, 'Proporción': 348.22}, 3: {'City': 'Montpellier', 'Viajes en evento': 44, 'Viajes normal': 6.05, 'Proporción': 726.96}, 4: {'City': 'Valencia', 'Viajes en evento': 61, 'Viajes normal': 20.96, 'Proporción': 291.02}, 5: {'City': 'Zaragoza', 'Viajes en evento': 15, 'Viajes normal': 4.52, 'Proporción': 331.64}, 6: {'City': 'Girona', 'Viajes en evento': 17, 'Viajes normal': 3.36, 'Proporción': 506.17}, 7: {'City': 'Toulouse', 'Viajes en evento': 43, 'Viajes normal': 5.95, 'Proporción': 722.63}, 8: {'City': 'Narbonne', 'Viajes en evento': 7, 'Viajes normal': 1.43, 'Proporción': 490.0}, 9: {'City': 'Béziers', 'Viajes en evento': 7, 'Viajes normal': 1.03, 'Proporción': 681.64}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-primavera sound-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 131, 'Viajes normal': 23.98, 'Proporción': 546.39}, 1: {'City': 'Lleida', 'Viajes en evento': 167, 'Viajes normal': 26.91, 'Proporción': 620.66}, 2: {'City': 'Madrid', 'Viajes en evento': 107, 'Viajes normal': 23.15, 'Proporción': 462.13}, 3: {'City': 'Perpignull', 'Viajes en evento': 38, 'Viajes normal': 8.71, 'Proporción': 436.16}, 4: {'City': 'Montpellier', 'Viajes en evento': 35, 'Viajes normal': 8.95, 'Proporción': 391.27}, 5: {'City': 'Zaragoza', 'Viajes en evento': 19, 'Viajes normal': 4.68, 'Proporción': 406.27}, 6: {'City': 'Tarragona', 'Viajes en evento': 13, 'Viajes normal': 1.23, 'Proporción': 1054.44}, 7: {'City': 'Castellón de la Plana ', 'Viajes en evento': 24, 'Viajes normal': 3.15, 'Proporción': 763.07}, 8: {'City': 'Toulouse', 'Viajes en evento': 39, 'Viajes normal': 8.45, 'Proporción': 461.36}, 9: {'City': 'Girona', 'Viajes en evento': 18, 'Viajes normal': 2.21, 'Proporción': 814.13}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-primavera sound-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Lleida', 'Viajes en evento': 131, 'Viajes normal': 26.5, 'Proporción': 494.4}, 1: {'City': 'Valencia', 'Viajes en evento': 83, 'Viajes normal': 21.85, 'Proporción': 379.83}, 2: {'City': 'Madrid', 'Viajes en evento': 81, 'Viajes normal': 21.42, 'Proporción': 378.07}, 3: {'City': 'Zaragoza', 'Viajes en evento': 14, 'Viajes normal': 4.56, 'Proporción': 307.29}, 4: {'City': 'Tarragona', 'Viajes en evento': 8, 'Viajes normal': 1.2, 'Proporción': 668.13}, 5: {'City': 'Girona', 'Viajes en evento': 18, 'Viajes normal': 2.82, 'Proporción': 637.76}, 6: {'City': 'Perpignull', 'Viajes en evento': 21, 'Viajes normal': 6.64, 'Proporción': 316.2}, 7: {'City': 'Montpellier', 'Viajes en evento': 21, 'Viajes normal': 5.9, 'Proporción': 355.85}, 8: {'City': 'Tàrrega', 'Viajes en evento': 20, 'Viajes normal': 2.97, 'Proporción': 673.31}, 9: {'City': 'Narbonne', 'Viajes en evento': 7, 'Viajes normal': 1.69, 'Proporción': 415.07}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_primavera sound_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_primavera sound_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ARCA', 'TYLER, THE CREATOR', 'ASAP ROCKY', 'MIGOS', 'ARTIC MONKEYS', 'LORDE', 'CIGARETTES AFTER SEX', 'VINCE STAPLES', 'CHVRCHES', 'THE NATIONAL', 'LYKKE LI', 'HAIM', 'MAJIN JORDAN', 'FOUR TET', 'BEACH HOUSE', 'NICK CAVE AND THE BAD SEEDS', 'NILS FRAHM', 'THE BLAZE', 'FATHER JON MISTRY', 'THE WAR ON DRUGS', 'BELLE AND SEBASTIAN', 'GRIZZLY BEAR', 'SLOWDIVE', 'DEERHUNTER', 'FLOATING POINTS', 'JAVIERA MENA', 'FEVER RAY', 'THE BREEDERS', 'SPIRITUALIZED', 'CHARLOTTE GAINSBOURG', 'JANE BIRKIN', 'WOLF PARADE', 'MARINA ARNAL', 'DJORK'],
	 	 "artistas_evento_2019":['J BALVIN', 'FUTURE', 'TAME IMPALA', 'MILEY CYRUS', 'ROSALIA', 'KALI UCHIS', 'NAS', 'MAC DEMARCO', 'CUCO', 'CARLY RAE JEPSEN', 'PUSHA T', 'JAMES BLAKE', 'MURA MASA', 'ERYKAH BADU', 'INTERPOL', 'CHRISTINE AND THE QUEENS', 'ROBYN', 'SONAGE', 'FKA TWIGS', 'IVY QUEEN', 'PRIMAL CREAM', 'COURTNEY BARNETT', 'DEERHUNTER', 'CUPIDO', 'ROISIN MURPHY', 'STEREOLAB', 'LIZ PHAIR', 'KATE TEMPEST', 'MODESELEKTOR', 'JARVIS COCKER', 'BIG RED MACHINE', 'JAWBREAKER', 'RICHIE HAWTIN', 'SUEDE', 'JANELLE NOMAE', 'APPARAT DJ'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_primavera sound_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_primavera sound_2019.jpg",
	 }, 
	 { 
	 	 "id": "37",
	 	 "nombre": "RABOLAGARTIJA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Villena",
	 	 "fecha_ini_2018": "16-08-2018",
	 	 "fecha_fin_2018": "18-08-2018",
	 	 "fecha_ini_2019": "15-08-2019",
	 	 "fecha_fin_2019": "17-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 27.0,
	 	 "D002_evento_media_viajes_2018": 4.78,
	 	 "D003_evento_media_viajes_dia_semana_2018": 5.06,
	 	 "D004_evento_viajes_2019": 31.25,
	 	 "D005_evento_media_viajes_2019": 6.01,
	 	 "D006_evento_media_viajes_dia_semana_2019": 6.51,

	 	 "D007_proporcion_asientos_evento_2018": 36.49,
	 	 "D008_proporcion_asientos_media_evento_2018": 3.4,
	 	 "D009_proporcion_asientos_evento_2019": 28.81,
	 	 "D010_proporcion_asientos_media_evento_2019": 4.6,

	 	 "D011_ocupacion_asientos_evento_2018": 0.1,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.31,
	 	 "D013_ocupacion_asientos_evento_2019": 0.11,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.3,

	 	 "D015_precio_evento_2018": 0.0573,
	 	 "D016_precio_medio_evento_2018": 0.0566,
	 	 "D017_precio_evento_2019": 0.0609,
	 	 "D018_precio_medio_evento_2019": 0.0555,

	 	 "D019_novatos_evento_2018": 8.75,
	 	 "D020_novatos_media_evento_2018": 9.55,
	 	 "D021_novatos_evento_2019": 8.0,
	 	 "D022_novatos_media_evento_2019": 6.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-rabolagartija-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 37, 'Viajes normal': 2.17, 'Proporción': 1706.61}, 1: {'City': 'Valencia', 'Viajes en evento': 25, 'Viajes normal': 0.93, 'Proporción': 2692.31}, 2: {'City': 'Albacete', 'Viajes en evento': 4, 'Viajes normal': 0.29, 'Proporción': 1373.58}, 3: {'City': 'Murcia', 'Viajes en evento': 9, 'Viajes normal': 0.11, 'Proporción': 8448.39}, 4: {'City': 'Barcelona', 'Viajes en evento': 8, 'Viajes normal': 0.29, 'Proporción': 2800.0}, 5: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1400.0}, 6: {'City': 'La Roda', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 15400.0}, 7: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.16, 'Proporción': 1242.86}, 8: {'City': 'Benicasim', 'Viajes en evento': 2, 'Viajes normal': 0.11, 'Proporción': 1800.0}, 9: {'City': 'Cartagena', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2500.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-rabolagartija-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 37, 'Viajes normal': 2.79, 'Proporción': 1325.18}, 1: {'City': 'Albacete', 'Viajes en evento': 7, 'Viajes normal': 0.32, 'Proporción': 2186.6}, 2: {'City': 'Valencia', 'Viajes en evento': 24, 'Viajes normal': 1.2, 'Proporción': 2002.21}, 3: {'City': 'Murcia', 'Viajes en evento': 10, 'Viajes normal': 0.11, 'Proporción': 9120.0}, 4: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.04, 'Proporción': 7600.0}, 5: {'City': 'Granada', 'Viajes en evento': 3, 'Viajes normal': 0.19, 'Proporción': 1600.0}, 6: {'City': 'Zaragoza', 'Viajes en evento': 3, 'Viajes normal': 0.35, 'Proporción': 862.5}, 7: {'City': 'Villarrobledo', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4600.0}, 8: {'City': 'Torrevieja', 'Viajes en evento': 2, 'Viajes normal': 0.06, 'Proporción': 3133.33}, 9: {'City': 'Tomelloso', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 8300.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-rabolagartija-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 5, 'Viajes normal': 2.31, 'Proporción': 216.59}, 1: {'City': 'Valencia', 'Viajes en evento': 5, 'Viajes normal': 1.08, 'Proporción': 463.2}, 2: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.14, 'Proporción': 728.0}, 3: {'City': 'Benidorm', 'Viajes en evento': 2, 'Viajes normal': 0.04, 'Proporción': 4644.44}, 4: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.16, 'Proporción': 1818.75}, 5: {'City': 'Albacete', 'Viajes en evento': 1, 'Viajes normal': 0.38, 'Proporción': 265.69}, 6: {'City': 'Cerdanyola del Vallès', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Picanya', 'Viajes en evento': 1, 'Viajes normal': 0.2, 'Proporción': 500.0}, 8: {'City': 'Rivas-Vaciamadrid', 'Viajes en evento': 1, 'Viajes normal': 0.13, 'Proporción': 766.67}, 9: {'City': 'Sevilla', 'Viajes en evento': 1, 'Viajes normal': 0.14, 'Proporción': 740.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-rabolagartija-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 11, 'Viajes normal': 2.97, 'Proporción': 370.75}, 1: {'City': 'Valencia', 'Viajes en evento': 5, 'Viajes normal': 1.52, 'Proporción': 329.72}, 2: {'City': 'Alcoi', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2330.0}, 3: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.18, 'Proporción': 567.5}, 4: {'City': 'Torrevieja', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 3033.33}, 5: {'City': 'Castelldefels', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 6: {'City': 'Getafe', 'Viajes en evento': 1, 'Viajes normal': 0.16, 'Proporción': 618.18}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_rabolagartija_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_rabolagartija_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BEBE', 'NACH', 'ROZALEN', 'KASE.O', 'LA PEGATINA', 'LA M.O.D.A', 'EL KANKA', 'LA RAIZ', 'ZOO', 'LAGRIMAS DE SANGRE', 'THE SKATALITES', 'SKINDRED', 'DESAKATO', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'LOS DE MARRAS', 'EL CANIJO DE JEREZ', 'JULIAN MARLEY', 'ESKORZO', 'RIOT PROPAGANDA', 'MARTIRES DEL COMPAS', 'MACHETE EN BOCA', 'LA REGADERA', 'CHE SUDAKA', "O FUNK'ILLO", 'EL LANGUI'],
	 	 "artistas_evento_2019":['MORAT', 'NATOS Y WAOR', 'ROZALEN', 'LOS CALIGARIS', 'SFDK', 'IRATXO', 'LA PEGATINA', 'LA M.O.D.A', 'GREEN VALLEY', 'JUANITO MAKANDE', 'ZOO', 'EL NIÑO DE LA HIPOTECA', 'BOIKOT', 'ISEO & DODOSOUND', 'LOS DE MARRAS', 'BUHOS', 'TALCO', 'GOGOL BORDELLO', 'ITACA BAND', 'DUBIOZA KOLEKTIV', 'BENITO KAMELAS', 'TREMENDA JAURIA', 'BARCO', 'VALIRA', 'EBRI KNIGHT', 'LA EXCEPCION', 'MAFALDA', 'CHIKI LORA', 'JAMONES CON TACONES', 'AUXILI', 'COMBO CALADA', 'EL SOMBRERO DEL ABUELO', 'EL KAMION DE LA BASURA', 'HURACAN ROMANTICA', 'KANELA EN RAMA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_rabolagartija_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_rabolagartija_2019.jpg",
	 }, 
	 { 
	 	 "id": "38",
	 	 "nombre": "RESURRECTION FEST",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Viveiro",
	 	 "fecha_ini_2018": "11-07-2018",
	 	 "fecha_fin_2018": "14-07-2018",
	 	 "fecha_ini_2019": "03-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 34.8,
	 	 "D002_evento_media_viajes_2018": 2.54,
	 	 "D003_evento_media_viajes_dia_semana_2018": 2.46,
	 	 "D004_evento_viajes_2019": 55.6,
	 	 "D005_evento_media_viajes_2019": 3.74,
	 	 "D006_evento_media_viajes_dia_semana_2019": 2.57,

	 	 "D007_proporcion_asientos_evento_2018": 52.04,
	 	 "D008_proporcion_asientos_media_evento_2018": 14.43,
	 	 "D009_proporcion_asientos_evento_2019": 51.37,
	 	 "D010_proporcion_asientos_media_evento_2019": 19.24,

	 	 "D011_ocupacion_asientos_evento_2018": 0.35,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.62,
	 	 "D013_ocupacion_asientos_evento_2019": 0.55,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.67,

	 	 "D015_precio_evento_2018": 0.0605,
	 	 "D016_precio_medio_evento_2018": 0.0597,
	 	 "D017_precio_evento_2019": 0.0608,
	 	 "D018_precio_medio_evento_2019": 0.0616,

	 	 "D019_novatos_evento_2018": 10.2,
	 	 "D020_novatos_media_evento_2018": 1.31,
	 	 "D021_novatos_evento_2019": 8.2,
	 	 "D022_novatos_media_evento_2019": 1.05,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-resurrection fest-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 29, 'Viajes normal': 0.71, 'Proporción': 4113.27}, 1: {'City': 'Santiago de Compostela', 'Viajes en evento': 23, 'Viajes normal': 0.77, 'Proporción': 2980.61}, 2: {'City': 'Lugo', 'Viajes en evento': 16, 'Viajes normal': 0.58, 'Proporción': 2748.72}, 3: {'City': 'A Coruña', 'Viajes en evento': 25, 'Viajes normal': 0.96, 'Proporción': 2612.11}, 4: {'City': 'Vigo', 'Viajes en evento': 12, 'Viajes normal': 0.33, 'Proporción': 3675.0}, 5: {'City': 'León', 'Viajes en evento': 6, 'Viajes normal': 0.03, 'Proporción': 19800.0}, 6: {'City': 'Pontevedra', 'Viajes en evento': 3, 'Viajes normal': 0.11, 'Proporción': 2614.29}, 7: {'City': 'Bilbao', 'Viajes en evento': 6, 'Viajes normal': 0.14, 'Proporción': 4440.0}, 8: {'City': 'Ourense', 'Viajes en evento': 3, 'Viajes normal': 0.13, 'Proporción': 2271.43}, 9: {'City': 'Ponferrada', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2500.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-resurrection fest-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 56, 'Viajes normal': 0.85, 'Proporción': 6573.91}, 1: {'City': 'Lugo', 'Viajes en evento': 26, 'Viajes normal': 0.87, 'Proporción': 2977.25}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 32, 'Viajes normal': 1.03, 'Proporción': 3103.52}, 3: {'City': 'A Coruña', 'Viajes en evento': 28, 'Viajes normal': 1.22, 'Proporción': 2291.89}, 4: {'City': 'Gijón', 'Viajes en evento': 22, 'Viajes normal': 0.36, 'Proporción': 6128.57}, 5: {'City': 'Vigo', 'Viajes en evento': 15, 'Viajes normal': 0.35, 'Proporción': 4269.23}, 6: {'City': 'Bilbao', 'Viajes en evento': 8, 'Viajes normal': 0.12, 'Proporción': 6720.0}, 7: {'City': 'Avilés', 'Viajes en evento': 5, 'Viajes normal': 0.11, 'Proporción': 4400.0}, 8: {'City': 'Ponferrada', 'Viajes en evento': 2, 'Viajes normal': 0.01, 'Proporción': 17000.0}, 9: {'City': 'Pontevedra', 'Viajes en evento': 3, 'Viajes normal': 0.1, 'Proporción': 2940.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-resurrection fest-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Santiago de Compostela', 'Viajes en evento': 3, 'Viajes normal': 0.99, 'Proporción': 303.8}, 1: {'City': 'A Coruña', 'Viajes en evento': 1, 'Viajes normal': 1.07, 'Proporción': 93.2}, 2: {'City': 'Madrid', 'Viajes en evento': 2, 'Viajes normal': 0.65, 'Proporción': 308.24}, 3: {'City': 'Lugo', 'Viajes en evento': 2, 'Viajes normal': 0.58, 'Proporción': 345.26}, 4: {'City': 'Pontevedra', 'Viajes en evento': 1, 'Viajes normal': 0.16, 'Proporción': 633.33}, 5: {'City': 'Getxo', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-resurrection fest-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'A Coruña', 'Viajes en evento': 5, 'Viajes normal': 1.32, 'Proporción': 377.74}, 1: {'City': 'Lugo', 'Viajes en evento': 4, 'Viajes normal': 0.84, 'Proporción': 475.95}, 2: {'City': 'Santiago de Compostela', 'Viajes en evento': 5, 'Viajes normal': 1.15, 'Proporción': 436.65}, 3: {'City': 'Vigo', 'Viajes en evento': 3, 'Viajes normal': 0.52, 'Proporción': 578.57}, 4: {'City': 'Gijón', 'Viajes en evento': 2, 'Viajes normal': 0.36, 'Proporción': 551.72}, 5: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 1.12, 'Proporción': 268.53}, 6: {'City': 'Bilbao', 'Viajes en evento': 1, 'Viajes normal': 0.22, 'Proporción': 455.56}, 7: {'City': 'Avilés', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 528.57}, 8: {'City': 'Collado Villalba', 'Viajes en evento': 1, 'Viajes normal': 0.57, 'Proporción': 175.0}, 9: {'City': 'Ourense', 'Viajes en evento': 1, 'Viajes normal': 0.35, 'Proporción': 287.5}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_resurrection fest_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_resurrection fest_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['SCORPIONS', 'KISS', 'MEGADETH', 'STONESOUR', 'ALESTORM', 'ANTIFLAG', 'PARADISE LOST', 'PROPHETS OF RAGE', 'AT THE GATES', 'SHOS', 'CROWBAR', 'RIOT PROPAGANDA', 'OVER KILL', 'ANGELUS APARTRIDA'],
	 	 "artistas_evento_2019":['SLIPKNOT', 'TRIVIUM', 'SLAYER', 'LAMB OF GOD', 'WITHIN TEMPTATION', 'GOJIRA', 'TESTAMENT', 'ARCH ENEMY', 'AVATAR', 'KING DIAMOND', 'CRYSTAL LAKE', 'ALIEN WEAPONRY', 'BRANT BJORK', 'COLOUR HAZE', 'TOUNDRA', 'CRISIX', 'PARKWAY DRIFT', 'WHERE SHE SLEEPS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_resurrection fest_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_resurrection fest_2019.jpg",
	 }, 
	 { 
	 	 "id": "39",
	 	 "nombre": "RMF SOMNII",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Figueira da Foz",
	 	 "fecha_ini_2018": "06-07-2018",
	 	 "fecha_fin_2018": "08-07-2018",
	 	 "fecha_ini_2019": "05-07-2019",
	 	 "fecha_fin_2019": "07-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 5.0,
	 	 "D002_evento_media_viajes_2018": 0.21,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.2,
	 	 "D004_evento_viajes_2019": 3.33,
	 	 "D005_evento_media_viajes_2019": 0.14,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.12,

	 	 "D007_proporcion_asientos_evento_2018": 26.39,
	 	 "D008_proporcion_asientos_media_evento_2018": 2.64,
	 	 "D009_proporcion_asientos_evento_2019": 35.56,
	 	 "D010_proporcion_asientos_media_evento_2019": 1.4,

	 	 "D011_ocupacion_asientos_evento_2018": 0.38,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.56,
	 	 "D013_ocupacion_asientos_evento_2019": 0.32,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.36,

	 	 "D015_precio_evento_2018": 0.0573,
	 	 "D016_precio_medio_evento_2018": 0.0547,
	 	 "D017_precio_evento_2019": 0.0557,
	 	 "D018_precio_medio_evento_2019": 0.0566,

	 	 "D019_novatos_evento_2018": 2.0,
	 	 "D020_novatos_media_evento_2018": 1.23,
	 	 "D021_novatos_evento_2019": 0.33,
	 	 "D022_novatos_media_evento_2019": 0.59,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-rmf somnii-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Lisboa', 'Viajes en evento': 4, 'Viajes normal': 0.09, 'Proporción': 4685.71}, 1: {'City': 'Porto', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 1911.11}, 2: {'City': 'Badajoz', 'Viajes en evento': 2, 'Viajes normal': 0.25, 'Proporción': 800.0}, 3: {'City': 'Pontevedra', 'Viajes en evento': 2, 'Viajes normal': 0.5, 'Proporción': 400.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-rmf somnii-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Lisboa', 'Viajes en evento': 5, 'Viajes normal': 0.06, 'Proporción': 8375.0}, 1: {'City': 'Madrid', 'Viajes en evento': 2, 'Viajes normal': 0.22, 'Proporción': 900.0}, 2: {'City': 'A Coruña', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Porto', 'Viajes en evento': 1, 'Viajes normal': 0.08, 'Proporción': 1183.33}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-rmf somnii-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Lisboa', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 822.22}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-rmf somnii-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Lisboa', 'Viajes en evento': 2, 'Viajes normal': 0.08, 'Proporción': 2400.0}, 1: {'City': 'Santa Maria da Feira', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_rmf somnii_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_rmf somnii_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ALAN WALKER', 'BROOKS', 'AXWELL A INGROSSO', 'YELLOW CLAW', 'QUINTINO', 'MOSKI', 'SLUSHII', 'STEVE ANGELLO', 'ANGERFIRST', 'CURBI', 'CHOCOLATE PUMA', 'GARMIANI', 'GREGOR SALTO', 'GESQEAUX'],
	 	 "artistas_evento_2019":['OZUNA', 'TYGA', 'DJ SNAKE', 'JONAS BLUE', 'ALESSO', 'AFROJACK', 'DON DIABLO', 'FEDDE LE GRAND', 'JAMES HYPE', 'BLASTERJAXX', 'NETSKY', 'REDFOO', 'THIRD PARTY', 'RADICAL REDEMPTION'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_rmf somnii_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_rmf somnii_2019.jpg",
	 }, 
	 { 
	 	 "id": "40",
	 	 "nombre": "ROCK FEST",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Santa Coloma",
	 	 "fecha_ini_2018": "05-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "04-07-2019",
	 	 "fecha_fin_2019": "07-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": null,
	 	 "D002_evento_media_viajes_2018": 0.0,
	 	 "D003_evento_media_viajes_dia_semana_2018": null,
	 	 "D004_evento_viajes_2019": null,
	 	 "D005_evento_media_viajes_2019": 0.0,
	 	 "D006_evento_media_viajes_dia_semana_2019": null,

	 	 "D007_proporcion_asientos_evento_2018": null,
	 	 "D008_proporcion_asientos_media_evento_2018": 0.0,
	 	 "D009_proporcion_asientos_evento_2019": null,
	 	 "D010_proporcion_asientos_media_evento_2019": 0.0,

	 	 "D011_ocupacion_asientos_evento_2018": null,
	 	 "D012_ocupacion_asientos_media_evento_2018": null,
	 	 "D013_ocupacion_asientos_evento_2019": null,
	 	 "D014_ocupacion_asientos_media_evento_2019": null,

	 	 "D015_precio_evento_2018": null,
	 	 "D016_precio_medio_evento_2018": null,
	 	 "D017_precio_evento_2019": null,
	 	 "D018_precio_medio_evento_2019": null,

	 	 "D019_novatos_evento_2018": null,
	 	 "D020_novatos_media_evento_2018": 1.0,
	 	 "D021_novatos_evento_2019": null,
	 	 "D022_novatos_media_evento_2019": 0.0,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-rock fest-2018.html",
	 	 "tabla_origenes_evento_2018":{},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-rock fest-2019.html",
	 	 "tabla_origenes_evento_2019":{},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-rock fest-2018.html",
	 	 "tabla_destinos_evento_2018":{},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-rock fest-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_rock fest_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_rock fest_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['OZZY OSBOURNE', 'KISS', 'SCORPIONS', 'MEGADETH', 'MAGO DE OZ', 'JUDAS PRIEST', 'HELLOWEEN', 'ACCEPT', 'STRATOVARIUS', 'URIAH HEEP', 'DIMMU BORGIR', 'PORRETAS', 'MOJINOS ESCOZIOS', 'EVERGREY', 'DEEDSNIDER'],
	 	 "artistas_evento_2019":['DEF LEPPARD', 'ZZTOP', 'EUROPE', 'POWERWOLF', 'DREAM THEATER', 'TESTAMENT', 'HAMMERFALL', 'WASP', 'CHILDREN OF BODOM', 'SAXON', 'KING DIAMOND', 'DEMONS WIZARDS', 'MICHAEL SCHENKER'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_rock fest_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_rock fest_2019.jpg",
	 }, 
	 { 
	 	 "id": "41",
	 	 "nombre": "ROTOTOM SUNSPLASH",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Benicasim",
	 	 "fecha_ini_2018": "16-08-2018",
	 	 "fecha_fin_2018": "22-08-2018",
	 	 "fecha_ini_2019": "16-08-2019",
	 	 "fecha_fin_2019": "22-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 56.625,
	 	 "D002_evento_media_viajes_2018": 3.46,
	 	 "D003_evento_media_viajes_dia_semana_2018": 5.62,
	 	 "D004_evento_viajes_2019": 50.75,
	 	 "D005_evento_media_viajes_2019": 4.26,
	 	 "D006_evento_media_viajes_dia_semana_2019": 9.14,

	 	 "D007_proporcion_asientos_evento_2018": 48.47,
	 	 "D008_proporcion_asientos_media_evento_2018": 8.47,
	 	 "D009_proporcion_asientos_evento_2019": 45.22,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.12,

	 	 "D011_ocupacion_asientos_evento_2018": 0.68,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.69,
	 	 "D013_ocupacion_asientos_evento_2019": 0.59,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.62,

	 	 "D015_precio_evento_2018": 0.0609,
	 	 "D016_precio_medio_evento_2018": 0.0573,
	 	 "D017_precio_evento_2019": 0.0649,
	 	 "D018_precio_medio_evento_2019": 0.0587,

	 	 "D019_novatos_evento_2018": 24.88,
	 	 "D020_novatos_media_evento_2018": 5.35,
	 	 "D021_novatos_evento_2019": 17.12,
	 	 "D022_novatos_media_evento_2019": 4.3,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-rototom sunsplash-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 106, 'Viajes normal': 1.54, 'Proporción': 6872.25}, 1: {'City': 'Valencia', 'Viajes en evento': 70, 'Viajes normal': 0.42, 'Proporción': 16550.39}, 2: {'City': 'Barcelona', 'Viajes en evento': 75, 'Viajes normal': 0.77, 'Proporción': 9776.79}, 3: {'City': 'Zaragoza', 'Viajes en evento': 16, 'Viajes normal': 0.54, 'Proporción': 2961.19}, 4: {'City': 'Perpignull', 'Viajes en evento': 7, 'Viajes normal': 0.1, 'Proporción': 7233.33}, 5: {'City': 'Murcia', 'Viajes en evento': 12, 'Viajes normal': 0.36, 'Proporción': 3342.86}, 6: {'City': 'Tarragona', 'Viajes en evento': 3, 'Viajes normal': 0.04, 'Proporción': 7928.57}, 7: {'City': 'Alicante', 'Viajes en evento': 11, 'Viajes normal': 0.36, 'Proporción': 3025.0}, 8: {'City': 'Montpellier', 'Viajes en evento': 6, 'Viajes normal': 0.45, 'Proporción': 1333.33}, 9: {'City': 'Granada', 'Viajes en evento': 7, 'Viajes normal': 0.33, 'Proporción': 2100.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-rototom sunsplash-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 91, 'Viajes normal': 1.73, 'Proporción': 5270.79}, 1: {'City': 'Valencia', 'Viajes en evento': 53, 'Viajes normal': 0.38, 'Proporción': 13823.71}, 2: {'City': 'Barcelona', 'Viajes en evento': 74, 'Viajes normal': 0.8, 'Proporción': 9225.33}, 3: {'City': 'Zaragoza', 'Viajes en evento': 17, 'Viajes normal': 0.41, 'Proporción': 4136.67}, 4: {'City': 'Murcia', 'Viajes en evento': 7, 'Viajes normal': 0.2, 'Proporción': 3422.22}, 5: {'City': 'Perpignull', 'Viajes en evento': 9, 'Viajes normal': 0.21, 'Proporción': 4320.0}, 6: {'City': 'Tarragona', 'Viajes en evento': 5, 'Viajes normal': 0.09, 'Proporción': 5785.71}, 7: {'City': 'Montpellier', 'Viajes en evento': 3, 'Viajes normal': 0.21, 'Proporción': 1425.0}, 8: {'City': 'Alicante', 'Viajes en evento': 7, 'Viajes normal': 0.31, 'Proporción': 2250.0}, 9: {'City': 'Teruel', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 2100.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-rototom sunsplash-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 73, 'Viajes normal': 1.77, 'Proporción': 4128.69}, 1: {'City': 'Valencia', 'Viajes en evento': 29, 'Viajes normal': 0.67, 'Proporción': 4335.05}, 2: {'City': 'Barcelona', 'Viajes en evento': 39, 'Viajes normal': 0.8, 'Proporción': 4869.09}, 3: {'City': 'Zaragoza', 'Viajes en evento': 13, 'Viajes normal': 0.48, 'Proporción': 2691.23}, 4: {'City': 'Murcia', 'Viajes en evento': 6, 'Viajes normal': 0.37, 'Proporción': 1636.36}, 5: {'City': 'Teruel', 'Viajes en evento': 4, 'Viajes normal': 0.01, 'Proporción': 30400.0}, 6: {'City': 'Granada', 'Viajes en evento': 3, 'Viajes normal': 0.34, 'Proporción': 870.0}, 7: {'City': 'Montpellier', 'Viajes en evento': 5, 'Viajes normal': 0.32, 'Proporción': 1562.5}, 8: {'City': 'Perpignull', 'Viajes en evento': 2, 'Viajes normal': 0.34, 'Proporción': 584.62}, 9: {'City': 'Pamplona', 'Viajes en evento': 4, 'Viajes normal': 0.39, 'Proporción': 1018.18}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-rototom sunsplash-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 63, 'Viajes normal': 2.02, 'Proporción': 3123.75}, 1: {'City': 'Valencia', 'Viajes en evento': 25, 'Viajes normal': 0.6, 'Proporción': 4161.18}, 2: {'City': 'Barcelona', 'Viajes en evento': 46, 'Viajes normal': 1.01, 'Proporción': 4548.02}, 3: {'City': 'Zaragoza', 'Viajes en evento': 12, 'Viajes normal': 0.63, 'Proporción': 1897.67}, 4: {'City': 'Tarragona', 'Viajes en evento': 2, 'Viajes normal': 0.13, 'Proporción': 1578.95}, 5: {'City': 'Alicante', 'Viajes en evento': 5, 'Viajes normal': 0.36, 'Proporción': 1375.0}, 6: {'City': 'Reus', 'Viajes en evento': 3, 'Viajes normal': 0.02, 'Proporción': 17550.0}, 7: {'City': 'Perpignull', 'Viajes en evento': 2, 'Viajes normal': 0.38, 'Proporción': 520.0}, 8: {'City': 'Montpellier', 'Viajes en evento': 4, 'Viajes normal': 0.25, 'Proporción': 1600.0}, 9: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.35, 'Proporción': 852.63}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_rototom sunsplash_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_rototom sunsplash_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BEN HARPER', 'JIMMY CLIFF', 'ALBOROSIE', 'PROTOJE', 'ORISHAS', 'TARRUS RILEY', 'MORODO', 'GREEN VALLEY', 'THE SKATALITES', 'SKARRA MUCCI', 'SLY & ROBBIE', 'JULIAN MARLEY', 'COCOA TEA', 'JOHNNY OSBOURNE', 'BITTY MCLEAN', 'MIGHTY DIAMONDS', 'ZION TRAIN', 'DAVID RODIGAN', 'ALPHEUS'],
	 	 "artistas_evento_2019":['BUSY SIGNAL', 'MACACO', 'ZIGGY MARLEY', 'GREEN VALLEY', 'TAIRO', 'MORGAN HERITAGE', 'ISRAEL VIBRATION', 'MARCIA GRIFFITHS', 'ANTHONY B', 'BEDNAREK', 'THIRD WORLD', 'LILA IKE', 'EVA LAZARUS', 'QUEEN IFRICA', 'JO MERSA MARLEY', 'BUSHMAN', 'THE ABYSSINIANS', 'THE SELECTER', 'BRINSLEY FORDE', 'SEVANA', 'MISTY IN ROOTS', 'INTERNATIONAL DUB AMBASSADORS', 'EMETERIANS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_rototom sunsplash_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_rototom sunsplash_2019.jpg",
	 }, 
	 { 
	 	 "id": "42",
	 	 "nombre": "SANSAN",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Benicasim",
	 	 "fecha_ini_2018": "29-03-2018",
	 	 "fecha_fin_2018": "31-03-2018",
	 	 "fecha_ini_2019": "18-04-2019",
	 	 "fecha_fin_2019": "20-04-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 11.25,
	 	 "D002_evento_media_viajes_2018": 3.46,
	 	 "D003_evento_media_viajes_dia_semana_2018": 5.62,
	 	 "D004_evento_viajes_2019": 23.33,
	 	 "D005_evento_media_viajes_2019": 4.26,
	 	 "D006_evento_media_viajes_dia_semana_2019": 5.74,

	 	 "D007_proporcion_asientos_evento_2018": 39.02,
	 	 "D008_proporcion_asientos_media_evento_2018": 8.47,
	 	 "D009_proporcion_asientos_evento_2019": 41.58,
	 	 "D010_proporcion_asientos_media_evento_2019": 9.12,

	 	 "D011_ocupacion_asientos_evento_2018": 0.06,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.69,
	 	 "D013_ocupacion_asientos_evento_2019": 0.1,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.62,

	 	 "D015_precio_evento_2018": 0.0568,
	 	 "D016_precio_medio_evento_2018": 0.0573,
	 	 "D017_precio_evento_2019": 0.0593,
	 	 "D018_precio_medio_evento_2019": 0.0587,

	 	 "D019_novatos_evento_2018": 4.0,
	 	 "D020_novatos_media_evento_2018": 5.35,
	 	 "D021_novatos_evento_2019": 4.67,
	 	 "D022_novatos_media_evento_2019": 4.3,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-sansan-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 24, 'Viajes normal': 1.54, 'Proporción': 1555.98}, 1: {'City': 'Barcelona', 'Viajes en evento': 5, 'Viajes normal': 0.77, 'Proporción': 651.79}, 2: {'City': 'Valencia', 'Viajes en evento': 5, 'Viajes normal': 0.42, 'Proporción': 1182.17}, 3: {'City': 'Zaragoza', 'Viajes en evento': 4, 'Viajes normal': 0.54, 'Proporción': 740.3}, 4: {'City': 'Tarragona', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2642.86}, 5: {'City': 'Valladolid', 'Viajes en evento': 1, 'Viajes normal': 0.16, 'Proporción': 633.33}, 6: {'City': 'Alicante', 'Viajes en evento': 2, 'Viajes normal': 0.36, 'Proporción': 550.0}, 7: {'City': 'Bilbao', 'Viajes en evento': 1, 'Viajes normal': 0.43, 'Proporción': 233.33}, 8: {'City': 'Arganda del Rey', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Fuenlabrada', 'Viajes en evento': 1, 'Viajes normal': 0.29, 'Proporción': 350.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-sansan-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 32, 'Viajes normal': 1.73, 'Proporción': 1853.47}, 1: {'City': 'Valencia', 'Viajes en evento': 13, 'Viajes normal': 0.38, 'Proporción': 3390.72}, 2: {'City': 'Barcelona', 'Viajes en evento': 8, 'Viajes normal': 0.8, 'Proporción': 997.33}, 3: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.41, 'Proporción': 486.67}, 4: {'City': 'San Vicente del Raspeig', 'Viajes en evento': 1, 'Viajes normal': 0.17, 'Proporción': 600.0}, 5: {'City': 'Pamplona', 'Viajes en evento': 2, 'Viajes normal': 0.14, 'Proporción': 1400.0}, 6: {'City': 'Murcia', 'Viajes en evento': 2, 'Viajes normal': 0.2, 'Proporción': 977.78}, 7: {'City': 'Donostia', 'Viajes en evento': 1, 'Viajes normal': 0.18, 'Proporción': 550.0}, 8: {'City': 'Arnedo', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Fuenlabrada', 'Viajes en evento': 1, 'Viajes normal': 0.38, 'Proporción': 266.67}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-sansan-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 5, 'Viajes normal': 1.77, 'Proporción': 282.79}, 1: {'City': 'Alcoi', 'Viajes en evento': 1, 'Viajes normal': 0.09, 'Proporción': 1066.67}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-sansan-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 16, 'Viajes normal': 2.02, 'Proporción': 793.33}, 1: {'City': 'Barcelona', 'Viajes en evento': 2, 'Viajes normal': 1.01, 'Proporción': 197.74}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_sansan_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_sansan_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['ARCO', 'DINERO', 'DESPISTAOS', 'LA M.O.D.A', 'RULO Y LA CONTRABANDA', 'TXARANGO', 'IVAN FERREIRO', 'JUANITO MAKANDE', 'MUERDO', 'LORI MEYERS', 'SIDONIE', 'CELTAS CORTOS', 'VIVA SUECIA', 'BOMBAI', 'VARRY BRAVA', 'FULL', 'THE ROYAL CONCEPT', 'ELEFANTES', 'OJETE CALOR', 'RUFUS T.FIREFLY', 'NUNATAK', 'MERIDIAN', 'SEXY ZEBRAS', 'LAS CHILLERS', 'SPACE ELEPHANTS'],
	 	 "artistas_evento_2019":['EUT', 'MORGAN', 'C TANGANA', 'LOS AUTENTICOS DECADENTES', 'ROZALEN', 'IZAL', 'LOVE OF LESBIAN', 'SECOND', 'LA PEGATINA', 'LA CASA AZUL', 'MISS CAFFEINA', 'XOEL LOPEZ', 'FUEL FANDANGO', 'CAROLINA DURANTE', 'VARRY BRAVA', 'SHINOVA', 'TRIANGULO DE AMOR BIZARRO', 'LOS PUNSETES', 'CALA VENTO', 'ZEA MAYS', 'LA SONRISA DE JULIA', 'LOS VINAGRES', 'LA PLATA', 'THE PARROTS', 'JACOBO SERRA', 'LAS CHILLERS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_sansan_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_sansan_2019.jpg",
	 }, 
	 { 
	 	 "id": "43",
	 	 "nombre": "SHIKILLO",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Candeleda",
	 	 "fecha_ini_2018": "01-08-2018",
	 	 "fecha_fin_2018": "05-08-2018",
	 	 "fecha_ini_2019": "31-07-2019",
	 	 "fecha_fin_2019": "03-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 14.0,
	 	 "D002_evento_media_viajes_2018": 0.41,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.19,
	 	 "D004_evento_viajes_2019": 6.67,
	 	 "D005_evento_media_viajes_2019": 0.27,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.5,

	 	 "D007_proporcion_asientos_evento_2018": 53.8,
	 	 "D008_proporcion_asientos_media_evento_2018": 4.61,
	 	 "D009_proporcion_asientos_evento_2019": 38.32,
	 	 "D010_proporcion_asientos_media_evento_2019": 3.98,

	 	 "D011_ocupacion_asientos_evento_2018": 1.0,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.53,
	 	 "D013_ocupacion_asientos_evento_2019": 0.45,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.69,

	 	 "D015_precio_evento_2018": 0.0564,
	 	 "D016_precio_medio_evento_2018": 0.0586,
	 	 "D017_precio_evento_2019": 0.0634,
	 	 "D018_precio_medio_evento_2019": 0.0581,

	 	 "D019_novatos_evento_2018": 2.25,
	 	 "D020_novatos_media_evento_2018": 0.79,
	 	 "D021_novatos_evento_2019": 1.33,
	 	 "D022_novatos_media_evento_2019": 0.36,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-shikillo-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 33, 'Viajes normal': 0.23, 'Proporción': 14520.0}, 1: {'City': 'Ávila', 'Viajes en evento': 5, 'Viajes normal': 0.06, 'Proporción': 8833.33}, 2: {'City': 'Burgos', 'Viajes en evento': 3, 'Viajes normal': 0.0, 'Proporción': "∞"}, 3: {'City': 'Toledo', 'Viajes en evento': 3, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Valladolid', 'Viajes en evento': 2, 'Viajes normal': 0.19, 'Proporción': 1066.67}, 5: {'City': 'Alcobendas', 'Viajes en evento': 2, 'Viajes normal': null, 'Proporción': null}, 6: {'City': 'Arroyomolinos', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 7: {'City': 'Cáceres', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 8: {'City': 'Mérida', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 9: {'City': 'Palencia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-shikillo-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 13, 'Viajes normal': 0.21, 'Proporción': 6153.33}, 1: {'City': 'Talavera de la Reina', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 2: {'City': 'Valladolid', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 2000.0}, 3: {'City': 'Ávila', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2300.0}, 4: {'City': 'Toledo', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Plasencia', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 6: {'City': 'Pozuelo de Alarcón', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-shikillo-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 10, 'Viajes normal': 0.46, 'Proporción': 2196.08}, 1: {'City': 'Ávila', 'Viajes en evento': 1, 'Viajes normal': 0.13, 'Proporción': 766.67}, 2: {'City': 'Valladolid', 'Viajes en evento': 2, 'Viajes normal': 0.13, 'Proporción': 1533.33}, 3: {'City': 'Badajoz', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Guadalajara', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 5: {'City': 'Pamplona', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 6: {'City': 'Setúbal', 'Viajes en evento': 2, 'Viajes normal': null, 'Proporción': null}, 7: {'City': 'Segovia', 'Viajes en evento': 1, 'Viajes normal': 1.0, 'Proporción': 100.0}, 8: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-shikillo-2019.html",
	 	 "tabla_destinos_evento_2019":{},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_shikillo_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_shikillo_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['BERET', 'LA M.O.D.A', 'LA PEGATINA', 'LA RAIZ', 'LAGRIMAS DE SANGRE', 'ZOO', 'LA FUGA', 'DESAKATO', 'BOIKOT', 'LOS DE MARRAS', 'SARA HEBE', 'LITTLE PEPE', 'SHO-HAI', 'DEF CON DOS', 'ANKOR', 'ASIAN DUB FOUNDATION', 'LASAI', 'MALA REPUTACION', 'RIOT PROPAGANDA', 'MACHETE EN BOCA', 'LOS BENITO', 'HORA ZULU', 'DAKIDARRIA', 'MAD DIVISION', 'KITAI', 'VADEBO', 'FREE CITY', 'NARCO', 'EL COLETA', 'CARROÑA', 'DREMEN', 'APHONNIC', 'WISKY CARAVAN', 'OFERTA ESPECIAL', 'AUXILI', 'SILENCIADOS', 'XPRESIDENTX', 'SISTA AWA', 'BOURBON KINGS', 'YO NO LAS CONOZCO', 'REVOLTA PERMANENT', 'FALSA BANDERA', 'NONS OF AGUIRRE'],
	 	 "artistas_evento_2019":['MAREA', 'BLAKE', 'FYAHBWOY', 'DESAKATO', 'BOIKOT', 'MOSH', 'TALCO', 'GATILLAZO', 'SONS OF AGUIRRE', 'SINKOPE', 'JARFAITER', 'GRITANDO EN SILENCIO', 'EL DESVAN', 'SKAKEITAN', 'CAPITAN COBARDE', 'HAMLET', 'KITAI', 'EXCESO', 'NARCO', 'DESVARIADOS', 'SONOTONES', 'MISS OCTUBRE', 'KAMIKAZES', 'KING SAPO', 'CUATRO MADRES', 'VUELO SOS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_shikillo_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_shikillo_2019.jpg",
	 }, 
	 { 
	 	 "id": "44",
	 	 "nombre": "SONORAMA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Aranda de Duero",
	 	 "fecha_ini_2018": "08-08-2018",
	 	 "fecha_fin_2018": "12-08-2018",
	 	 "fecha_ini_2019": "07-08-2019",
	 	 "fecha_fin_2019": "11-08-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 90.33333333333333,
	 	 "D002_evento_media_viajes_2018": 11.81,
	 	 "D003_evento_media_viajes_dia_semana_2018": 8.75,
	 	 "D004_evento_viajes_2019": 104.17,
	 	 "D005_evento_media_viajes_2019": 14.37,
	 	 "D006_evento_media_viajes_dia_semana_2019": 10.7,

	 	 "D007_proporcion_asientos_evento_2018": 39.89,
	 	 "D008_proporcion_asientos_media_evento_2018": 6.42,
	 	 "D009_proporcion_asientos_evento_2019": 43.18,
	 	 "D010_proporcion_asientos_media_evento_2019": 7.83,

	 	 "D011_ocupacion_asientos_evento_2018": 0.23,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.41,
	 	 "D013_ocupacion_asientos_evento_2019": 0.27,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.42,

	 	 "D015_precio_evento_2018": 0.0589,
	 	 "D016_precio_medio_evento_2018": 0.0563,
	 	 "D017_precio_evento_2019": 0.0605,
	 	 "D018_precio_medio_evento_2019": 0.0564,

	 	 "D019_novatos_evento_2018": 22.83,
	 	 "D020_novatos_media_evento_2018": 11.12,
	 	 "D021_novatos_evento_2019": 19.33,
	 	 "D022_novatos_media_evento_2019": 10.13,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-sonorama-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 220, 'Viajes normal': 4.03, 'Proporción': 5455.16}, 1: {'City': 'Burgos', 'Viajes en evento': 67, 'Viajes normal': 2.6, 'Proporción': 2574.21}, 2: {'City': 'Valladolid', 'Viajes en evento': 44, 'Viajes normal': 1.32, 'Proporción': 3327.5}, 3: {'City': 'Zaragoza', 'Viajes en evento': 16, 'Viajes normal': 0.2, 'Proporción': 8140.35}, 4: {'City': 'Bilbao', 'Viajes en evento': 15, 'Viajes normal': 0.32, 'Proporción': 4760.87}, 5: {'City': 'Valencia', 'Viajes en evento': 11, 'Viajes normal': 0.23, 'Proporción': 4796.0}, 6: {'City': 'Salamanca', 'Viajes en evento': 15, 'Viajes normal': 0.44, 'Proporción': 3385.32}, 7: {'City': 'León', 'Viajes en evento': 5, 'Viajes normal': 0.31, 'Proporción': 1638.3}, 8: {'City': 'Albacete', 'Viajes en evento': 4, 'Viajes normal': 0.05, 'Proporción': 7750.0}, 9: {'City': 'Santander', 'Viajes en evento': 8, 'Viajes normal': 0.2, 'Proporción': 4030.19}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-sonorama-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 241, 'Viajes normal': 3.82, 'Proporción': 6310.42}, 1: {'City': 'Burgos', 'Viajes en evento': 75, 'Viajes normal': 3.18, 'Proporción': 2355.37}, 2: {'City': 'Valladolid', 'Viajes en evento': 50, 'Viajes normal': 1.68, 'Proporción': 2968.75}, 3: {'City': 'Bilbao', 'Viajes en evento': 27, 'Viajes normal': 0.43, 'Proporción': 6220.59}, 4: {'City': 'Zaragoza', 'Viajes en evento': 12, 'Viajes normal': 0.2, 'Proporción': 6109.09}, 5: {'City': 'Vitoria-Gasteiz', 'Viajes en evento': 9, 'Viajes normal': 0.13, 'Proporción': 6917.14}, 6: {'City': 'Palencia', 'Viajes en evento': 11, 'Viajes normal': 0.32, 'Proporción': 3485.39}, 7: {'City': 'Salamanca', 'Viajes en evento': 16, 'Viajes normal': 0.34, 'Proporción': 4684.06}, 8: {'City': 'Alcobendas', 'Viajes en evento': 11, 'Viajes normal': 0.15, 'Proporción': 7276.92}, 9: {'City': 'Torrelavega', 'Viajes en evento': 10, 'Viajes normal': 0.07, 'Proporción': 14470.59}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-sonorama-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 106, 'Viajes normal': 4.5, 'Proporción': 2357.71}, 1: {'City': 'Burgos', 'Viajes en evento': 35, 'Viajes normal': 2.9, 'Proporción': 1207.47}, 2: {'City': 'Valladolid', 'Viajes en evento': 21, 'Viajes normal': 1.56, 'Proporción': 1349.47}, 3: {'City': 'Santander', 'Viajes en evento': 8, 'Viajes normal': 0.23, 'Proporción': 3514.75}, 4: {'City': 'Salamanca', 'Viajes en evento': 15, 'Viajes normal': 0.46, 'Proporción': 3294.87}, 5: {'City': 'Zaragoza', 'Viajes en evento': 9, 'Viajes normal': 0.23, 'Proporción': 3937.5}, 6: {'City': 'Gijón', 'Viajes en evento': 12, 'Viajes normal': 0.19, 'Proporción': 6436.36}, 7: {'City': 'León', 'Viajes en evento': 6, 'Viajes normal': 0.33, 'Proporción': 1800.0}, 8: {'City': 'Valencia', 'Viajes en evento': 7, 'Viajes normal': 0.27, 'Proporción': 2575.0}, 9: {'City': 'Aguilar de Campoo', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': 26700.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-sonorama-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 111, 'Viajes normal': 5.18, 'Proporción': 2141.12}, 1: {'City': 'Burgos', 'Viajes en evento': 30, 'Viajes normal': 3.68, 'Proporción': 814.29}, 2: {'City': 'Valladolid', 'Viajes en evento': 24, 'Viajes normal': 1.88, 'Proporción': 1273.3}, 3: {'City': 'Bilbao', 'Viajes en evento': 12, 'Viajes normal': 0.57, 'Proporción': 2100.0}, 4: {'City': 'Zaragoza', 'Viajes en evento': 9, 'Viajes normal': 0.23, 'Proporción': 3916.67}, 5: {'City': 'Palencia', 'Viajes en evento': 6, 'Viajes normal': 0.33, 'Proporción': 1793.81}, 6: {'City': 'Vitoria-Gasteiz', 'Viajes en evento': 5, 'Viajes normal': 0.17, 'Proporción': 2877.55}, 7: {'City': 'Soria', 'Viajes en evento': 4, 'Viajes normal': 0.24, 'Proporción': 1661.02}, 8: {'City': 'Salamanca', 'Viajes en evento': 7, 'Viajes normal': 0.45, 'Proporción': 1564.71}, 9: {'City': 'León', 'Viajes en evento': 7, 'Viajes normal': 0.38, 'Proporción': 1834.48}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_sonorama_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_sonorama_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MAGA', 'MILY CHANCE', 'BUNBURY', 'ROZALEN', 'IZAL', 'LIAM GALLAGHER', 'HOT CHIP', 'NATHY PELUSO', 'LA PEGATINA', 'LA M.O.D.A', 'MORCHEEBA', 'ANAUT', 'SIDECARS', 'MIKEL ERENTXUN', 'LA BIEN QUERIDA', 'XOEL LOPEZ', 'NADA SURF', 'VIVA SUECIA', 'DORIAN', 'CAROLINA DURANTE', 'THE SUBWAYS', 'CORREOS', 'ELEFANTES', 'L.A.', 'NEUMAN', 'VINTAGE TROUBLE', 'AMATRIA', 'ELYELLA', 'RUBEN POZO', 'JOE CREPUSCULO', 'LOS PUNSETES', 'SOLEA MORENTE', 'ANGEL STANICH', 'CALA VENTO', 'TULSA', 'RAYDEN', 'NUNATAK', 'LAGARTIJA NICK', 'EGON SODA', 'LA PLATA', 'NANCYS RUBIAS', 'ARIZONA BABY', 'EMBUSTEROS', 'FLORIDABLANCA', 'SEXY ZEBRAS', 'LOS NASTYS', 'JOSELE SANTIAGO', 'TRIANGULO INVERSO', 'MODELO DE RESPUESTA POLAR', 'LEY DJ', 'JOSE IGNACIO LAPIDO', 'BADLANDS', 'JACOBO SERRA', 'EL MEISTER', 'BURRITO PANZA', 'BABY CANIBAL'],
	 	 "artistas_evento_2019":['MORGAN', 'GRISES', 'DINERO', 'CAT DEALERS', 'SILOE', 'TABURETE', 'CARLOS SADNESS', 'LOVE OF LESBIAN', 'DESPISTAOS', 'THE VACCINES', 'SECOND', 'ZAHARA', 'CRYSTAL FIGHTERS', 'RULO Y LA CONTRABANDA', 'BALTHAZAR', 'MISS CAFFEINA', 'FANGORIA', 'TOTEKING', 'DEPEDRO', 'JAVIERA MENA', 'EL NIÑO DE LA HIPOTECA', 'NIXON', 'FUEL FANDANGO', 'CAROLINA DURANTE', 'BERRI TXARRAK', 'VARRY BRAVA', 'FULL', 'SHINOVA', 'NACHO VEGAS', 'TEQUILA', 'THE FUTUREHEADS', 'JOAN AS POLICE WOMAN', 'LADILLA RUSA', 'ORQUESTA MONDRAGON', 'NACHO CANO', 'TARQUE', 'AUSTRALIAN BLONDE', 'LA SONRISA DE JULIA', 'LA EXCEPCION', 'KITAI', 'DEACON BLUE', 'HOLLY MIRANDA', 'APARTAMENTOS ACAPULCO', 'DELOREAN', 'MUCHO', 'SOLEDAD VELEZ', 'EMBUSTEROS', 'MASTODONTE', 'LUIS ALBERT SEGURA', 'LOS PILOTOS', 'WE ARE NOT DJ', 'LUIS BREA Y EL MIEDO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_sonorama_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_sonorama_2019.jpg",
	 }, 
	 { 
	 	 "id": "45",
	 	 "nombre": "SUMOL SUMMER FEST",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Lisboa",
	 	 "fecha_ini_2018": "06-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "05-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 54.0,
	 	 "D002_evento_media_viajes_2018": 45.26,
	 	 "D003_evento_media_viajes_dia_semana_2018": 59.6,
	 	 "D004_evento_viajes_2019": 40.67,
	 	 "D005_evento_media_viajes_2019": 35.29,
	 	 "D006_evento_media_viajes_dia_semana_2019": 45.93,

	 	 "D007_proporcion_asientos_evento_2018": 44.67,
	 	 "D008_proporcion_asientos_media_evento_2018": 18.42,
	 	 "D009_proporcion_asientos_evento_2019": 31.13,
	 	 "D010_proporcion_asientos_media_evento_2019": 14.06,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.01,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.68,

	 	 "D015_precio_evento_2018": 0.0538,
	 	 "D016_precio_medio_evento_2018": 0.0548,
	 	 "D017_precio_evento_2019": 0.0551,
	 	 "D018_precio_medio_evento_2019": 0.0552,

	 	 "D019_novatos_evento_2018": 10.67,
	 	 "D020_novatos_media_evento_2018": 28.83,
	 	 "D021_novatos_evento_2019": 6.0,
	 	 "D022_novatos_media_evento_2019": 22.66,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-sumol summer fest-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Badajoz', 'Viajes en evento': 22, 'Viajes normal': 3.33, 'Proporción': 660.73}, 1: {'City': 'Sevilla', 'Viajes en evento': 46, 'Viajes normal': 5.26, 'Proporción': 875.27}, 2: {'City': 'Madrid', 'Viajes en evento': 34, 'Viajes normal': 4.01, 'Proporción': 847.08}, 3: {'City': 'Porto', 'Viajes en evento': 21, 'Viajes normal': 6.1, 'Proporción': 344.34}, 4: {'City': 'Coimbra', 'Viajes en evento': 4, 'Viajes normal': 0.79, 'Proporción': 506.01}, 5: {'City': 'Salamanca', 'Viajes en evento': 4, 'Viajes normal': 0.72, 'Proporción': 552.28}, 6: {'City': 'Lagos', 'Viajes en evento': 2, 'Viajes normal': 0.58, 'Proporción': 345.32}, 7: {'City': 'Faro', 'Viajes en evento': 2, 'Viajes normal': 0.81, 'Proporción': 247.45}, 8: {'City': 'Aveiro', 'Viajes en evento': 2, 'Viajes normal': 0.28, 'Proporción': 707.04}, 9: {'City': 'Barcelona', 'Viajes en evento': 2, 'Viajes normal': 0.07, 'Proporción': 2777.78}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-sumol summer fest-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Badajoz', 'Viajes en evento': 22, 'Viajes normal': 2.89, 'Proporción': 760.09}, 1: {'City': 'Madrid', 'Viajes en evento': 26, 'Viajes normal': 3.11, 'Proporción': 836.01}, 2: {'City': 'Porto', 'Viajes en evento': 17, 'Viajes normal': 5.06, 'Proporción': 335.8}, 3: {'City': 'Sevilla', 'Viajes en evento': 22, 'Viajes normal': 3.31, 'Proporción': 665.47}, 4: {'City': 'Vigo', 'Viajes en evento': 6, 'Viajes normal': 0.75, 'Proporción': 802.76}, 5: {'City': 'Faro', 'Viajes en evento': 5, 'Viajes normal': 0.78, 'Proporción': 639.64}, 6: {'City': 'Coimbra', 'Viajes en evento': 2, 'Viajes normal': 0.69, 'Proporción': 290.2}, 7: {'City': 'Cáceres', 'Viajes en evento': 2, 'Viajes normal': 0.5, 'Proporción': 402.25}, 8: {'City': 'El Puerto de Santa María', 'Viajes en evento': 2, 'Viajes normal': 0.2, 'Proporción': 1000.0}, 9: {'City': 'Portimão', 'Viajes en evento': 1, 'Viajes normal': 0.49, 'Proporción': 204.72}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-sumol summer fest-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Badajoz', 'Viajes en evento': 22, 'Viajes normal': 3.28, 'Proporción': 670.13}, 1: {'City': 'Porto', 'Viajes en evento': 27, 'Viajes normal': 6.05, 'Proporción': 446.33}, 2: {'City': 'Sevilla', 'Viajes en evento': 34, 'Viajes normal': 5.34, 'Proporción': 636.3}, 3: {'City': 'Madrid', 'Viajes en evento': 24, 'Viajes normal': 3.71, 'Proporción': 647.53}, 4: {'City': 'Mérida', 'Viajes en evento': 4, 'Viajes normal': 0.47, 'Proporción': 850.33}, 5: {'City': 'Coimbra', 'Viajes en evento': 2, 'Viajes normal': 0.86, 'Proporción': 231.85}, 6: {'City': 'Faro', 'Viajes en evento': 4, 'Viajes normal': 0.85, 'Proporción': 472.98}, 7: {'City': 'Figueira da Foz', 'Viajes en evento': 4, 'Viajes normal': 0.09, 'Proporción': 4685.71}, 8: {'City': 'Salamanca', 'Viajes en evento': 2, 'Viajes normal': 0.68, 'Proporción': 295.19}, 9: {'City': 'Huelva', 'Viajes en evento': 2, 'Viajes normal': 0.37, 'Proporción': 546.48}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-sumol summer fest-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Porto', 'Viajes en evento': 17, 'Viajes normal': 4.9, 'Proporción': 347.08}, 1: {'City': 'Badajoz', 'Viajes en evento': 18, 'Viajes normal': 2.78, 'Proporción': 646.81}, 2: {'City': 'Madrid', 'Viajes en evento': 18, 'Viajes normal': 3.14, 'Proporción': 573.86}, 3: {'City': 'Sevilla', 'Viajes en evento': 18, 'Viajes normal': 3.41, 'Proporción': 527.26}, 4: {'City': 'Vigo', 'Viajes en evento': 6, 'Viajes normal': 0.87, 'Proporción': 688.24}, 5: {'City': 'Coimbra', 'Viajes en evento': 2, 'Viajes normal': 0.69, 'Proporción': 291.26}, 6: {'City': 'Portimão', 'Viajes en evento': 3, 'Viajes normal': 0.44, 'Proporción': 688.42}, 7: {'City': 'Faro', 'Viajes en evento': 4, 'Viajes normal': 0.75, 'Proporción': 532.09}, 8: {'City': 'Figueira da Foz', 'Viajes en evento': 5, 'Viajes normal': 0.06, 'Proporción': 8375.0}, 9: {'City': 'Albufeira', 'Viajes en evento': 4, 'Viajes normal': 0.41, 'Proporción': 964.49}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_sumol summer fest_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_sumol summer fest_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['FRENCH MONTANA', 'JOEY BADASS', 'VIC MENSA', 'WET BED GANG', 'PIRUKA', 'THE JILLONAIRE'],
	 	 "artistas_evento_2019":['YOUNG THUG', 'BROCKHAMPTON', 'DEEJAY TELIO', 'SAM THE KID', 'HOLLY HOOD', 'KAPPA JOTTA', 'KARETUS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_sumol summer fest_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_sumol summer fest_2019.jpg",
	 }, 
	 { 
	 	 "id": "46",
	 	 "nombre": "SÓNAR",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Barcelona",
	 	 "fecha_ini_2018": "14-06-2018",
	 	 "fecha_fin_2018": "16-06-2018",
	 	 "fecha_ini_2019": "18-07-2019",
	 	 "fecha_fin_2019": "20-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 206.25,
	 	 "D002_evento_media_viajes_2018": 190.07,
	 	 "D003_evento_media_viajes_dia_semana_2018": 157.04,
	 	 "D004_evento_viajes_2019": 186.0,
	 	 "D005_evento_media_viajes_2019": 183.12,
	 	 "D006_evento_media_viajes_dia_semana_2019": 148.47,

	 	 "D007_proporcion_asientos_evento_2018": 42.08,
	 	 "D008_proporcion_asientos_media_evento_2018": 23.71,
	 	 "D009_proporcion_asientos_evento_2019": 36.88,
	 	 "D010_proporcion_asientos_media_evento_2019": 22.78,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.86,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.77,

	 	 "D015_precio_evento_2018": 0.0586,
	 	 "D016_precio_medio_evento_2018": 0.058,
	 	 "D017_precio_evento_2019": 0.0613,
	 	 "D018_precio_medio_evento_2019": 0.0603,

	 	 "D019_novatos_evento_2018": 48.0,
	 	 "D020_novatos_media_evento_2018": 91.49,
	 	 "D021_novatos_evento_2019": 53.25,
	 	 "D022_novatos_media_evento_2019": 77.17,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-sónar-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 123, 'Viajes normal': 22.06, 'Proporción': 557.49}, 1: {'City': 'Valencia', 'Viajes en evento': 101, 'Viajes normal': 23.34, 'Proporción': 432.79}, 2: {'City': 'Lleida', 'Viajes en evento': 108, 'Viajes normal': 26.22, 'Proporción': 411.96}, 3: {'City': 'Perpignull', 'Viajes en evento': 40, 'Viajes normal': 7.61, 'Proporción': 525.37}, 4: {'City': 'Montpellier', 'Viajes en evento': 47, 'Viajes normal': 8.23, 'Proporción': 571.07}, 5: {'City': 'Zaragoza', 'Viajes en evento': 17, 'Viajes normal': 4.96, 'Proporción': 342.82}, 6: {'City': 'Toulouse', 'Viajes en evento': 60, 'Viajes normal': 7.57, 'Proporción': 792.12}, 7: {'City': 'Tarragona', 'Viajes en evento': 4, 'Viajes normal': 1.74, 'Proporción': 229.56}, 8: {'City': 'Narbonne', 'Viajes en evento': 8, 'Viajes normal': 1.6, 'Proporción': 500.34}, 9: {'City': 'Girona', 'Viajes en evento': 9, 'Viajes normal': 2.7, 'Proporción': 333.5}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-sónar-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 120, 'Viajes normal': 19.59, 'Proporción': 612.59}, 1: {'City': 'Valencia', 'Viajes en evento': 90, 'Viajes normal': 20.96, 'Proporción': 429.38}, 2: {'City': 'Lleida', 'Viajes en evento': 80, 'Viajes normal': 25.85, 'Proporción': 309.53}, 3: {'City': 'Perpignull', 'Viajes en evento': 33, 'Viajes normal': 5.66, 'Proporción': 582.92}, 4: {'City': 'Zaragoza', 'Viajes en evento': 19, 'Viajes normal': 4.52, 'Proporción': 420.07}, 5: {'City': 'Montpellier', 'Viajes en evento': 33, 'Viajes normal': 6.05, 'Proporción': 545.22}, 6: {'City': 'Tarragona', 'Viajes en evento': 4, 'Viajes normal': 1.56, 'Proporción': 256.0}, 7: {'City': 'Toulouse', 'Viajes en evento': 34, 'Viajes normal': 5.95, 'Proporción': 571.38}, 8: {'City': 'Girona', 'Viajes en evento': 17, 'Viajes normal': 3.36, 'Proporción': 506.17}, 9: {'City': 'Narbonne', 'Viajes en evento': 7, 'Viajes normal': 1.43, 'Proporción': 490.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-sónar-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 98, 'Viajes normal': 23.98, 'Proporción': 408.75}, 1: {'City': 'Lleida', 'Viajes en evento': 115, 'Viajes normal': 26.91, 'Proporción': 427.4}, 2: {'City': 'Madrid', 'Viajes en evento': 67, 'Viajes normal': 23.15, 'Proporción': 289.37}, 3: {'City': 'Zaragoza', 'Viajes en evento': 16, 'Viajes normal': 4.68, 'Proporción': 342.12}, 4: {'City': 'Perpignull', 'Viajes en evento': 35, 'Viajes normal': 8.71, 'Proporción': 401.73}, 5: {'City': 'Montpellier', 'Viajes en evento': 18, 'Viajes normal': 8.95, 'Proporción': 201.23}, 6: {'City': 'Tarragona', 'Viajes en evento': 4, 'Viajes normal': 1.23, 'Proporción': 324.44}, 7: {'City': 'Toulouse', 'Viajes en evento': 33, 'Viajes normal': 8.45, 'Proporción': 390.38}, 8: {'City': 'Girona', 'Viajes en evento': 10, 'Viajes normal': 2.21, 'Proporción': 452.29}, 9: {'City': 'Alicante', 'Viajes en evento': 10, 'Viajes normal': 2.65, 'Proporción': 377.22}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-sónar-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 92, 'Viajes normal': 21.85, 'Proporción': 421.01}, 1: {'City': 'Lleida', 'Viajes en evento': 109, 'Viajes normal': 26.5, 'Proporción': 411.37}, 2: {'City': 'Madrid', 'Viajes en evento': 64, 'Viajes normal': 21.42, 'Proporción': 298.73}, 3: {'City': 'Zaragoza', 'Viajes en evento': 24, 'Viajes normal': 4.56, 'Proporción': 526.79}, 4: {'City': 'Perpignull', 'Viajes en evento': 15, 'Viajes normal': 6.64, 'Proporción': 225.85}, 5: {'City': 'Girona', 'Viajes en evento': 19, 'Viajes normal': 2.82, 'Proporción': 673.19}, 6: {'City': 'Montpellier', 'Viajes en evento': 21, 'Viajes normal': 5.9, 'Proporción': 355.85}, 7: {'City': 'Castellón de la Plana ', 'Viajes en evento': 16, 'Viajes normal': 3.07, 'Proporción': 521.89}, 8: {'City': 'Tarragona', 'Viajes en evento': 7, 'Viajes normal': 1.2, 'Proporción': 584.62}, 9: {'City': 'Alicante', 'Viajes en evento': 15, 'Viajes normal': 2.43, 'Proporción': 616.07}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_sónar_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_sónar_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['GORILLAZ', 'ROSALIA', 'IAMDDB', 'LIBERATO', 'LITTLE SIMZ', 'NIÑO DE ELCHE', 'PUTOCHINOMARICON', 'FRANCISCO LOPEZ', 'ABSOLUTE TERROR', 'LORY MONEY', 'ANGEL MOLINA', '2MANYDJS'],
	 	 "artistas_evento_2019":['BAD BUNNY', 'ARCA', 'ASAP ROCKY', 'DANO', 'BAD GYAL', 'DELLAFUENTE', 'UNDERWORLD', 'AMELIE LENS', 'DAPHNI', 'AFRODEUTSCHE', 'ACID ARAB LIVE', 'TITI CALOR', 'QUIET ENSEMBLE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_sónar_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_sónar_2019.jpg",
	 }, 
	 { 
	 	 "id": "47",
	 	 "nombre": "THE BPM FESTIVAL",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Portimão",
	 	 "fecha_ini_2018": "20-09-2018",
	 	 "fecha_fin_2018": "23-09-2018",
	 	 "fecha_ini_2019": "12-09-2019",
	 	 "fecha_fin_2019": "15-09-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 3.5,
	 	 "D002_evento_media_viajes_2018": 1.25,
	 	 "D003_evento_media_viajes_dia_semana_2018": 1.04,
	 	 "D004_evento_viajes_2019": 3.2,
	 	 "D005_evento_media_viajes_2019": 1.24,
	 	 "D006_evento_media_viajes_dia_semana_2019": 1.02,

	 	 "D007_proporcion_asientos_evento_2018": 42.42,
	 	 "D008_proporcion_asientos_media_evento_2018": 6.06,
	 	 "D009_proporcion_asientos_evento_2019": 29.59,
	 	 "D010_proporcion_asientos_media_evento_2019": 5.83,

	 	 "D011_ocupacion_asientos_evento_2018": 0.07,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.62,
	 	 "D013_ocupacion_asientos_evento_2019": 0.07,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.6,

	 	 "D015_precio_evento_2018": 0.0555,
	 	 "D016_precio_medio_evento_2018": 0.053,
	 	 "D017_precio_evento_2019": 0.068,
	 	 "D018_precio_medio_evento_2019": 0.0573,

	 	 "D019_novatos_evento_2018": 0.75,
	 	 "D020_novatos_media_evento_2018": 2.66,
	 	 "D021_novatos_evento_2019": 0.8,
	 	 "D022_novatos_media_evento_2019": 2.06,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-the bpm festival-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Sevilla', 'Viajes en evento': 8, 'Viajes normal': 0.56, 'Proporción': 1416.67}, 1: {'City': 'Lisboa', 'Viajes en evento': 1, 'Viajes normal': 0.54, 'Proporción': 184.38}, 2: {'City': 'El Viso del Alcor', 'Viajes en evento': 2, 'Viajes normal': 0.5, 'Proporción': 400.0}, 3: {'City': 'Rivas-Vaciamadrid', 'Viajes en evento': 2, 'Viajes normal': null, 'Proporción': null}, 4: {'City': 'Oeiras', 'Viajes en evento': 1, 'Viajes normal': 0.06, 'Proporción': 1600.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-the bpm festival-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Lisboa', 'Viajes en evento': 5, 'Viajes normal': 0.44, 'Proporción': 1147.37}, 1: {'City': 'Sevilla', 'Viajes en evento': 4, 'Viajes normal': 0.5, 'Proporción': 800.0}, 2: {'City': 'Huelva', 'Viajes en evento': 2, 'Viajes normal': 0.03, 'Proporción': 6600.0}, 3: {'City': 'Leganés', 'Viajes en evento': 2, 'Viajes normal': 0.0, 'Proporción': "∞"}, 4: {'City': 'Albufeira', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 15500.0}, 5: {'City': 'Almada', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1400.0}, 6: {'City': 'Barreiro', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-the bpm festival-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Lisboa', 'Viajes en evento': 4, 'Viajes normal': 0.54, 'Proporción': 746.27}, 1: {'City': 'Sevilla', 'Viajes en evento': 6, 'Viajes normal': 0.43, 'Proporción': 1400.0}, 2: {'City': 'El Viso del Alcor', 'Viajes en evento': 2, 'Viajes normal': 0.67, 'Proporción': 300.0}, 3: {'City': 'Granada', 'Viajes en evento': 2, 'Viajes normal': 0.2, 'Proporción': 1025.0}, 4: {'City': 'Huelva', 'Viajes en evento': 2, 'Viajes normal': 0.04, 'Proporción': 5000.0}, 5: {'City': 'Évora', 'Viajes en evento': 1, 'Viajes normal': 0.03, 'Proporción': 3400.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-the bpm festival-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Lisboa', 'Viajes en evento': 5, 'Viajes normal': 0.49, 'Proporción': 1023.58}, 1: {'City': 'Sevilla', 'Viajes en evento': 4, 'Viajes normal': 0.46, 'Proporción': 867.53}, 2: {'City': 'Huelva', 'Viajes en evento': 2, 'Viajes normal': 0.03, 'Proporción': 7600.0}, 3: {'City': 'Albufeira', 'Viajes en evento': 1, 'Viajes normal': 0.01, 'Proporción': 16400.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_the bpm festival_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_the bpm festival_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MAR-T', 'ALI', 'LEON', 'LUCIANO', 'BORIS', 'GUSTA-VO', 'GREEKO', 'HECTOR', 'WAFF', 'SKIZZO', 'PEGGY GOU', 'UNER', 'ALAN FITZPATRICK', 'PATRICK TOPPING', 'NOUR', 'OSCAR L', 'JAMIE JONES', 'MARK KNIGHT', 'THE MARTINEZ BROTHERS', 'CUKY', 'DENNIS CRUZ', 'MOLLY', 'SECONDCITY', 'FRANKY RIZARDO', 'NITIN', 'SECE', 'MR. G', 'OXIA', 'DJ V', 'EGBERT', 'CARL CRAIG', 'CHUS % CEBALLOS', 'CUARTERO', 'DARIUS SYROSSIAN', 'DETLEF', 'GUTI', 'ANDREA OLIVA', 'LEE BURRIGDE', 'MAGDA', 'NIC FANCIULLI', 'MAGAZINO', 'MANDANA', 'MAX CHAPMAN', 'SERGE DEVANT', 'JULIAN JEWEIL', 'THE LEGEND', 'CAIN', 'DANNY TENAGLIA', 'JEAN PIERRE', 'JOSH WINK', 'RAFA BARRIOS', 'TECHNASIA', 'CHRIS LIEBING', 'JOSEPH CAPRIATI', 'LATMUN', 'LOCO DICE', 'YOKOO', 'EMANUEL SATIE', 'KENNY DOPE', 'LUIGI MADONNA', 'MARCO CAROLA', 'NICOLE MOUDABER', 'POPOF', 'RICHIE HAWTIN', 'OCTAVE ONE', 'HECTOR COUTO', 'STEFANO NOFERINI', 'MATTHIAS TANZMANN', 'NENO', 'RICHY AHMED', 'SETH TROXLER', 'DAVID BERRIE', 'PACO OSUNA', 'ART DEPARTMENT', "LIL' LOUIS", 'NICK CURLY', 'SIMINA GRIGORIU', 'CARLO LIO', 'MARCO FARAONE', 'NASTIA', 'VICTOR CALDERONE', 'CARLO RUETZ', 'LEE CURTISS', 'THE MEKANISM', 'FRANCISCO ALLENDES', 'PABLO INZUNZA', 'YAYA', 'DAVIDE SQUILLACE', 'NATHAN BARATO', 'RONY SEIKALY', 'SERGINHO', 'WILLIAM DJOKO', 'DANNY SERRANO', 'PAPA LU', 'JESSE CALOSSO', 'KINNERMAN', 'ALBUQUERQUE', 'DJ DEP', 'NEVERDOGS', 'PIRATE COPY', 'RANDALL M', 'JOHN ACQUAVIVA', 'LAUREN LANE', 'LEO POL', 'MICHELANGELO', 'STACEY PULLEN', 'TIAGO FRAGATEIRO', 'FABIO FLORIDO', 'SEBASTIAN LEDHER', "D'JULZ", 'KYLE HALL', 'LAUREN LO SUNG', 'ALEX KENNON', 'ANDRE BULJAT', 'DJ VIBE', 'FER BR', 'DAVID GTRONIC', 'DAVINA MOSS', 'JULIAN ALEXANDER', 'MATT TOLFREY', 'CHAD ANDREW', 'TOBI NEUMANN', 'SHAUN REEVES', 'JOEY DANIEL', 'CRAIG RICHARDS', 'RHOOWAX', 'RYAN CROSSON', 'ACID MONDAYS', 'ANIMAL & ME', 'BORIS WERNER', 'FRANK STORM', 'GONÇALO', 'MAHONY', 'SAMUEL DEEP', 'KENNY GLASGOW', 'GORJE HEWK & IZHEVSKI', 'JAVIER CARBALLO', 'JOSHUA PUERTA', 'ANNA TUR', 'APOLLONIA', 'CALEB CALLOWAY', 'CARLOS CHAPARRO', 'DA VID', 'HUGO BIANCO', 'MISS SHEILA', 'PETE ZORBA', 'INGI VISIONS', 'MATTEO GATTI', 'FRANK MAUREL', 'II FACES', 'CALI LANAUZE', 'CALVIN CLARKE', 'JAMIE LIE A KWIE', 'BILL PATRICK', 'DUSTY CARTER', 'JULIEN LORETO', 'NICOLA BERNARDINI'],
	 	 "artistas_evento_2019":['JDP', 'MELÉ', 'AME', 'CHRIS LAKE', 'GUSTA-VO', 'GREEKO', 'HECTOR', 'ELI BROWN', 'WAFF', 'SKIZZO', 'SOLARDO', 'ALAN FITZPATRICK', 'PATRICK TOPPING', 'MACEO PLEX', 'JAMIE JONES', 'THE MARTINEZ BROTHERS', 'EATS EVERYTHING', 'SKREAM', 'STEPHAN BODZIN', 'ADRIATIQUE', 'ANDREW MELLER', 'NITIN', 'HONEY DIJON', 'LAURENT GARNIER', 'PLEASUREKRAFT', 'SANCHEZ', 'OXIA', 'ALISHA', 'CARL CRAIG', 'DUBFIRE', 'GUTI', 'JULIET FOX', 'MATADOR', 'SAMA', 'SPEKTRE', 'ANDREA OLIVA', 'MANDANA', 'MAX CHAPMAN', 'JULIAN JEWEIL', 'JEAN PIERRE', 'JOSH WINK', 'TECHNASIA', 'GROMMA', 'JOSEPH CAPRIATI', 'LOCO DICE', 'JANSONS', 'MACCA', 'MARCO CAROLA', 'NICOLE MOUDABER', 'POPOF', 'STEFANO NOFERINI', 'RICHY AHMED', 'ARCHIE HAMILTON', 'DE LA SWING', 'DEL30', 'SETH TROXLER', 'DAVID BERRIE', 'ENZO SIRAGUSA', 'FATIMA HAJJI', 'MARCO STROUS', 'PACO OSUNA', 'PAUL RITCH', 'SEB ZITO', 'FRANCESCA LOMBARDO', 'SIMINA GRIGORIU', 'CARLO LIO', 'MARCO FARAONE', 'VICTOR CALDERONE', 'RICH NXT', 'BEN STERLING', 'JENNIFER CARDINI', 'MASON COLLECTIVE', 'YAYA', 'NATHAN BARATO', 'RONY SEIKALY', 'SERGINHO', 'WILLIAM DJOKO', 'SHALL OCIN', 'DANNY DAZE', 'DJ PEP', 'JESSE CALOSSO', 'KINNERMAN', 'NEVERDOGS', 'PAUL DAREY', 'PIRATE COPY', 'RENATO RATIER', 'STACEY PULLEN', 'TIAGO FRAGATEIRO', 'SEBASTIAN LEDHER', 'KYLE HALL', 'RAY MONO', 'DYED SOUNDOROM', 'ALAN NIEVES', 'CHRIS GARCIA', 'MIKE MORRISEY', 'JOEY DANIEL', 'SINISA TAMAMOVIC', 'ANIMAL & ME', 'GONÇALO', 'CELLINI', 'LUKE WELSH', 'OLLY RYDER', 'ANNA TUR', 'APOLLONIA', 'CALEB CALLOWAY', 'DA VID', 'PETE ZORBA', 'MATTEO GATTI', 'SONJA MOONEAR', 'FRANK MAUREL', 'RAUL PACHECO', 'SILVIE LOTO', 'CALVIN CLARKE', 'DUB TIGER', 'BEN IKIN', 'LEE JO LIFE', 'MOBB HANDED', 'NICOLA BERNARDINI', 'OLI BLANC', 'WAN ISSARA'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_the bpm festival_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_the bpm festival_2019.jpg",
	 }, 
	 { 
	 	 "id": "48",
	 	 "nombre": "TOMAVISTAS",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Madrid",
	 	 "fecha_ini_2018": "25-05-2018",
	 	 "fecha_fin_2018": "26-05-2018",
	 	 "fecha_ini_2019": "24-05-2019",
	 	 "fecha_fin_2019": "25-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 651.3333333333334,
	 	 "D002_evento_media_viajes_2018": 744.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 903.92,
	 	 "D004_evento_viajes_2019": 615.67,
	 	 "D005_evento_media_viajes_2019": 804.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 973.71,

	 	 "D007_proporcion_asientos_evento_2018": 34.97,
	 	 "D008_proporcion_asientos_media_evento_2018": 26.7,
	 	 "D009_proporcion_asientos_evento_2019": 33.74,
	 	 "D010_proporcion_asientos_media_evento_2019": 28.01,

	 	 "D011_ocupacion_asientos_evento_2018": 0.01,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.01,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.8,

	 	 "D015_precio_evento_2018": 0.0484,
	 	 "D016_precio_medio_evento_2018": 0.0491,
	 	 "D017_precio_evento_2019": 0.0497,
	 	 "D018_precio_medio_evento_2019": 0.0502,

	 	 "D019_novatos_evento_2018": 164.33,
	 	 "D020_novatos_media_evento_2018": 214.74,
	 	 "D021_novatos_evento_2019": 140.67,
	 	 "D022_novatos_media_evento_2019": 184.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-tomavistas-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 208, 'Viajes normal': 75.84, 'Proporción': 274.27}, 1: {'City': 'Murcia', 'Viajes en evento': 117, 'Viajes normal': 38.76, 'Proporción': 301.82}, 2: {'City': 'Granada', 'Viajes en evento': 65, 'Viajes normal': 22.2, 'Proporción': 292.76}, 3: {'City': 'Zaragoza', 'Viajes en evento': 39, 'Viajes normal': 14.25, 'Proporción': 273.64}, 4: {'City': 'Sevilla', 'Viajes en evento': 47, 'Viajes normal': 21.66, 'Proporción': 216.99}, 5: {'City': 'Burgos', 'Viajes en evento': 45, 'Viajes normal': 15.87, 'Proporción': 283.58}, 6: {'City': 'Alicante', 'Viajes en evento': 74, 'Viajes normal': 24.77, 'Proporción': 298.75}, 7: {'City': 'Albacete', 'Viajes en evento': 34, 'Viajes normal': 15.02, 'Proporción': 226.34}, 8: {'City': 'Barcelona', 'Viajes en evento': 66, 'Viajes normal': 23.15, 'Proporción': 285.06}, 9: {'City': 'Jaén', 'Viajes en evento': 20, 'Viajes normal': 10.77, 'Proporción': 185.75}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-tomavistas-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 196, 'Viajes normal': 81.06, 'Proporción': 241.8}, 1: {'City': 'Murcia', 'Viajes en evento': 101, 'Viajes normal': 42.04, 'Proporción': 240.25}, 2: {'City': 'Zaragoza', 'Viajes en evento': 54, 'Viajes normal': 15.0, 'Proporción': 360.0}, 3: {'City': 'Burgos', 'Viajes en evento': 47, 'Viajes normal': 17.33, 'Proporción': 271.22}, 4: {'City': 'Albacete', 'Viajes en evento': 34, 'Viajes normal': 15.92, 'Proporción': 213.51}, 5: {'City': 'Sevilla', 'Viajes en evento': 46, 'Viajes normal': 20.66, 'Proporción': 222.6}, 6: {'City': 'Granada', 'Viajes en evento': 45, 'Viajes normal': 21.68, 'Proporción': 207.52}, 7: {'City': 'Barcelona', 'Viajes en evento': 69, 'Viajes normal': 21.42, 'Proporción': 322.06}, 8: {'City': 'Salamanca', 'Viajes en evento': 77, 'Viajes normal': 28.08, 'Proporción': 274.23}, 9: {'City': 'Alicante', 'Viajes en evento': 51, 'Viajes normal': 24.62, 'Proporción': 207.19}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-tomavistas-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 264, 'Viajes normal': 74.61, 'Proporción': 353.85}, 1: {'City': 'Murcia', 'Viajes en evento': 128, 'Viajes normal': 37.84, 'Proporción': 338.23}, 2: {'City': 'Granada', 'Viajes en evento': 83, 'Viajes normal': 21.29, 'Proporción': 389.8}, 3: {'City': 'Albacete', 'Viajes en evento': 62, 'Viajes normal': 13.73, 'Proporción': 451.43}, 4: {'City': 'Burgos', 'Viajes en evento': 60, 'Viajes normal': 15.47, 'Proporción': 387.82}, 5: {'City': 'Sevilla', 'Viajes en evento': 75, 'Viajes normal': 19.96, 'Proporción': 375.72}, 6: {'City': 'Córdoba', 'Viajes en evento': 86, 'Viajes normal': 9.0, 'Proporción': 955.85}, 7: {'City': 'Salamanca', 'Viajes en evento': 103, 'Viajes normal': 25.84, 'Proporción': 398.67}, 8: {'City': 'Mérida', 'Viajes en evento': 39, 'Viajes normal': 10.33, 'Proporción': 377.39}, 9: {'City': 'Zaragoza', 'Viajes en evento': 50, 'Viajes normal': 13.25, 'Proporción': 377.38}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-tomavistas-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 293, 'Viajes normal': 80.15, 'Proporción': 365.57}, 1: {'City': 'Sevilla', 'Viajes en evento': 163, 'Viajes normal': 20.65, 'Proporción': 789.17}, 2: {'City': 'Murcia', 'Viajes en evento': 150, 'Viajes normal': 41.38, 'Proporción': 362.48}, 3: {'City': 'Córdoba', 'Viajes en evento': 144, 'Viajes normal': 9.29, 'Proporción': 1550.69}, 4: {'City': 'Granada', 'Viajes en evento': 101, 'Viajes normal': 20.91, 'Proporción': 483.0}, 5: {'City': 'Albacete', 'Viajes en evento': 72, 'Viajes normal': 14.29, 'Proporción': 503.75}, 6: {'City': 'Mérida', 'Viajes en evento': 52, 'Viajes normal': 10.91, 'Proporción': 476.58}, 7: {'City': 'Zaragoza', 'Viajes en evento': 64, 'Viajes normal': 14.19, 'Proporción': 451.0}, 8: {'City': 'Salamanca', 'Viajes en evento': 116, 'Viajes normal': 28.94, 'Proporción': 400.77}, 9: {'City': 'Burgos', 'Viajes en evento': 63, 'Viajes normal': 16.48, 'Proporción': 382.28}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_tomavistas_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_tomavistas_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MELANGE', 'PRINCESS NOKIA', 'LA CASA AZUL', 'EL MATO A UN POLICIA MOTORIZADO', 'LA BIEN QUERIDA', 'ROOSEVELT', 'THE JESUS AND MARY CHAIN', 'JAVIERA MENA', 'LOS PLANETAS', 'NOVEDADES CARMINHA', 'RIDE', 'DJANGO DJANGO', 'EL COLUMPIO ASESINO', 'BELAKO', 'PERRO', 'TULSA', 'SUPERCHUNK', 'PONY BRAVO', 'DISCO LAS PALMERAS'],
	 	 "artistas_evento_2019":['MORGAN', 'CIGARETTES AFTER SEX', 'BEACH HOUSE', 'TORO Y MOI', 'CASS MCCOMBS', 'DEERHUNTER', 'DIGITALISM', 'FRIENDLY FIRES', 'SPIRITUALIZED', 'CAROLINA DURANTE', 'CARIÑO', 'HINDS', 'WOODEN SHJIPS', 'JOE CREPUSCULO', 'TRIANGULO DE AMOR BIZARRO', 'ANGEL STANICH', 'SOLEA MORENTE', 'CALA VENTO', 'STONEFIELD', 'MUCHO', 'YAWNERS', 'ENRIC MONTEFUSCO', 'LAS ODIO', 'TREPAT', ''],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_tomavistas_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_tomavistas_2019.jpg",
	 }, 
	 { 
	 	 "id": "49",
	 	 "nombre": "VIDA",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Vilanova i la Geltrú",
	 	 "fecha_ini_2018": "28-06-2018",
	 	 "fecha_fin_2018": "01-07-2018",
	 	 "fecha_ini_2019": "04-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 2.0,
	 	 "D002_evento_media_viajes_2018": 0.4,
	 	 "D003_evento_media_viajes_dia_semana_2018": 0.33,
	 	 "D004_evento_viajes_2019": 2.0,
	 	 "D005_evento_media_viajes_2019": 0.39,
	 	 "D006_evento_media_viajes_dia_semana_2019": 0.38,

	 	 "D007_proporcion_asientos_evento_2018": 44.0,
	 	 "D008_proporcion_asientos_media_evento_2018": 2.36,
	 	 "D009_proporcion_asientos_evento_2019": 27.78,
	 	 "D010_proporcion_asientos_media_evento_2019": 2.05,

	 	 "D011_ocupacion_asientos_evento_2018": 0.08,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.29,
	 	 "D013_ocupacion_asientos_evento_2019": 0.04,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.27,

	 	 "D015_precio_evento_2018": 0.0544,
	 	 "D016_precio_medio_evento_2018": 0.057,
	 	 "D017_precio_evento_2019": 0.0649,
	 	 "D018_precio_medio_evento_2019": 0.06,

	 	 "D019_novatos_evento_2018": 0.0,
	 	 "D020_novatos_media_evento_2018": 1.13,
	 	 "D021_novatos_evento_2019": 0.0,
	 	 "D022_novatos_media_evento_2019": 1.08,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-vida-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 3, 'Viajes normal': 0.28, 'Proporción': 1071.43}, 1: {'City': 'Valencia', 'Viajes en evento': 2, 'Viajes normal': 0.31, 'Proporción': 650.0}, 2: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1350.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-vida-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.37, 'Proporción': 270.59}, 1: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.13, 'Proporción': 1553.85}, 2: {'City': "Sant Joan d'Alacant", 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-vida-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Bilbao', 'Viajes en evento': 1, 'Viajes normal': 0.27, 'Proporción': 375.0}, 1: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.28, 'Proporción': 351.72}, 2: {'City': 'Valencia', 'Viajes en evento': 1, 'Viajes normal': 0.18, 'Proporción': 545.83}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-vida-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Igualada', 'Viajes en evento': 1, 'Viajes normal': 0.0, 'Proporción': "∞"}, 1: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.2, 'Proporción': 506.25}, 2: {'City': 'Zaragoza', 'Viajes en evento': 1, 'Viajes normal': 0.12, 'Proporción': 814.29}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_vida_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_vida_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MAGA', 'IRON & WINE', 'FRANZ FERDINAND', 'VULK', 'NICK MULVEY', 'ST. VINCENT', 'THEY MIGHT BE GIANTS', 'TENSNAKE', 'OF MONTREAL', 'CURTIS HARDING', 'CALEXICO', 'LOS PLANETAS', 'NOVEDADES CARMINHA', 'ALBERT PLA', 'JOE CREPUSCULO', 'J. BERNARDT', 'PERRO', 'NURIA GRAHAM', 'MAREM LADSON', 'ESTOEST', 'MOURN', 'OSO LEONE', 'GREAT NEWS', 'DBFC', 'ELVIS PERKINS', 'BEGUN', 'FUTURO TERROR', 'MODELO DE RESPUESTA POLAR', 'JACOBO SERRA', 'GUILLE MILKYWAY', 'PACOSAN', 'THE NEW RAEMON & MCENROE'],
	 	 "artistas_evento_2019":['GUS DAPPERTON', 'JOSE GONZALEZ', 'ARIES', 'POLO & PAN', 'SHARON VAN ETTEN', 'MADNESS', 'HOT CHIP', 'BEIRUT', 'KEVIN MORBY', 'EL MATO A UN POLICIA MOTORIZADO', 'JULIA JACKLIN', 'FONTAINERS', 'MEUTE', 'TODD TERJE', 'THE CHARLATANS', 'TEMPLES', 'CAROLINA DURANTE', 'MARLON WILLIAMS', 'DIDIRRI', 'WESTERMAN', 'STELLA DONNELLY', 'SLEAFORD MODS', 'NACHO VEGAS', 'CARIÑO', 'MOLLY BURCH', 'FAT WHITE FAMILY', 'FERRAN PALAU', 'ALONDRA BENTLEY', 'EL PETIT DE CAL ERIL', 'PAU VALLVE', 'CALA VENTO', 'MAVICA', 'SUPERCHUNK', 'JUAN COLOMO', 'DERBY MOTORETA BURRITO KACHIMBA', 'HIDROGENESSE', 'THE 2 BEARS', 'LUISE CHEN', 'SOLEDAD VELEZ', 'YAWNERS', 'EGOSEX', 'INVISIBLE HARVEY', 'RAMON CASTELLS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_vida_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_vida_2019.jpg",
	 }, 
	 { 
	 	 "id": "50",
	 	 "nombre": "VIÑA ROCK",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Villarrobledo",
	 	 "fecha_ini_2018": "28-04-2018",
	 	 "fecha_fin_2018": "30-04-2018",
	 	 "fecha_ini_2019": "02-05-2019",
	 	 "fecha_fin_2019": "04-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 186.75,
	 	 "D002_evento_media_viajes_2018": 7.21,
	 	 "D003_evento_media_viajes_dia_semana_2018": 8.37,
	 	 "D004_evento_viajes_2019": 181.25,
	 	 "D005_evento_media_viajes_2019": 8.63,
	 	 "D006_evento_media_viajes_dia_semana_2019": 6.88,

	 	 "D007_proporcion_asientos_evento_2018": 49.14,
	 	 "D008_proporcion_asientos_media_evento_2018": 5.26,
	 	 "D009_proporcion_asientos_evento_2019": 49.44,
	 	 "D010_proporcion_asientos_media_evento_2019": 6.31,

	 	 "D011_ocupacion_asientos_evento_2018": 0.54,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.49,
	 	 "D013_ocupacion_asientos_evento_2019": 0.52,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.47,

	 	 "D015_precio_evento_2018": 0.056,
	 	 "D016_precio_medio_evento_2018": 0.0542,
	 	 "D017_precio_evento_2019": 0.0603,
	 	 "D018_precio_medio_evento_2019": 0.0544,

	 	 "D019_novatos_evento_2018": 98.0,
	 	 "D020_novatos_media_evento_2018": 7.87,
	 	 "D021_novatos_evento_2019": 16.75,
	 	 "D022_novatos_media_evento_2019": 6.07,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-viña rock-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 162, 'Viajes normal': 0.69, 'Proporción': 23458.44}, 1: {'City': 'Valencia', 'Viajes en evento': 127, 'Viajes normal': 1.89, 'Proporción': 6734.56}, 2: {'City': 'Barcelona', 'Viajes en evento': 57, 'Viajes normal': 0.43, 'Proporción': 13219.15}, 3: {'City': 'Albacete', 'Viajes en evento': 39, 'Viajes normal': 0.72, 'Proporción': 5433.21}, 4: {'City': 'Murcia', 'Viajes en evento': 28, 'Viajes normal': 0.38, 'Proporción': 7384.62}, 5: {'City': 'Granada', 'Viajes en evento': 29, 'Viajes normal': 0.34, 'Proporción': 8655.38}, 6: {'City': 'Tarragona', 'Viajes en evento': 6, 'Viajes normal': 0.04, 'Proporción': 17000.0}, 7: {'City': 'Burgos', 'Viajes en evento': 9, 'Viajes normal': 0.18, 'Proporción': 4950.0}, 8: {'City': 'Castellón de la Plana ', 'Viajes en evento': 5, 'Viajes normal': 0.09, 'Proporción': 5600.0}, 9: {'City': 'Zaragoza', 'Viajes en evento': 18, 'Viajes normal': 0.29, 'Proporción': 6230.77}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-viña rock-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 138, 'Viajes normal': 0.79, 'Proporción': 17563.64}, 1: {'City': 'Valencia', 'Viajes en evento': 107, 'Viajes normal': 2.17, 'Proporción': 4929.58}, 2: {'City': 'Albacete', 'Viajes en evento': 44, 'Viajes normal': 1.22, 'Proporción': 3615.14}, 3: {'City': 'Barcelona', 'Viajes en evento': 45, 'Viajes normal': 0.43, 'Proporción': 10384.62}, 4: {'City': 'Murcia', 'Viajes en evento': 30, 'Viajes normal': 0.34, 'Proporción': 8830.99}, 5: {'City': 'Zaragoza', 'Viajes en evento': 20, 'Viajes normal': 0.45, 'Proporción': 4444.44}, 6: {'City': 'Toledo', 'Viajes en evento': 11, 'Viajes normal': 0.35, 'Proporción': 3100.0}, 7: {'City': 'Granada', 'Viajes en evento': 25, 'Viajes normal': 0.28, 'Proporción': 8846.15}, 8: {'City': 'Burgos', 'Viajes en evento': 17, 'Viajes normal': 0.8, 'Proporción': 2125.0}, 9: {'City': 'Alicante', 'Viajes en evento': 15, 'Viajes normal': 0.26, 'Proporción': 5727.27}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-viña rock-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 35, 'Viajes normal': 2.5, 'Proporción': 1402.34}, 1: {'City': 'Albacete', 'Viajes en evento': 7, 'Viajes normal': 1.11, 'Proporción': 629.31}, 2: {'City': 'Madrid', 'Viajes en evento': 19, 'Viajes normal': 1.19, 'Proporción': 1602.41}, 3: {'City': 'Barcelona', 'Viajes en evento': 6, 'Viajes normal': 0.71, 'Proporción': 840.0}, 4: {'City': 'Córdoba', 'Viajes en evento': 2, 'Viajes normal': 0.07, 'Proporción': 2736.36}, 5: {'City': 'Ciudad Real', 'Viajes en evento': 3, 'Viajes normal': 0.28, 'Proporción': 1075.53}, 6: {'City': 'Murcia', 'Viajes en evento': 3, 'Viajes normal': 0.41, 'Proporción': 732.69}, 7: {'City': 'Zaragoza', 'Viajes en evento': 4, 'Viajes normal': 0.59, 'Proporción': 672.73}, 8: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.08, 'Proporción': 2510.0}, 9: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.38, 'Proporción': 785.71}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-viña rock-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Albacete', 'Viajes en evento': 8, 'Viajes normal': 1.63, 'Proporción': 491.31}, 1: {'City': 'Valencia', 'Viajes en evento': 24, 'Viajes normal': 2.75, 'Proporción': 872.46}, 2: {'City': 'Madrid', 'Viajes en evento': 19, 'Viajes normal': 1.22, 'Proporción': 1557.66}, 3: {'City': 'Manzanares', 'Viajes en evento': 3, 'Viajes normal': 0.07, 'Proporción': 4257.14}, 4: {'City': 'Sevilla', 'Viajes en evento': 2, 'Viajes normal': 0.1, 'Proporción': 2030.0}, 5: {'City': 'Alicante', 'Viajes en evento': 3, 'Viajes normal': 0.45, 'Proporción': 674.16}, 6: {'City': 'Almansa', 'Viajes en evento': 1, 'Viajes normal': 0.02, 'Proporción': 4925.0}, 7: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.2, 'Proporción': 488.0}, 8: {'City': 'Zaragoza', 'Viajes en evento': 2, 'Viajes normal': 0.68, 'Proporción': 296.0}, 9: {'City': 'Cádiz', 'Viajes en evento': 2, 'Viajes normal': 0.11, 'Proporción': 1800.0}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_viña rock_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_viña rock_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['DARDEM', 'AYAX Y PROK', 'SFDK', 'LA PEGATINA', 'TXARANGO', 'RULO Y LA CONTRABANDA', 'DENOM', 'LA RAIZ', 'SHARIF', 'MUERDO', 'JUANITO MAKANDE', 'ZOO', 'GREEN VALLEY', 'FOYONE', 'TOTEKING', 'DESAKATO', 'EL NIÑO DE LA HIPOTECA', 'WARCRY', 'BOIKOT', 'REINCIDENTES', 'BUHOS', 'PONCHO K', 'MUCHACHITO', 'RUSSKAJA', 'MEDINA AZAHARA', 'EL ULTIMO KE ZIERRE', 'EL CANIJO DE JEREZ', 'ITACA BAND', 'NON SERVIUM', 'DUBIOZA KOLEKTIV', 'GATILLAZO', 'PORRETAS', 'KAOTIKO', 'SOZIEDAD ALKOHOLIKA', 'SEGISMUNDO TOXICOMANO', 'SMOKING SOULS', 'FALSALARMA', 'GORDO MASTER', 'SHOTTA', 'DEF CON DOS', 'TOMASITO', 'EL RENO RENARDO', 'KONSUMO RESPETO', 'RIOT PROPAGANDA', 'RAYDEN', 'LOS BENITO', 'XAVI SARRIA', 'STRAVAGANZZA', 'CHE SUDAKA', 'HORA ZULU', 'HAMLET', 'LA REGADERA', 'TOUNDRA', 'DAKIDARRIA', 'PARABELLUM', 'VADEBO', 'EXCESO', 'FUNKIWIS', 'SAUROM', 'NARCO', 'KAZE', 'ANGELUS APATRIDA', 'MAFALDA', 'DREMEN', 'SEXY ZEBRAS', 'VENDETTA', 'OFERTA ESPECIAL', 'PROZAK SOUP', 'AUXILI', 'ATZEMBLA', 'JOSETXU PIPERRAK', 'EL TIO LA CARETA', 'GREENLIGHT', 'BOURBON KINGS', 'BATRACIO', 'KING KONG BOY', 'CAZAFUNKTASMAS', 'NWHO-HAI'],
	 	 "artistas_evento_2019":['KOMA', 'SUU', 'BERET', 'LOS AUTENTICOS DECADENTES', 'ROZALEN', 'NATOS Y WAOR', 'AYAX Y PROK', 'SKA-P', 'SFDK', 'LA BERISO', 'IRATXO', 'LA PEGATINA', 'LA M.O.D.A', 'EL KANKA', 'LAGRIMAS DE SANGRE', 'GREEN VALLEY', 'ZOO', 'JUANITO MAKANDE', 'RAPSUSKLEI', 'LA FUGA', 'TOTEKING', 'LOS CHIKOS DEL MAIZ', 'DESAKATO', 'BOIKOT', 'LOS DE MARRAS', 'BUHOS', 'BERRI TXARRAK', 'TALCO', 'CRIM', 'SARATOGA', 'DUBIOZA KOLEKTIV', 'TU OTRA BONITA', 'GATILLAZO', 'KAOTIKO', 'SONS OF AGUIRRE', 'BARON ROJO', 'SINKOPE', 'SOZIEDAD ALKOHOLIKA', 'SMOKING SOULS', 'MOJINOS ESCOZIOS', 'TREMENDA JAURIA', 'SEGISMUNDO TOXICOMANO', 'SKALARI', 'SOBER', 'KULTO KULTIBO', 'VALIRA', 'SHOTTA', 'KAOS URBANO', 'LOS ZIGARROS', 'EL DILUVI', 'SKAKEITAN', 'EL RENO RENARDO', 'MACHETE EN BOCA', 'XAVI SARRIA', 'STRAVAGANZZA', 'TRIPLE XXX', 'KDERS', 'RUDE PRIDE', 'LA EXCEPCION', 'CRISIX', 'NARCO', 'SAUROM', 'KAZE', 'RAT-ZINGER', 'MAFALDA', 'ALAMEDADOSOULNA', 'LIMANDO', 'PERANOIA', 'FUEGO AMIGO', 'PAKO ESKORBUTO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_viña rock_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_viña rock_2019.jpg",
	 }, 
	 { 
	 	 "id": "51",
	 	 "nombre": "WARM UP",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Murcia",
	 	 "fecha_ini_2018": "04-05-2018",
	 	 "fecha_fin_2018": "05-05-2018",
	 	 "fecha_ini_2019": "03-05-2019",
	 	 "fecha_fin_2019": "04-05-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 178.33333333333334,
	 	 "D002_evento_media_viajes_2018": 128.29,
	 	 "D003_evento_media_viajes_dia_semana_2018": 187.58,
	 	 "D004_evento_viajes_2019": 164.33,
	 	 "D005_evento_media_viajes_2019": 142.08,
	 	 "D006_evento_media_viajes_dia_semana_2019": 195.88,

	 	 "D007_proporcion_asientos_evento_2018": 32.28,
	 	 "D008_proporcion_asientos_media_evento_2018": 16.14,
	 	 "D009_proporcion_asientos_evento_2019": 30.17,
	 	 "D010_proporcion_asientos_media_evento_2019": 16.83,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.64,
	 	 "D013_ocupacion_asientos_evento_2019": 0.02,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.61,

	 	 "D015_precio_evento_2018": 0.0536,
	 	 "D016_precio_medio_evento_2018": 0.0542,
	 	 "D017_precio_evento_2019": 0.0565,
	 	 "D018_precio_medio_evento_2019": 0.0557,

	 	 "D019_novatos_evento_2018": 21.67,
	 	 "D020_novatos_media_evento_2018": 54.27,
	 	 "D021_novatos_evento_2019": 17.67,
	 	 "D022_novatos_media_evento_2019": 46.51,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-warm up-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 183, 'Viajes normal': 37.84, 'Proporción': 483.57}, 1: {'City': 'Valencia', 'Viajes en evento': 75, 'Viajes normal': 13.28, 'Proporción': 564.67}, 2: {'City': 'Granada', 'Viajes en evento': 48, 'Viajes normal': 11.16, 'Proporción': 430.04}, 3: {'City': 'Alicante', 'Viajes en evento': 20, 'Viajes normal': 4.79, 'Proporción': 417.38}, 4: {'City': 'Albacete', 'Viajes en evento': 26, 'Viajes normal': 6.57, 'Proporción': 395.91}, 5: {'City': 'Cartagena', 'Viajes en evento': 8, 'Viajes normal': 2.58, 'Proporción': 309.98}, 6: {'City': 'Lorca', 'Viajes en evento': 3, 'Viajes normal': 2.36, 'Proporción': 127.33}, 7: {'City': 'Almería', 'Viajes en evento': 27, 'Viajes normal': 6.32, 'Proporción': 426.99}, 8: {'City': 'Elche', 'Viajes en evento': 4, 'Viajes normal': 0.9, 'Proporción': 445.12}, 9: {'City': 'Málaga', 'Viajes en evento': 19, 'Viajes normal': 4.47, 'Proporción': 424.94}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-warm up-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 148, 'Viajes normal': 41.38, 'Proporción': 357.65}, 1: {'City': 'Valencia', 'Viajes en evento': 62, 'Viajes normal': 15.11, 'Proporción': 410.45}, 2: {'City': 'Cartagena', 'Viajes en evento': 5, 'Viajes normal': 4.95, 'Proporción': 101.0}, 3: {'City': 'Granada', 'Viajes en evento': 46, 'Viajes normal': 10.81, 'Proporción': 425.56}, 4: {'City': 'Alicante', 'Viajes en evento': 24, 'Viajes normal': 5.71, 'Proporción': 420.28}, 5: {'City': 'Albacete', 'Viajes en evento': 23, 'Viajes normal': 7.45, 'Proporción': 308.7}, 6: {'City': 'Lorca', 'Viajes en evento': 8, 'Viajes normal': 2.94, 'Proporción': 272.34}, 7: {'City': 'Almería', 'Viajes en evento': 27, 'Viajes normal': 6.73, 'Proporción': 401.17}, 8: {'City': 'Elche', 'Viajes en evento': 4, 'Viajes normal': 1.46, 'Proporción': 274.49}, 9: {'City': 'Málaga', 'Viajes en evento': 18, 'Viajes normal': 4.9, 'Proporción': 367.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-warm up-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Madrid', 'Viajes en evento': 110, 'Viajes normal': 38.76, 'Proporción': 283.77}, 1: {'City': 'Granada', 'Viajes en evento': 42, 'Viajes normal': 10.97, 'Proporción': 382.87}, 2: {'City': 'Lorca', 'Viajes en evento': 13, 'Viajes normal': 1.93, 'Proporción': 673.05}, 3: {'City': 'Valencia', 'Viajes en evento': 35, 'Viajes normal': 13.88, 'Proporción': 252.17}, 4: {'City': 'Albacete', 'Viajes en evento': 34, 'Viajes normal': 6.04, 'Proporción': 562.56}, 5: {'City': 'Alicante', 'Viajes en evento': 15, 'Viajes normal': 5.09, 'Proporción': 294.67}, 6: {'City': 'Almería', 'Viajes en evento': 28, 'Viajes normal': 6.29, 'Proporción': 445.12}, 7: {'City': 'Hellín', 'Viajes en evento': 12, 'Viajes normal': 1.49, 'Proporción': 803.67}, 8: {'City': 'Málaga', 'Viajes en evento': 17, 'Viajes normal': 4.57, 'Proporción': 371.78}, 9: {'City': 'Cartagena', 'Viajes en evento': 2, 'Viajes normal': 2.43, 'Proporción': 82.39}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-warm up-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Madrid', 'Viajes en evento': 101, 'Viajes normal': 42.04, 'Proporción': 240.25}, 1: {'City': 'Granada', 'Viajes en evento': 44, 'Viajes normal': 10.63, 'Proporción': 413.86}, 2: {'City': 'Albacete', 'Viajes en evento': 31, 'Viajes normal': 6.69, 'Proporción': 463.55}, 3: {'City': 'Lorca', 'Viajes en evento': 12, 'Viajes normal': 2.53, 'Proporción': 475.0}, 4: {'City': 'Valencia', 'Viajes en evento': 47, 'Viajes normal': 15.38, 'Proporción': 305.56}, 5: {'City': 'Cartagena', 'Viajes en evento': 5, 'Viajes normal': 4.57, 'Proporción': 109.51}, 6: {'City': 'Alicante', 'Viajes en evento': 22, 'Viajes normal': 6.0, 'Proporción': 366.67}, 7: {'City': 'Almería', 'Viajes en evento': 24, 'Viajes normal': 6.59, 'Proporción': 364.44}, 8: {'City': 'Sevilla', 'Viajes en evento': 21, 'Viajes normal': 3.64, 'Proporción': 576.88}, 9: {'City': 'Elche', 'Viajes en evento': 3, 'Viajes normal': 1.35, 'Proporción': 222.98}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_warm up_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_warm up_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['MELANGE', 'ALT-J', 'DURGA', 'KASABIAN', 'IZAL', 'CARLOS SADNESS', 'MS NINA', 'IVAN FERREIRO', 'COOPER', 'THE BLOODY BEETROOTS', 'SIDONIE', 'VITALIC', 'NADA SURF', 'DORIAN', 'VIVA SUECIA', 'VARRY BRAVA', 'YELLE', '!!! CHK CHK CHK', 'SHINOVA', 'NEUMAN', 'EL COLUMPIO ASESINO', 'ELYELLA', 'JOE CREPUSCULO', 'RUFUS T.FIREFLY', 'CALA VENTO', 'SIERRA', 'NUNATAK', 'MAIKA MAKOVSKY', 'THE OCTOPUS PROJECT', 'MUEVELOREINA', 'BIZNAGA', 'ED IS DEAD', 'LOS PEPES', 'LA PLATA', 'THE PARROTS', 'ESTEBAN & MANUEL', 'MODELO DE RESPUESTA POLAR', 'WISEMEN PROJECT', 'VIRGINIA MAESTRO', 'LEBOWSKY', 'DJ COCO', 'NORTH STATE', 'SIX CATS', 'PBSR', 'THE YELLOW MELODIES', 'MURCIANO TOTAL', 'JAIME BUENAVENTURA', 'WOMEN BEAT', 'OCHOYMEDIO DJ'],
	 	 "artistas_evento_2019":['TYGA', 'TWO DOOR CINEMA CLUB', 'VETUSTA MORLA', 'SECOND', 'NOEL GALLAGHER', 'AMAIA', 'ZAHARA', 'LA M.O.D.A', 'LA CASA AZUL', 'ROOSEVELT', 'CUPIDO', 'THE JESUS AND MARY CHAIN', 'MISS CAFFEINA', 'JAVIERA MENA', 'CAROLINA DURANTE', 'CATNAPP', 'DELAPORTE', 'NOVEDADES CARMINHA', 'CARIÑO', 'TEENAGE FANCLUB', 'ALONDRA BENTLEY', 'THE SHIVAS', 'PEDRINA', 'PUTOCHINOMARICON', 'SPACE SURIMI', 'A GIANT DOG', 'THE SOUND OF ARROWS', 'SAU POLER', 'THE MOLOCHS', 'CRUDO PIMIENTO', '2MANYDJS', 'SURI KATO'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_warm up_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_warm up_2019.jpg",
	 }, 
	 { 
	 	 "id": "52",
	 	 "nombre": "WEEKEND BEACH",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Vélez-Málaga",
	 	 "fecha_ini_2018": "04-07-2018",
	 	 "fecha_fin_2018": "07-07-2018",
	 	 "fecha_ini_2019": "03-07-2019",
	 	 "fecha_fin_2019": "06-07-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 28.6,
	 	 "D002_evento_media_viajes_2018": 4.27,
	 	 "D003_evento_media_viajes_dia_semana_2018": 2.58,
	 	 "D004_evento_viajes_2019": 36.6,
	 	 "D005_evento_media_viajes_2019": 6.22,
	 	 "D006_evento_media_viajes_dia_semana_2019": 5.02,

	 	 "D007_proporcion_asientos_evento_2018": 34.69,
	 	 "D008_proporcion_asientos_media_evento_2018": 5.61,
	 	 "D009_proporcion_asientos_evento_2019": 37.07,
	 	 "D010_proporcion_asientos_media_evento_2019": 7.4,

	 	 "D011_ocupacion_asientos_evento_2018": 0.15,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.41,
	 	 "D013_ocupacion_asientos_evento_2019": 0.2,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.4,

	 	 "D015_precio_evento_2018": 0.0598,
	 	 "D016_precio_medio_evento_2018": 0.0568,
	 	 "D017_precio_evento_2019": 0.0588,
	 	 "D018_precio_medio_evento_2019": 0.0589,

	 	 "D019_novatos_evento_2018": 6.6,
	 	 "D020_novatos_media_evento_2018": 4.35,
	 	 "D021_novatos_evento_2019": 5.0,
	 	 "D022_novatos_media_evento_2019": 3.77,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-weekend beach-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Granada', 'Viajes en evento': 44, 'Viajes normal': 1.71, 'Proporción': 2570.79}, 1: {'City': 'Madrid', 'Viajes en evento': 22, 'Viajes normal': 0.51, 'Proporción': 4327.87}, 2: {'City': 'Sevilla', 'Viajes en evento': 12, 'Viajes normal': 0.5, 'Proporción': 2419.67}, 3: {'City': 'Almería', 'Viajes en evento': 5, 'Viajes normal': 0.34, 'Proporción': 1484.54}, 4: {'City': 'Córdoba', 'Viajes en evento': 13, 'Viajes normal': 0.49, 'Proporción': 2672.22}, 5: {'City': 'Jaén', 'Viajes en evento': 9, 'Viajes normal': 0.3, 'Proporción': 3000.0}, 6: {'City': 'Málaga', 'Viajes en evento': 2, 'Viajes normal': 0.01, 'Proporción': 35700.0}, 7: {'City': 'Murcia', 'Viajes en evento': 4, 'Viajes normal': 0.26, 'Proporción': 1538.46}, 8: {'City': 'Marbella', 'Viajes en evento': 2, 'Viajes normal': 0.06, 'Proporción': 3350.0}, 9: {'City': 'Lucena', 'Viajes en evento': 3, 'Viajes normal': 0.07, 'Proporción': 4050.0}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-weekend beach-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Granada', 'Viajes en evento': 53, 'Viajes normal': 2.36, 'Proporción': 2249.16}, 1: {'City': 'Córdoba', 'Viajes en evento': 25, 'Viajes normal': 0.67, 'Proporción': 3719.51}, 2: {'City': 'Sevilla', 'Viajes en evento': 24, 'Viajes normal': 0.72, 'Proporción': 3331.76}, 3: {'City': 'Madrid', 'Viajes en evento': 22, 'Viajes normal': 0.45, 'Proporción': 4907.69}, 4: {'City': 'Jaén', 'Viajes en evento': 9, 'Viajes normal': 0.44, 'Proporción': 2041.94}, 5: {'City': 'Almería', 'Viajes en evento': 10, 'Viajes normal': 0.87, 'Proporción': 1143.54}, 6: {'City': 'Algeciras', 'Viajes en evento': 4, 'Viajes normal': 0.18, 'Proporción': 2234.48}, 7: {'City': 'Murcia', 'Viajes en evento': 5, 'Viajes normal': 0.23, 'Proporción': 2187.5}, 8: {'City': 'Motril', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2422.22}, 9: {'City': 'Antequera', 'Viajes en evento': 1, 'Viajes normal': 0.04, 'Proporción': 2520.0}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-weekend beach-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Granada', 'Viajes en evento': 8, 'Viajes normal': 1.99, 'Proporción': 402.76}, 1: {'City': 'Almería', 'Viajes en evento': 2, 'Viajes normal': 0.39, 'Proporción': 515.38}, 2: {'City': 'Córdoba', 'Viajes en evento': 2, 'Viajes normal': 0.48, 'Proporción': 414.63}, 3: {'City': 'Sevilla', 'Viajes en evento': 3, 'Viajes normal': 0.5, 'Proporción': 595.42}, 4: {'City': 'El Ejido', 'Viajes en evento': 2, 'Viajes normal': 0.08, 'Proporción': 2516.67}, 5: {'City': 'Alicante', 'Viajes en evento': 1, 'Viajes normal': 0.32, 'Proporción': 315.38}, 6: {'City': 'Jerez de la Frontera', 'Viajes en evento': 1, 'Viajes normal': 0.15, 'Proporción': 685.71}, 7: {'City': 'Madrid', 'Viajes en evento': 1, 'Viajes normal': 0.56, 'Proporción': 179.37}, 8: {'City': 'Santa Cruz de Mudela', 'Viajes en evento': 1, 'Viajes normal': null, 'Proporción': null}, 9: {'City': 'Tarifa', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 1850.0}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-weekend beach-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Granada', 'Viajes en evento': 19, 'Viajes normal': 2.44, 'Proporción': 779.49}, 1: {'City': 'Córdoba', 'Viajes en evento': 5, 'Viajes normal': 0.83, 'Proporción': 602.44}, 2: {'City': 'Sevilla', 'Viajes en evento': 3, 'Viajes normal': 0.73, 'Proporción': 410.53}, 3: {'City': 'Jaén', 'Viajes en evento': 1, 'Viajes normal': 0.43, 'Proporción': 231.52}, 4: {'City': 'Armilla', 'Viajes en evento': 1, 'Viajes normal': 0.19, 'Proporción': 525.0}, 5: {'City': 'Murcia', 'Viajes en evento': 1, 'Viajes normal': 0.25, 'Proporción': 400.0}, 6: {'City': 'Benidorm', 'Viajes en evento': 1, 'Viajes normal': 0.07, 'Proporción': 1500.0}, 7: {'City': 'El Ejido', 'Viajes en evento': 1, 'Viajes normal': 0.05, 'Proporción': 1912.5}, 8: {'City': 'Elche', 'Viajes en evento': 1, 'Viajes normal': 0.17, 'Proporción': 600.0}, 9: {'City': 'Roquetas de Mar', 'Viajes en evento': 1, 'Viajes normal': 0.27, 'Proporción': 366.67}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_weekend beach_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_weekend beach_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":['DAVID GUETTA', 'ARCO', 'THE OFFSRING', 'BUNBURY', 'WHYCLEF JEAN', 'NATOS Y WAOR', 'IZAL', 'MALA RODRIGUEZ', 'PENDULUM', 'SFDK', 'INNER CIRCLE', 'MACACO', 'JIMMY CLIFF', 'SUB FOCUS', 'ALBOROSIE', 'AVALANCH', 'LA PEGATINA', 'EL KANKA', 'LA M.O.D.A', 'SIDECARS', 'LA RAIZ', 'GREEN VALLEY', 'JUANITO MAKANDE', 'FYAHBWOY', 'LA FUGA', 'VITALIC', 'ADAM BEYER', 'DESAKATO', 'CELTAS CORTOS', 'BOIKOT', 'VIVA SUECIA', 'DORIAN', 'HUECCO', 'LOCOPLAYA', 'ILL NIÑO', 'THE QEMISTS', 'DANZA INVISIBLE', 'MUCHACHITO', 'EL CANIJO DE JEREZ', 'DUBIOZA KOLEKTIV', 'GATILLAZO', 'ANTILOPEZ', 'NEUMAN', 'THE TOY DOLLS', 'LOCO DICE', 'ASIAN DUB FOUNDATION', 'ESKORZO', 'FATIMA HAJJI', 'PACO OSUNA', 'HORACIO CRUZ', 'EL LANGUI'],
	 	 "artistas_evento_2019":['OZUNA', 'BLACK EYED PEAS', 'BECKY G', 'MAKA', 'BERET', 'BAD GYAL', 'NGHTMRE', 'ROZALEN', 'DELLAFUENTE', 'VETUSTA MORLA', 'SKA-P', 'AYAX Y PROK', 'CARLOS SADNESS', 'SFDK', 'NATHY PELUSO', 'SECOND', 'LA PEGATINA', 'NIKONE', 'MISS CAFFEINA', 'ZOO', 'JUANITO MAKANDE', 'LUZ CASAL', 'MUERDO', 'RAPSUSKLEI', 'SKINDRED', 'FUEL FANDANGO', 'THE ORIGINAL WAILERS', 'MORGAN HERITAGE', 'GENERAL LEVY', 'VARRY BRAVA', 'TALCO', 'MEDINA AZAHARA', 'LAURENT GARNIER', 'ASLANDTICOS', 'NIÑOS MUTANTES', 'ANTILOPEZ', 'MOJINOS ESCOZIOS', 'LOS ZIGARROS', 'NICOLE MOUDABER', 'CHE SUDAKA', 'PACO OSUNA', 'CARLO LIO', 'SEXY ZEBRAS', 'MARKY RAMONE'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_weekend beach_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_weekend beach_2019.jpg",
	 }, 
	 { 
	 	 "id": "53",
	 	 "nombre": "WEEKEND CITY MADRID",
	 	 "tipo": "FESTIVAL",
	 	 "ciudad": "Madrid",
	 	 "fecha_ini_2018": "21-09-2018",
	 	 "fecha_fin_2018": "22-09-2018",
	 	 "fecha_ini_2019": "20-09-2019",
	 	 "fecha_fin_2019": "21-09-2019",

	 	 // datos relevantes numéricos
	 	 "D001_evento_viajes_2018": 734.6666666666666,
	 	 "D002_evento_media_viajes_2018": 744.16,
	 	 "D003_evento_media_viajes_dia_semana_2018": 903.92,
	 	 "D004_evento_viajes_2019": 733.33,
	 	 "D005_evento_media_viajes_2019": 804.54,
	 	 "D006_evento_media_viajes_dia_semana_2019": 973.71,

	 	 "D007_proporcion_asientos_evento_2018": 35.09,
	 	 "D008_proporcion_asientos_media_evento_2018": 26.7,
	 	 "D009_proporcion_asientos_evento_2019": 35.05,
	 	 "D010_proporcion_asientos_media_evento_2019": 28.01,

	 	 "D011_ocupacion_asientos_evento_2018": 0.02,
	 	 "D012_ocupacion_asientos_media_evento_2018": 1.82,
	 	 "D013_ocupacion_asientos_evento_2019": 0.01,
	 	 "D014_ocupacion_asientos_media_evento_2019": 1.8,

	 	 "D015_precio_evento_2018": 0.0494,
	 	 "D016_precio_medio_evento_2018": 0.0491,
	 	 "D017_precio_evento_2019": 0.0495,
	 	 "D018_precio_medio_evento_2019": 0.0502,

	 	 "D019_novatos_evento_2018": 217.33,
	 	 "D020_novatos_media_evento_2018": 214.74,
	 	 "D021_novatos_evento_2019": 154.33,
	 	 "D022_novatos_media_evento_2019": 184.95,

	 	 // gráficos y mapas
	 	 "mapa_origenes_evento_2018": "mapas-eventos/mapa-origenes-weekend city madrid-2018.html",
	 	 "tabla_origenes_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 267, 'Viajes normal': 75.84, 'Proporción': 352.06}, 1: {'City': 'Murcia', 'Viajes en evento': 126, 'Viajes normal': 38.76, 'Proporción': 325.04}, 2: {'City': 'Zaragoza', 'Viajes en evento': 35, 'Viajes normal': 14.25, 'Proporción': 245.58}, 3: {'City': 'Barcelona', 'Viajes en evento': 90, 'Viajes normal': 23.15, 'Proporción': 388.71}, 4: {'City': 'Burgos', 'Viajes en evento': 55, 'Viajes normal': 15.87, 'Proporción': 346.6}, 5: {'City': 'Sevilla', 'Viajes en evento': 68, 'Viajes normal': 21.66, 'Proporción': 313.94}, 6: {'City': 'Granada', 'Viajes en evento': 64, 'Viajes normal': 22.2, 'Proporción': 288.25}, 7: {'City': 'Albacete', 'Viajes en evento': 39, 'Viajes normal': 15.02, 'Proporción': 259.62}, 8: {'City': 'Salamanca', 'Viajes en evento': 81, 'Viajes normal': 25.28, 'Proporción': 320.42}, 9: {'City': 'Bilbao', 'Viajes en evento': 76, 'Viajes normal': 20.98, 'Proporción': 362.24}},
	 	 "mapa_origenes_evento_2019": "mapas-eventos/mapa-origenes-weekend city madrid-2019.html",
	 	 "tabla_origenes_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 258, 'Viajes normal': 81.06, 'Proporción': 318.29}, 1: {'City': 'Murcia', 'Viajes en evento': 152, 'Viajes normal': 42.04, 'Proporción': 361.56}, 2: {'City': 'Zaragoza', 'Viajes en evento': 45, 'Viajes normal': 15.0, 'Proporción': 300.0}, 3: {'City': 'Granada', 'Viajes en evento': 49, 'Viajes normal': 21.68, 'Proporción': 225.97}, 4: {'City': 'Sevilla', 'Viajes en evento': 45, 'Viajes normal': 20.66, 'Proporción': 217.77}, 5: {'City': 'Albacete', 'Viajes en evento': 44, 'Viajes normal': 15.92, 'Proporción': 276.31}, 6: {'City': 'Burgos', 'Viajes en evento': 56, 'Viajes normal': 17.33, 'Proporción': 323.16}, 7: {'City': 'Barcelona', 'Viajes en evento': 78, 'Viajes normal': 21.42, 'Proporción': 364.07}, 8: {'City': 'Mérida', 'Viajes en evento': 26, 'Viajes normal': 11.9, 'Proporción': 218.4}, 9: {'City': 'Alicante', 'Viajes en evento': 50, 'Viajes normal': 24.62, 'Proporción': 203.13}},
	 	 "mapa_destinos_evento_2018": "mapas-eventos/mapa-destinos-weekend city madrid-2018.html",
	 	 "tabla_destinos_evento_2018":{0: {'City': 'Valencia', 'Viajes en evento': 331, 'Viajes normal': 74.61, 'Proporción': 443.65}, 1: {'City': 'Granada', 'Viajes en evento': 193, 'Viajes normal': 21.29, 'Proporción': 906.39}, 2: {'City': 'Murcia', 'Viajes en evento': 147, 'Viajes normal': 37.84, 'Proporción': 388.44}, 3: {'City': 'Sevilla', 'Viajes en evento': 85, 'Viajes normal': 19.96, 'Proporción': 425.82}, 4: {'City': 'Burgos', 'Viajes en evento': 72, 'Viajes normal': 15.47, 'Proporción': 465.38}, 5: {'City': 'Albacete', 'Viajes en evento': 47, 'Viajes normal': 13.73, 'Proporción': 342.21}, 6: {'City': 'Salamanca', 'Viajes en evento': 124, 'Viajes normal': 25.84, 'Proporción': 479.96}, 7: {'City': 'Zaragoza', 'Viajes en evento': 55, 'Viajes normal': 13.25, 'Proporción': 415.12}, 8: {'City': 'Mérida', 'Viajes en evento': 43, 'Viajes normal': 10.33, 'Proporción': 416.09}, 9: {'City': 'Jaén', 'Viajes en evento': 37, 'Viajes normal': 9.58, 'Proporción': 386.08}},
	 	 "mapa_destinos_evento_2019": "mapas-eventos/mapa-destinos-weekend city madrid-2019.html",
	 	 "tabla_destinos_evento_2019":{0: {'City': 'Valencia', 'Viajes en evento': 289, 'Viajes normal': 80.15, 'Proporción': 360.58}, 1: {'City': 'Granada', 'Viajes en evento': 194, 'Viajes normal': 20.91, 'Proporción': 927.73}, 2: {'City': 'Murcia', 'Viajes en evento': 196, 'Viajes normal': 41.38, 'Proporción': 473.64}, 3: {'City': 'Zaragoza', 'Viajes en evento': 59, 'Viajes normal': 14.19, 'Proporción': 415.76}, 4: {'City': 'Sevilla', 'Viajes en evento': 88, 'Viajes normal': 20.65, 'Proporción': 426.06}, 5: {'City': 'Albacete', 'Viajes en evento': 61, 'Viajes normal': 14.29, 'Proporción': 426.79}, 6: {'City': 'Burgos', 'Viajes en evento': 85, 'Viajes normal': 16.48, 'Proporción': 515.77}, 7: {'City': 'Mérida', 'Viajes en evento': 61, 'Viajes normal': 10.91, 'Proporción': 559.06}, 8: {'City': 'Salamanca', 'Viajes en evento': 111, 'Viajes normal': 28.94, 'Proporción': 383.5}, 9: {'City': 'Alicante', 'Viajes en evento': 83, 'Viajes normal': 23.83, 'Proporción': 348.36}},
	 	 // mapas timestamps
	 	 "mapa_kepler_2018":"KeplerMaps/Kepler_map_weekend city madrid_2018.html",
	 	 "mapa_kepler_2019":"KeplerMaps/Kepler_map_weekend city madrid_2019.html",
	 	 // Artistas
	 	 "artistas_evento_2018":[],
	 	 "artistas_evento_2019":['RELS B', 'NACH', 'BAD GYAL', 'NEW ORDER', 'AME', 'ROZALEN', 'DELLAFUENTE', 'AYAX Y PROK', 'MALA RODRIGUEZ', 'JUANCHO MARQUES', 'SFDK', 'MACACO', 'SIDECARS', 'JUANITO MAKANDE', 'FYAHBWOY', 'JAMIE JONES', 'EATS EVERYTHING', 'MIGUEL CAMPELLO', 'EL CANIJO DE JEREZ', 'SHINOVA', 'SHO-HAI', 'ANTILOPEZ', 'PAN-POT', 'TRIBADE', 'HENRY SAIZ', 'LA FLEUR', 'HORACIO CRUZ', 'LOS CIGARROS'],
	 	 // Carteles
	 	 "cartel_evento_2018":"F2_Carteles/cartel_weekend city madrid_2018.jpg",
	 	 "cartel_evento_2019":"F2_Carteles/cartel_weekend city madrid_2019.jpg",
	 }, 
]