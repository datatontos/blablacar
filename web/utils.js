function getArtistInfo(nombreArtista){
	return nuestrosArtistas[nombreArtista];
}

function generateArtistTile(nombreArtista){
	artistInfo = getArtistInfo(nombreArtista);

	if (artistInfo){
		tilehtml = '<div class="col-11 col-md-5 m-2">'
		tilehtml += '<div class="row border border-primary p-2 rounded">'
		tilehtml += '<div class="col-2">'
		tilehtml += '<i class="icon-info btn btn-primary" data-toggle="modal" data-target="#modal-info-artista" onclick="clickInfoArtista(\'' + nombreArtista +'\')"></i>'
		tilehtml += '</div>'
		tilehtml += '<div class="col-10 text-center text-truncate">'
		tilehtml += '<iframe src="https://embed.spotify.com/follow/1/?uri=' + artistInfo["uri"] + '&size=detail&theme=light" width="100%" height="55px" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>'
		tilehtml += '</div>'
		tilehtml += '</div>'
		tilehtml += '</div>'
	} else {
		tilehtml = ""
	}
	return tilehtml;
}

function clickInfoArtista(nombreArtista){
	artistInfo = getArtistInfo(nombreArtista)
	document.getElementById("modal-info-artista-label").innerHTML = artistInfo['nombre_spotify'];

	internalHTMLModal = '<div class="row">'
	internalHTMLModal += '<div class="col-12 col-md-6 text-center">'
	internalHTMLModal += '<iframe src="https://open.spotify.com/embed/artist/' + artistInfo['uri'].split(":")[2] + '" width="300" height="425" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>'
	internalHTMLModal += '</div>'
	internalHTMLModal += '<div class="col-12 col-md-6">'
	internalHTMLModal += '<h4> Festivales 2018: </h4>'
	if (artistInfo['2018'].length){
		internalHTMLModal += '<ul>'
		for (f in artistInfo['2018']){
			internalHTMLModal += '<li>'
			internalHTMLModal += artistInfo['2018'][f]
			internalHTMLModal += ' &nbsp; <i class="icon-eye btn btn-primary btn-sm pt-0 pb-0" data-dismiss="modal" onclick="ArtistaToFestival(\'' + artistInfo['2018'][f] +'\')"></i>'
			internalHTMLModal += '</li>'
		}
		internalHTMLModal += '</ul>'
	} else {
		internalHTMLModal += '<div class="text-center">'
		internalHTMLModal += '<img src="images/404.svg" alt="404" width="50%"> <br>'
		internalHTMLModal += '<b>¡Uy! parece que no fue a ninguno este año</b>'
		internalHTMLModal += '</div>'
	}

	internalHTMLModal += '<h4> Festivales 2019: </h4>'
	if (artistInfo['2019'].length){
		internalHTMLModal += '<ul>'
		for (f in artistInfo['2019']){
			internalHTMLModal += '<li>'
			internalHTMLModal += artistInfo['2019'][f]
			internalHTMLModal += ' &nbsp; <i class="icon-eye btn btn-primary btn-sm pt-0 pb-0" data-dismiss="modal" onclick="ArtistaToFestival(\'' + artistInfo['2019'][f] +'\')"></i>'
			internalHTMLModal += '</li>'
		}
		internalHTMLModal += '</ul>'
	} else {
		internalHTMLModal += '<div class="text-center">'
		internalHTMLModal += '<img src="images/404.svg" alt="404" width="50%"> <br>'
		internalHTMLModal += '<b>¡Uy! parece que no fue a ninguno este año</b>'
		internalHTMLModal += '</div>'
	}
	internalHTMLModal += '</div>'
	internalHTMLModal += '</div>'
	document.getElementById("modal-info-artista-container").innerHTML = internalHTMLModal;
}

function ArtistaToFestival(nombreFestival){
	nuevoEventoSelect(nuestrosEventosNombre[nombreFestival].id);
}


function cambiarCartelYArtistas(evento){

	// cambiar cartel 2018
	document.getElementById("img-cartel-2018").src = evento.cartel_evento_2018;
	document.getElementById("img-cartel-2018").alt = evento.nombre;

	// cambiar cartel 2019
	document.getElementById("img-cartel-2019").src = evento.cartel_evento_2019;
	document.getElementById("img-cartel-2019").alt = evento.nombre;

	// cambiar artistas 2018
    document.getElementById("lista-artistas-2018").innerHTML = "";
    artistas2018HTML = ""
	for (artista in evento.artistas_evento_2018){
		if(evento.artistas_evento_2018[artista]){
			artistas2018HTML += generateArtistTile(evento.artistas_evento_2018[artista])
		}
	}
    document.getElementById("lista-artistas-2018").innerHTML = artistas2018HTML;

	// cambiar artistas 2019
    document.getElementById("lista-artistas-2019").innerHTML = "";
    artistas2019HTML = ""
	for (artista in evento.artistas_evento_2019){
		if(evento.artistas_evento_2019[artista]){
			artistas2019HTML += generateArtistTile(evento.artistas_evento_2019[artista])
		}
	}
    document.getElementById("lista-artistas-2019").innerHTML = artistas2019HTML;

}

function topFestivales(metrica, top=5){
	ord = nuestrosEventos.sort(function(first, second) {
		return second[metrica] - first[metrica];
	});

	return ord.slice(0, top);
}

function generateTopFestivalesGraph(title, x, y, divID, color){
	var data = [
	  {
	    x: x,
	    y: y,
	    type: 'bar',
	    marker: {
	    	color: color
	    }
	  }
	];

	var layout = {
	  title: title,
	  xaxis:{
	  	title: {
	  		text: 'Festivales'
	  	}
	  },
	  yaxis:{
	  	title: {
	  		text: 'Impacto'
	  	}
	  },
	};
	Plotly.newPlot(divID, data, layout);
}

function generateViajesRenta(){
	var trace1 = {
	  x: viajesRenta['x'],
	  y: viajesRenta['y'],
	  mode: 'markers',
	  type: 'scatter',
	  name: 'Viajes',
	  text: viajesRenta['tag'],
	  marker: { size: 12 }
	};

	var data = [ trace1 ];

	var layout = {
	  title:'Viajes según renta y población',
	  xaxis:{
	  	title: {
	  		text: 'renta bruta media (x 1000)'
	  	}
	  },
	  yaxis:{
	  	title: {
	  		text: 'viajes cada 100000 habitantes'
	  	}
	  },
	};

	Plotly.newPlot('div-viajes-renta', data, layout);
}

function generateTopFestivales(){
	top_2018 = topFestivales('INT_VIAJES_2018', 8)
	top_2019 = topFestivales('INT_VIAJES_2019', 8)

	x = []
	y = []
	for (e in top_2018){
		x.push(top_2018[e]['nombre'])
		y.push(top_2018[e]['INT_VIAJES_2018'])
	}
	generateTopFestivalesGraph('Top mayor incremento en 2018',x, y, 'conclusiones-top-festivales-2018', '#29BAE4')
	x = []
	y = []
	for (e in top_2019){
		x.push(top_2019[e]['nombre'])
		y.push(top_2019[e]['INT_VIAJES_2019'])
	}
	generateTopFestivalesGraph('Top mayor incremento en 2019',x, y, 'conclusiones-top-festivales-2019', '#EE77AD')
}

function conclusionsGeneralIncrement(){
	num_total_viajes = 0;
	num_total_asientos = 0;

	total_asientos = 0;
	total_viajes = 0;

	for (e in nuestrosEventos){
		//total_viajes_evento += nuestrosEventos[e].D001_evento_viajes_2018 + nuestrosEventos[e].D004_evento_viajes_2019
		//total_viajes_media += nuestrosEventos[e].D002_evento_media_viajes_2018 + nuestrosEventos[e].D005_evento_media_viajes_2019
		//total_asientos_evento += nuestrosEventos[e].D007_proporcion_asientos_evento_2018 + nuestrosEventos[e].D009_proporcion_asientos_evento_2019
		//total_asientos_media += nuestrosEventos[e].D008_proporcion_asientos_media_evento_2018 + nuestrosEventos[e].D010_proporcion_asientos_media_evento_2019

		// media viajes 2018
		prop = (nuestrosEventos[e].D001_evento_viajes_2018-nuestrosEventos[e].D002_evento_media_viajes_2018)*100/nuestrosEventos[e].D002_evento_media_viajes_2018
		if(prop && prop != Infinity){
			total_viajes += prop
			num_total_viajes +=1
		}

		// media viajes 2019
		prop = (nuestrosEventos[e].D004_evento_viajes_2019-nuestrosEventos[e].D005_evento_media_viajes_2019)*100/nuestrosEventos[e].D005_evento_media_viajes_2019
		if(prop && prop != Infinity){
			total_viajes += prop
			num_total_viajes +=1
		}

		// media asientos 2018
		prop = (nuestrosEventos[e].D007_proporcion_asientos_evento_2018-nuestrosEventos[e].D008_proporcion_asientos_media_evento_2018)*100/nuestrosEventos[e].D008_proporcion_asientos_media_evento_2018
		if(prop && prop != Infinity){
			total_asientos += prop
			num_total_asientos +=1
		}

		// media asientos 2019
		prop = (nuestrosEventos[e].D009_proporcion_asientos_evento_2019-nuestrosEventos[e].D010_proporcion_asientos_media_evento_2019)*100/nuestrosEventos[e].D010_proporcion_asientos_media_evento_2019
		if(prop && prop != Infinity){
			total_asientos += prop
			num_total_asientos +=1
		}

	}

	html_por_viajes = '<strong class="number">+ ' + (total_viajes/num_total_viajes).toFixed(2) + ' %</strong>';
	html_por_asientos = '<strong class="number">+ ' + (total_asientos/num_total_asientos).toFixed(2) + ' %</strong>';

	document.getElementById("conclusiones-tot-viajes").innerHTML = html_por_viajes
	document.getElementById("conclusiones-tot-asientos").innerHTML = html_por_asientos

}